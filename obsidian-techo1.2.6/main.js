var obsidian = require('obsidian');
//必要的预先定义的值
const DEFAULT_SETTINGS = {
    "weekStartDay": "周日",
    "backPattern":"网格",
    "svgWidth":200,
    "weatherWeekSum": true,
    "weekClassName": 'a',
    "weatherUseLine": true,
    "timeBarWidth": 10,
    "timeBarRadius":5,
    "timeBarLocated":0,
    "timeLineRadius":6,
    "timeLineWidth":4,
    "timeLineDash":"30,10",
    "timeLineLocated":10,
    "timeNoteWidth":170,
    "timeNoteLocated":15,
    "timeNoteRadius":4,
    "timeNoteStrokewidth":5,
    "timeNoteStrokeDash":"10,20",
    "showTodo":true,
    "showSchedule":true,
    "showNobill":true,
    "showBillLable":false,
    "showBillWidth":1000,
    "showNightSVG":true
};
// 必要的函数
// "1,3-8,10"=>1,3,4,5,6,7,8,10
function getNumberByString(str){
    var arr=str.split(',')
    var resultNumber=[]
    for(var i=0;i<arr.length;i++){
        if(/^\d+-\d+$/.test(arr[i])){
            var numbers=arr[i].match(/\d+/g).map(Number).sort()
            var starNumber=numbers[0]
            var endNumber=numbers[1]
            for(var j=starNumber;j<=endNumber;j++){
                resultNumber.push(j)
            }
        }else{
            resultNumber.push(Number(arr[i]))
        }
    }
    return resultNumber
}
// var str="1,3-8,10"
//接受一个数组，删除空值‘’
function filterarr(arr){
    if(Array.isArray(arr)){
        return arr.filter(word=> !word.match(/^\s*$/))
    }else{
        return []
    }
    
}

function timeLast(numberString1,numberString2,weatherNumber){
    starTimeCode=numberString1
    endTimeCode=numberString2
    var starTime=parseInt(starTimeCode.slice(0,2))*60+parseInt(starTimeCode.slice(2,4))
    var endTime=parseInt(endTimeCode.slice(0,2))*60+parseInt(endTimeCode.slice(2,4))
    var timeLastnumber=endTime-starTime
    var timeLast=timeLastnumber>=0?timeLastnumber:1440+timeLastnumber
    
    if(weatherNumber){
        return timeLast/60
    }else{
        var hours=parseInt(timeLast/60)
        var minutes=timeLast%60
        return Array(2-String(hours).length+1).join('0')+hours+Array(2-String(minutes).length+1).join('0')+minutes;
    }
}
//接受data字符串，返回一个数组，数组有12个元素，每个元素是每月待办的数组
function getMonthTodo(str){
    let months=[
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    ]
    // [[0,月计划1],[0,月计划2],[1,月计划3]]
    let monthTodoReg=/#\s\d{1,2}[M|月]\n[^#]+/g
    let monthTodos=str.match(monthTodoReg)
    monthTodos=filterarr(monthTodos)
    
    // .replace(/\n#$/,'')
    for(var i=0;i<monthTodos.length;i++){
        // 获取月份字符串
        var monthStr=monthTodos[i].match(/#\s\d{1,2}[M|月]\n/i)[0]
        //获取月份数字
        var monthNumber=monthStr.match(/\d{1,2}/i)[0]

        //删掉每月月份字符串
        monthTodos[i]=monthTodos[i].replace(monthStr,"")
        //删掉每月月计划末尾的#
        monthTodos[i]=monthTodos[i].replace(/\n#$/,'')
        //根据 -[x]分割每月计划
        monthTodos[i]=monthTodos[i].split(/\n/)
        monthTodos[i]=filterarr(monthTodos[i])
        //将每项计划分割为状态码和字符串
        for(var j=0;j<monthTodos[i].length;j++){
            var todoArr=[]
            if(monthTodos[i][j].match(/^\s*-\s*\[\s*x\s*\]\s*/)){
                todoArr[0]=1
                todoArr[1]=monthTodos[i][j].replace(/^\s*-\s*\[\s*x\s*\]\s*/,'')
                monthTodos[i][j]=todoArr
            }else if(monthTodos[i][j].match(/^\s*-\s*\[\s*\]\s*/)){
                todoArr[0]=0
                todoArr[1]=monthTodos[i][j].replace(/^\s*-\s*\[\s*\]\s*/,'')
                monthTodos[i][j]=todoArr
            }else{
                console.log('这里出错了')
            }

        }
        months[monthNumber-1]=monthTodos[i]
    }
    return months
}
//接受周数、原始数据，返回该周的todo
function getThisWeekTodo(number,str){
    let weekTodoReg=new RegExp(('##\\s+'+number+'[W|周]\\n[^#]+'),'g') 
    let weekTodos=str.match(weekTodoReg)
    weekTodos=filterarr(weekTodos)
    let ThisWeekTodo=[]
    //对于拿到的每一个今日Todo字符串
    for(var i=0;i<weekTodos.length;i++){
        
        weekTodos[i]=weekTodos[i].replace(/^##\s+\d{1,2}[W|周]\n/,'')
        weekTodos[i]=weekTodos[i].replace(/\s*#/,'')
        weekTodos[i]=weekTodos[i].split(/\n/)
        weekTodos[i]=filterarr(weekTodos[i])
        for(var j=0;j<weekTodos[i].length;j++){
            var todoarr=[]
            //已完成
            if(weekTodos[i][j].match(/^\s*-\s*\[\s*x\s*\]\s*/)){
                todoarr[0]=1
                todoarr[1]=weekTodos[i][j].replace(/^\s*-\s*\[\s*x\s*\]\s*/,'')
                ThisWeekTodo.push(todoarr)
            //未完成
            }else if(weekTodos[i][j].match(/^\s*-\s*\[\s*\]\s*/)){
                todoarr[0]=0
                todoarr[1]=weekTodos[i][j].replace(/^\s*-\s*\[\s*\]\s*/,'')
                ThisWeekTodo.push(todoarr)
            }else{
            }
        }
        
        
    }
    return ThisWeekTodo
}

// 接受日期，返回这一天的todo,1/22 =>
function getThisDayTodo(dayStr,dataStr){
    let dayTodoReg=new RegExp('###\\s+'+dayStr+'\\s+[^#]+','g')
    let dayTodos=dataStr.match(dayTodoReg)
    dayTodos=filterarr(dayTodos)
    let thisDayTodo=[]
    //对于拿到的每一个今天数组元素 i：拿到的第i个今天的记录
    for(var i=0;i<dayTodos.length;i++){
        //去除日期
        dayTodos[i]=dayTodos[i].replace(/^###\s+\d{1,2}\/\d{1,2}[^\n]*/,'')
        dayTodos[i]=dayTodos[i].match(/-\s*\[\s*x?\s*\][^\n]*/g)
        dayTodos[i]=filterarr(dayTodos[i])
        //j：第j个记录
        for(var j=0;j<dayTodos[i].length;j++){
            var todoarr=[]
            if(dayTodos[i][j].match(/^\s*-\s*\[\s*x\s*\]\s*/)){
                todoarr[0]=1
                todoarr[1]=dayTodos[i][j].replace(/^\s*-\s*\[\s*x\s*\]\s*/,'')
                thisDayTodo.push(todoarr)
            }else if(dayTodos[i][j].match(/^\s*-\s*\[\s*\]\s*/)){
                todoarr[0]=0
                todoarr[1]=dayTodos[i][j].replace(/^\s*-\s*\[\s*\]\s*/,'')
                thisDayTodo.push(todoarr)
            }else{
                console.log('出错了')
            }
        }
    }
    return thisDayTodo
}

//接受日期返回这一天的log日志
//  ["0050", "0630", "0540", "a", "睡觉"]
// ["0720", "0830", "0110", "b", "吃饭"]
// ["0900", "1200", "0300", "w1", "工作项目1"]
function getThisDayLog(dayStr,data){
    let dayLogReg=new RegExp('###\\s+'+dayStr+'\\s+[^#]+','g')
    let dayLogs=data.match(dayLogReg)
    dayLogs=filterarr(dayLogs)
    let thisDayLog=[]
    //对于拿到的每一个今天数组元素 i：拿到的第i个今天的记录
    for(var i=0;i<dayLogs.length;i++){
        //去除日期
        dayLogs[i]=dayLogs[i].replace(/^###\s+\d{1,2}\/\d{1,2}[^\n]*/,'')
        dayLogs[i]=dayLogs[i].match(/\d{4}-\d{4}\s+[a-y][0-9]{0,2} [^\n]*/g)
        dayLogs[i]=filterarr(dayLogs[i])
        
        for(var j=0;j<dayLogs[i].length;j++){
            var thisLog=[]
            var thisLogTimeStrs=[]
            dayLogs[i][j]=dayLogs[i][j].split(/\s/)
            thisLogTimeStrs=dayLogs[i][j].shift().split(/-/)
            thisLog.push(thisLogTimeStrs[0])
            thisLog.push(thisLogTimeStrs[1])
            thisLog.push(timeLast(thisLogTimeStrs[0],thisLogTimeStrs[1],false))

            thisLog.push(dayLogs[i][j].shift())
            thisLog.push(dayLogs[i][j].join(' '))

            thisDayLog.push(thisLog)
        }
        
    }
    return thisDayLog
}
function getThisDaySchedule(dayStr,data){
    let dayLogReg=new RegExp('###\\s+'+dayStr+'\\s+[^#]+','g')
    let dayLogs=data.match(dayLogReg)
    dayLogs=filterarr(dayLogs)
    let thisDayLog=[]
    //对于拿到的每一个今天数组元素 i：拿到的第i个今天的记录
    for(var i=0;i<dayLogs.length;i++){
        //去除日期
        dayLogs[i]=dayLogs[i].replace(/^###\s+\d{1,2}\/\d{1,2}[^\n]*/,'')
        dayLogs[i]=dayLogs[i].match(/\d{4}-\d{4}\sz[0-9]? [^\n]*/g)
        dayLogs[i]=filterarr(dayLogs[i])
        for(var j=0;j<dayLogs[i].length;j++){
            var thisLog=[]
            var thisLogTimeStrs=[]
            dayLogs[i][j]=dayLogs[i][j].split(/\s/)
            thisLogTimeStrs=dayLogs[i][j].shift().split(/-/)
            thisLog.push(thisLogTimeStrs[0])
            thisLog.push(thisLogTimeStrs[1])
            thisLog.push(timeLast(thisLogTimeStrs[0],thisLogTimeStrs[1],false))

            thisLog.push(dayLogs[i][j].shift())
            thisLog.push(dayLogs[i][j].join(' '))

            thisDayLog.push(thisLog)
        }
        
    }
    return thisDayLog
}

//接受日期，返回这一天的账单
function getThisDayBill(dayStr,data){
    let dayBillReg=new RegExp('###\\s+'+dayStr+'\\s+[^#]+','g')
    let dayBills=data.match(dayBillReg)
    dayBills=filterarr(dayBills)
    let thisDayBill=[]
    //对于拿到的每一个今天数组元素 i：拿到的第i个今天的记录
    for(var i=0;i<dayBills.length;i++){
        //去掉标题
        dayBills[i]=dayBills[i].replace(/^###\s+\d{1,2}\/\d{1,2}[^\n]*/,'')
        dayBills[i]=dayBills[i].match(/[+|-][\d|\.]+\s[A-Z][0-9]?\s[^\n]*/g)
        dayBills[i]=filterarr(dayBills[i])
        for(var j=0;j<dayBills[i].length;j++){
            var thisBill=[]
            dayBills[i][j]=dayBills[i][j].split(/\s/)
            thisBill.push(dayBills[i][j].shift())
            thisBill.push(dayBills[i][j].shift())
            thisBill.push(dayBills[i][j].join(' '))
            thisDayBill.push(thisBill)
        }
    }
    return thisDayBill
}
//接受日期，返回这一天的日记
function getThisDayDiarry(dayStr,dataStr){
    let dayDiarryReg=new RegExp('###\\s+'+dayStr+'\\s+[^#]+','g')
    let dayDiarrys=dataStr.match(dayDiarryReg)
    // dayDiarrys=filterarr(dayDiarrys)
    
    let thisDayDiarrys=[]
    //对于拿到的每一个今天数组元素 i：拿到的第i个今天的记录
    for(var i=0;i<dayDiarrys.length;i++){
        //去掉标题
        dayDiarrys[i]=dayDiarrys[i].replace(/^###\s+\d{1,2}\/\d{1,2}[^\n]*\n/,'')
        dayDiarrys[i]=dayDiarrys[i].replace(/-\s*\[\s*x?\s*\][^\n]*\n/g,'')
        dayDiarrys[i]=dayDiarrys[i].replace(/\d{4}-\d{4}\s[a-z][0-9]? [^\n]*\n/g,'')
        dayDiarrys[i]=dayDiarrys[i].replace(/[+|-]\d+\s[A-Z][0-9]?\s[^\n]*\n/g,'')
        thisDayDiarrys.push(dayDiarrys[i])
    }
    thisDayDiarrys=thisDayDiarrys.join('\n')
    return thisDayDiarrys

}
function monthToDay(year,month){
    var day=new Date(year,month,1,0,0,0)
    day.setDate(day.getDate()-1)
    var monthdays=day.getDate()
    var daysArr=[]
    for(var i=1;i<=monthdays;i++){
        var today=''+month+'/'+i
        daysArr.push(today)
    }
    return  daysArr
}
//输入年份，月份返回一个月历数组
// [
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//    [年,周],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形],[日期,是否隐形]
//]
function monthToMoreDay(year,month,starAtSunday){
    var monthFirstDay=new Date(year,month-1,1,0,0,0)
    var monthLastDay=new Date(year,month,1,0,0,0)
    monthLastDay.setDate(monthLastDay.getDate()-1)
    var firstDay=new Date()
    var lastDay=new Date()
    if(starAtSunday){
        firstDay.setTime(monthFirstDay.getTime()-(monthFirstDay.getDay()*24*60*60*1000))
        lastDay.setTime(monthLastDay.getTime()+((6-monthLastDay.getDay())*24*60*60*1000))
    }else{
        var moveFront=[-6,0,-1,-2,-3,-4,-5]
        var moveBack=[0,6,5,4,3,2,1]
        firstDay.setTime(monthFirstDay.getTime()+(moveFront[monthFirstDay.getDay()]*24*60*60*1000))
        lastDay.setTime(monthLastDay.getTime()+(moveBack[monthLastDay.getDay()]*24*60*60*1000))
    }
    var result=[]
    var thisweek=[]
    var today=new Date()
    for(var i=firstDay.getTime();i<=lastDay.getTime();i+=(24*60*60*1000)){
        today.setTime(i)
        if((today.getDay()+Number(starAtSunday))==1){

            thisweek=[]
            thisweek.push([year,todayWeekNumber(today,starAtSunday)])
            
        }
        var dayStr=''+(today.getMonth()+1)+'/'+today.getDate()
        thisweek.push([dayStr,((month==(today.getMonth()+1))?1:0)])


        function weatherWeekend(starAtSunday,today){
            if(starAtSunday){
                return  today.getDay()==6
            }else{
                return today.getDay()==0
            }
        }
        if(weatherWeekend(starAtSunday,today)){
            result.push(thisweek)
        }
    }
    return result
}
// "1/9/2022"

                    
function todayWeekNumber(thisday,starAtSunday){
    //今年第一天设为本年的一月一日
    var thisYearFirstDay=new Date()
    
    thisYearFirstDay.setTime(thisday.getTime())
    thisYearFirstDay.setMonth(0)
    thisYearFirstDay.setDate(1)
    thisYearFirstDay.setHours(0)
    thisYearFirstDay.setMinutes(0)
    thisYearFirstDay.setSeconds(0)
    //为避开凌晨零点零时算那天的问题，将一年初始时间向前调整1min
    thisYearFirstDay.setTime(thisYearFirstDay.getTime()-0*60*60*1000)
    
    //以周日开始
    if(starAtSunday){
        thisYearFirstDay.setTime(thisYearFirstDay.getTime()-(thisYearFirstDay.getDay()*24*60*60*1000))
        
    }else{
        var moveFront=[-6,0,-1,-2,-3,-4,-5]
        var moveBack=[0,6,5,4,3,2,1]
        thisYearFirstDay.setTime(thisYearFirstDay.getTime()+(moveFront[thisYearFirstDay.getDay()]*24*60*60*1000))
    }
    
    
    var starDay=thisYearFirstDay.getTime()
    var today=thisday.getTime()
    return parseInt((today-starDay)/(7*24*60*60*1000))
}
//输入年份 周数 返回本周七天日期
//['12/19', '12/20', '12/21', '12/22', '12/23', '12/24', '12/25']
function weekToDay(year,weeknumber,starAtSunday){
    var firstDayAtYear=new Date(year,0,1,0,0,0)
    // var todayWeek=firstDayAtYear.getDay()
    if(starAtSunday){
        weeknumber=weeknumber-1
        movefront=[0,-1,-2,-3,-4,-5,-6]
        firstDayAtYear.setTime(firstDayAtYear.getTime()+(movefront[firstDayAtYear.getDay()]*24*60*60*1000))
        
    }else{
        movefront=[-6,0,-1,-2,-3,-4,-5]
        firstDayAtYear.setTime(firstDayAtYear.getTime()+(movefront[firstDayAtYear.getDay()]*24*60*60*1000))
    }
    // weeknumber=weeknumber-1
    var today=new Date()
    today.setTime(firstDayAtYear.getTime()+weeknumber*7*24*60*60*1000)
    var weekDay=[]
    for(var i=0;i<7;i++){
        var month=today.getMonth()+1
        var day=today.getDate()
        
        var dayArr=[''+month+'/'+day,today.getDay()]
        today.setTime(today.getTime()+24*60*60*1000)
        weekDay.push(dayArr)
    }
    return weekDay
}
//接受 年月数、月历数组，返回月历表格
//绘制时间线svg
function getSvgByLog(arr,i,weatherStarAtSunday,settings){
    
    var svgWidth=settings.svgWidth
    var showNightSVG=settings.showNightSVG
    var weatherWeekSum=settings.weatherWeekSum

    var useTimeColor=false       //使用时间分类颜色作为笔记的背景色？
    
    var thisDayWeekDay=weatherStarAtSunday?[0,1,2,3,4,5,6][i]:[1,2,3,4,5,6,0][i]
    var thisDayWeekDayBc=(thisDayWeekDay==0||thisDayWeekDay==6)?'var(--weekendSvgBcakgroundColr)':'var(--svgBcakgroundColr)'
    
    //开头
    let svgHeigh=Number(svgWidth)*3.6
    let viewBoxy1
    let viewBoxy2
    if(showNightSVG){
        // svgHeigh+=Number(svgWidth)*1.2
        viewBoxy1=weatherWeekSum?-240:0
        viewBoxy2=weatherWeekSum?1000:720
    }else{
        viewBoxy1=weatherWeekSum?0:0
        viewBoxy2=weatherWeekSum?760:720
    }
    

    var result="<svg width="
    result+=svgWidth
    result+="px "
    // height="
    // result+=svgHeigh
    // result+="px 
    result+="viewBox='0 "+viewBoxy1+" 200 "+viewBoxy2+"' style='background: "
    result+=thisDayWeekDayBc
    result+=";'>\n"

    //汇总背景
    switch (settings.backPattern){
        case '网格':
            result +=draline(36,20,10,20,settings)
        break;
        case '空格':

        break;
        case '横线':
            result +=draline(36,20,0,0,settings)
        break;

    }
    
    //绘制时间轴
    result +=draTimeLine()

    if(settings.weatherWeekSum){
        let reg=/[a-y]/
        let className=String(settings.weekClassName.match(reg))
        let timeSumHour=0
        let timeSumMin=0
        //对于每一条日志
        for(var logNumber=0;logNumber<arr.length;logNumber++){
            if(arr[logNumber][3].match(new RegExp("^"+className,"i"))){
                //持续时长 0630
                let timeLast=arr[logNumber][2]
                let H=parseInt(timeLast.slice(0,2))
                let M=parseInt(timeLast.slice(2,4))

                timeSumHour+=H
                timeSumMin+=M
                

                // let thisLogTime=parseInt(timeLast.slice(0,2))+parseInt(timeLast.slice(2,4))/60
                // timeSum+=thisLogTime
            }
        }
        timeSumHour+=parseInt(timeSumMin/60)
        timeSumMin=timeSumMin%60
        timeSumHour=Array(2-String(timeSumHour).length+1).join('0')+timeSumHour
        timeSumMin=Array(2-String(timeSumMin).length+1).join('0')+timeSumMin

        // Array(2-String(hours).length+1).join('0')+hours+Array(2-String(minutes).length+1).join('0')

        // result+="<foreignObject class='time-sum' style='color:var(--"+className+"color)' width='"
        // result+=80
        // result+="' height='"
        // result+=80
        // result+="' x='100' y='"
        // result+=730
        // result+="'>\n<body xmlns='http://www.w3.org/1999/xhtml'>\n<div >"
        // result+='：'+timeSumHour+':'+timeSumMin
        // result+="</div>\n</body>\n</foreignObject>"

        result+="<text x='80' y='745' font-size='20' fill='var(--nomal)'>"+timeSumHour+':'+timeSumMin+"</text>"
        result+="<rect width='18' height='18' x= '60' y='730' fill='var(--"+className+"color)' stroke='var(--nomal)' stroke-width='1'"


        // <rect width='"
        //         result+=settings.timeBarWidth
        //         result+="' height='"   //时间条
        //         result +=timeLast*40
        //         result +="' x='"
        //         result+=settings.timeBarLocated
        //         result+="' y='"
        //         result +=(starTime-6)*40
        //         result +="' fill='"
        //         result +=colorCode
        //         result +="' rx='"
        //         result+=settings.timeBarRadius
        //         result+="' ry='"
        //         result+=settings.timeBarRadius
        //         result+="'/>\n"
    }


    result +="</svg>\n"

    
    return result
    function draline(hnumber,hstep,vnumber,vstep,settings){
                        
        var result=""
        var hlocated=0
        var vlocated=0
        
        for (var i=-12;i<=hnumber;i++){
            hlocated=hstep*i
            result +="<line  x1='0'  y1='"
            result +=hlocated
            result +="' x2='200' y2='"
            result +=hlocated
            //20*12=240
            if((hlocated%240==0)|(i==0)){
                //非整点
                result +="' stroke="
                result+="var(--clockLine)"
                result+=" stroke-width='2'/> \n"
            } else if(hlocated%120==0){  //9,15,21
                result +="' stroke="
                result+="var(--shline)"
                result+=" stroke-width='1'/> \n"
            }else{//整点
                result +="' stroke="
                result+="var(--hline)"
                result+=" stroke-width='1'/> \n"
            }
        }
        // <line x1='35' y1='0' x2='35' y2='700' stroke='#333'  stroke-width='0.5'/>
        for (var j=0;j<vnumber;j++){
            vlocated=vstep*j
            result +="<line x1='"
            result +=vlocated
            result +="' y1='-240' x2='"
            result +=vlocated
            result +="' y2='720' stroke="
            result+="var(--hline)"
            result+="  stroke-width='1'/> \n"
            
        }
        // result +="</svg>\n"
        return result
    }
    

    function draTimeLine(){
        var result=""
        // 对于每一条日程
        
        // 对于每一条记录
        for (var i=0;i<arr.length;i++){
            var starTimeCode=arr[i][0]
            var endTimeCode=arr[i][1]
            var starTime=parseInt(starTimeCode.slice(0,2))+parseInt(starTimeCode.slice(2,4))/60
            var endTime=parseInt(endTimeCode.slice(0,2))+parseInt(endTimeCode.slice(2,4))/60
            var timeLast=endTime-starTime

            var timeClass=arr[i][3]

            var colorCode='var(--'+timeClass.slice(0,1)+'color)'
            
            // 分割时间笔记
            
            var timeNote=arr[i][4]//时间备注
            // =========================
            // 绘制时间线rect
            if(settings.weatherUseLine){
                result+="<circle cx='"
                result+=settings.timeLineLocated
                result+="' cy='"
                result+=(starTime-6)*40
                result+="' r='"
                result+=settings.timeLineRadius
                result+="' stroke-width='1' stroke='var(--svgStrokeColor)' fill='"
                result+=colorCode
                result+="'/>"

                result+="<circle cx='"
                result+=settings.timeLineLocated
                result+="' cy='"
                result+=(endTime-6)*40
                result+="' r='"
                result+=settings.timeLineRadius
                result+="' stroke-width='1' stroke='var(--svgStrokeColor)' fill='"
                result+=colorCode
                result+="'/>"

                result+="<line x1='"
                result+=settings.timeLineLocated
                result+="' y1='"
                result+=(starTime-6)*40
                result+="' x2='"
                result+=settings.timeLineLocated
                result+="' y2='"
                result+=(endTime-6)*40
                result+="' stroke='"
                result+=colorCode
                result+="' stroke-width='"
                result+=settings.timeLineWidth
                result+="'  stroke-dasharray='"
                result+=settings.timeLineDash
                result+="'/>"

            }else{
                result +="<rect width='"
                result+=settings.timeBarWidth
                result+="' height='"   //时间条
                result +=timeLast*40
                result +="' x='"
                result+=settings.timeBarLocated
                result+="' y='"
                result +=(starTime-6)*40
                result +="' fill='"
                result +=colorCode
                result +="' rx='"
                result+=settings.timeBarRadius
                result+="' ry='"
                result+=settings.timeBarRadius
                result+="'/>\n"
            }
            // =========================
            //画方框rect
            result +="<rect width='"
            result+=settings.timeNoteWidth
            result+="' height='"   //方框
            result +=timeLast*40
            result +="' x='"
            result+=settings.timeNoteLocated
            result+="' y='"
            result +=(starTime-6)*40
            result +="' fill="
            
            if(useTimeColor){
                result +=colorCode
            }else{
                result+="var(--timeNoteBackgroundColor)"
            }
            // 
            
            result +=" rx='"
            result+=settings.timeNoteRadius
            result+="' ry='"
            result+=settings.timeNoteRadius
            result+="' stroke-width='"
            result+=settings.timeNoteStrokewidth
            result+="' stroke-dasharray='"
            result+=settings.timeNoteStrokeDash
            result+="' stroke="
            result+="var(--timeNoteBordeColor)"
            result+=" />\n"
            // =========================

            if(!showNightSVG&&starTime<=6&&endTime>6){
                result+="<foreignObject width='"
                result+=settings.timeNoteWidth
                result+="' height='"
                result+=(endTime-6)*40
                result+="' x='"
                result+=settings.timeNoteLocated
                result+="' y=0>\n<body xmlns='http://www.w3.org/1999/xhtml'>\n<div class='time-note'>"
                result+=timeNote
                result+="</div>\n</body>\n</foreignObject>"
            }else{
                result+="<foreignObject width='"
                result+=settings.timeNoteWidth
                result+="' height='"
                result+=timeLast*40
                result+="' x='"
                result+=settings.timeNoteLocated
                result+="' y='"
                result+=(starTime-6)*40
                result+="' >\n<body xmlns='http://www.w3.org/1999/xhtml'>\n<div class='time-note'>"
                result+=timeNote
                result+="</div>\n</body>\n</foreignObject>"
            }
        }
        
        return result
        
    }

}
function creatMonthTable(year,month,monthArr,settings){
    let monthPlan=getMonthTodo(app.workspace.activeLeaf.view.lastSavedData)
    let data=app.workspace.activeLeaf.view.lastSavedData
    let weatherStarAtSunday=settings.weekStartDay=='周日'
    let table=document.createElement('table')
    table.setAttribute('id','M-'+year+'-'+month)
    table.setAttribute('class','techo-month-table techo')
    let th=document.createElement('tr')
    if(weatherStarAtSunday){
        th.innerHTML="<th class='monthPlan'>"+year+'年'+month+"月</th><th class='weekNumber '>周</th><th class='sunday week'>周日</th><th class='monday week'>周一</th><th class='tuesday week'>周二</th><th class='wednesday week'>周三</th><th class=' thursday week'>周四</th><th class='friday week'>周五</th><th class='saturday week'>周六</th>"
    }else{
        th.innerHTML="<th class='monthPlan'>"+year+'年'+month+"月</th><th class='weekNumber '>周</th><th class='monday week'>周一</th><th class='tuesday week'>周二</th><th class='wednesday week'>周三</th><th class=' thursday week'>周四</th><th class='friday week'>周五</th><th class='saturday week'>周六</th><th class='sunday week'>周日</th>"
    
    }
    table.appendChild(th)

    let tr1=document.createElement('tr')

    let td1_1=document.createElement('td')
    td1_1.setAttribute('rowspan',monthArr.length)
    td1_1.setAttribute('class','monthPlan')
    let monthTeskArr=monthPlan[(month-1)]
    //对于每条月计划
    for(var i=0;i<monthTeskArr.length;i++){
        let input=document.createElement('input')
        let label=document.createElement('label')
        // 月计划是否已完成
        input.setAttribute('type','checkbox')
        if(monthTeskArr[i][0]){
            input.setAttribute('checked','')
        }
        input.setAttribute('id','M-'+year+'/'+month+'-'+i+'-monthTable')
        input.onclick=clickCheckChange
        label.setAttribute('for','M-'+year+'/'+month+'-'+i+'-monthTable')
        label.append(document.createTextNode(monthTeskArr[i][1]))

        td1_1.appendChild(input)
        td1_1.appendChild(label)
        td1_1.appendChild(document.createElement('br'))
    }
    tr1.appendChild(td1_1)
    //对于某一周
    for(var i=0;i<monthArr.length;i++){
        let weekdays=[]
        //j 对于某一周第j天
        for(var j=0;j<monthArr[i].length;j++){
            let thisTd=document.createElement('td')
            let title=document.createElement('h6')

            
            let tesk=document.createElement('p')
            let schedule=document.createElement('p')
            schedule.setAttribute('class','schedule')
            //对于每日
            if(j){
                let todayStr=monthArr[i][j][0]
                let todayTodos=getThisDayTodo(todayStr,data)
                let todaySchedule=getThisDaySchedule(todayStr,data)
                //将日期h6添加到td中
                // let tdClassKind=''
                title.append(document.createTextNode(todayStr))
                thisTd.appendChild(title)
                thisTd.setAttribute('id',todayStr)
                //将周几添加到单元格td的类名属性
                let weekClassArr=['sunday','monday','tuesday','wednesday','thursday','firday','saturday','sunday']
                let tdClassKind=weatherStarAtSunday?weekClassArr[j-1]:weekClassArr[j]
                tdClassKind+="  week "
                // thisTd.setAttribute{class,}
                //如果是本月日期
                if (monthArr[i][j][1]){
                //疑问 每周数组都是以周日开头的吗？
                //答 是的，内部使用时默认以周日开头
                    tdClassKind+='thisMonth'
                    thisTd.setAttribute('class',tdClassKind)
                    //对于今日每一条待办
                    //添加tesk段落
                    for(var dayTeskNumber=0;dayTeskNumber<todayTodos.length;dayTeskNumber++){
                        let input=document.createElement('input')
                        let label=document.createElement('label')
                        // 本条待办已完成



                        // <input data-line="1" type="checkbox" class="class=&quot;task-list-item-checkbox" id="2022-8i">
                        // <label for="2022-8i">111</label>
                        // ================
                        // <input data-line="1" type="checkbox" class="task-list-item-checkbox todo" id="8/1-0">
                        // <label for="8/1-0">成都市</label>
                        input.setAttribute('type','checkbox')
                        if(todayTodos[dayTeskNumber][0]){
                            input.setAttribute('checked','checked')
                            
                        }else{
                        //本条待办未完成
                            
                        }
                        input.setAttribute('id','D-'+todayStr+'-'+dayTeskNumber+'-monthTable')
                        label.setAttribute('for','D-'+todayStr+'-'+dayTeskNumber+'-monthTable')
                        label.append(document.createTextNode(todayTodos[dayTeskNumber][1]))
                        input.onclick=clickCheckChange

                        tesk.appendChild(input)
                        tesk.appendChild(label)
                        tesk.appendChild(document.createElement('br'))

                        
                        
                    }
                    if(settings.showTodo){thisTd.appendChild(tesk)}
                    
                    //添加schedule段落
                    if(todaySchedule.length){
                        for(var dayScheduleNumber=0;dayScheduleNumber<todaySchedule.length;dayScheduleNumber++){
                            let mydiv=document.createElement('div')
                            mydiv.innerText=todaySchedule[dayScheduleNumber][0]+'-'+todaySchedule[dayScheduleNumber][1]+':\t'+todaySchedule[dayScheduleNumber][4]
                            let thisClass='schedule  '+todaySchedule[dayScheduleNumber][3]
                            mydiv.setAttribute('class',thisClass)
                            schedule.appendChild(mydiv)
                            // schedule.appendChild(document.createElement('br'))
    
    
                            
                            
                        }
                        if(settings.showSchedule){thisTd.appendChild(schedule)}
                        
                    }
                    
                //反之非本月日期
                }else{
                //如果非本月日期
                    tdClassKind+='notthisMonth'
                    thisTd.setAttribute('class',tdClassKind)
                }
            }else{
            //对于周计划
                title.append(document.createTextNode(''+monthArr[i][j][1]))
                thisTd.appendChild(title)
                thisTd.setAttribute('class','weekNumber')
            }
            weekdays.push(thisTd)
        }
        //第一周加到tr1
        if(i==0){
            for(var k=0;k<weekdays.length;k++){
                tr1.appendChild(weekdays[k])
            }
            table.appendChild(tr1)
        }else{
            let thisTr=document.createElement('tr')
            for(var k=0;k<weekdays.length;k++){
                thisTr.appendChild(weekdays[k])
            }
            table.appendChild(thisTr)
        }
    }
    
    return table
}
//生成周计划需要的数据
//哪一年 第几周 据此生成一周七天的日期列表
//根据列表查询每日对应的todo 、log
//
function creatweekTable(year,weekNumber,settings){
    //默认标准是以周日起始
    //只选出一周七天日期，不标明每日是周几
    //周几需要结合是否周日开头、第几天来判断
    let data=app.workspace.activeLeaf.view.lastSavedData
    let weatherStarAtSunday=settings.weekStartDay=='周日'
    let weekArr=weekToDay(year,weekNumber+1,weatherStarAtSunday)
    let title=[]
    let plan=[]
    let log=[]
    title.push([year+'年 第'+weekNumber+'周'])
    for(var i=0;i<weekArr.length;i++){
        let weekday=['周日','周一','周二','周三','周四','周五','周六']
        let thisTitle=[weekArr[i][0]+'-'+weekday[weekArr[i][1]]]
        title.push(thisTitle)
    }
    plan.push(getThisWeekTodo(weekNumber,data))
    
    for(var j=0;j<weekArr.length;j++){
        var thisTodo=getThisDayTodo(weekArr[j][0],data)
        
        plan.push(thisTodo)
    }

    for(var k=0;k<weekArr.length;k++){
        var thisLog=getThisDayLog(weekArr[k][0],data)
        log.push(thisLog)
    }
    ThisWeekArr=[title,plan,log]
    
    
// +++++++++++++++++++++++++++++++
    //设置好基本的表格框架
    let weekTable=document.createElement('table')
    let weekTablethead=document.createElement('thead')
    let weekTabletbody=document.createElement('tbody')
    let tr1=document.createElement('tr')
    let tr2=document.createElement('tr')
    let tr3=document.createElement('tr')
    let tr2td0=document.createElement('td')
    let tr2td1=document.createElement('td')
    let tr2td2=document.createElement('td')
    let tr2td3=document.createElement('td')
    let tr2td4=document.createElement('td')
    let tr2td5=document.createElement('td')
    let tr2td6=document.createElement('td')
    let tr2td7=document.createElement('td')
    let tr2tds=[tr2td0,tr2td1,tr2td2,tr2td3,tr2td4,tr2td5,tr2td6,tr2td7]


    
    weekTable.setAttribute('class','techo-week-table techo')
    weekTable.setAttribute('id','W-'+year+'-'+weekNumber)
    tr1.setAttribute('class','firstLine')
    tr2.setAttribute('class','secondLine')
    tr3.setAttribute('class','thirdLine')

    weekTablethead.appendChild(tr1)
    
    weekTabletbody.appendChild(tr2)
    weekTabletbody.appendChild(tr3)

    weekTable.appendChild(weekTablethead)
    weekTable.appendChild(weekTabletbody)
    //++++++++++++++++++++++++++++++++
    //写入第一行标题
    // 待办 ：这里要重写 参考月历
    let tr1InnerHTML=''
    for(var l=0;l<title.length;l++){
        var thisDayWeekDay=weatherStarAtSunday?['sunday','monday','tuesday','wednesday','thursday','firday','saturday'][l-1]:['monday','tuesday','wednesday','thursday','firday','saturday','sunday'][l-1]
        
        if(l){
            splitTitle=title[l][0].split('-')
            tr1InnerHTML+="<td class='day "
            tr1InnerHTML+=thisDayWeekDay
            tr1InnerHTML+="  title'>"
            tr1InnerHTML+="<span class='left'>"+splitTitle[0]+"</span>"
            tr1InnerHTML+="<span class='right'>"+splitTitle[1]+"</span>"
            
            tr1InnerHTML+="</td>"
        }else{
            tr1InnerHTML+="<td class='week title'>"
            tr1InnerHTML+=title[l]
            tr1InnerHTML+="</td>"
        }
        

    }
    tr1.innerHTML=tr1InnerHTML
    //写入第二行计划
    //周计划
    let weakPlan=document.createElement('div')
    let dayPlan0=document.createElement('div')
    let dayPlan1=document.createElement('div')
    let dayPlan2=document.createElement('div')
    let dayPlan3=document.createElement('div')
    let dayPlan4=document.createElement('div')
    let dayPlan5=document.createElement('div')
    let dayPlan6=document.createElement('div')
    let dayPlan7=document.createElement('div')
    let weakPlans=[weakPlan,dayPlan0,dayPlan1,dayPlan2,dayPlan3,dayPlan4,dayPlan5,dayPlan6,dayPlan7]
    //对于每一天、周计划
    for(var mo=0;mo<8;mo++){
        // 对于每一项待办
        for(var no=0;no<plan[mo].length;no++){
            //<input data-line="1" type="checkbox" class="task-list-item-checkbox todo" id="2022/32-0">
            //<label for="2022/32-0">的VS</label>
            //<br>
            let input=document.createElement('input')
            let label=document.createElement('label')
            
            let id=mo==0?'W-'+year+'/'+weekNumber+'-'+no+'-weekTable':'D-'+weekArr[mo-1][0]+'-'+no+'-weekTable'
            
            
            input.setAttribute('type','checkbox')
            
            input.setAttribute('id',id)

            if(plan[mo][no][0]){
                input.setAttribute('checked','checked')
            }

            
            label.setAttribute('for',id)
            label.append(document.createTextNode(plan[mo][no][1]))

            input.onclick=clickCheckChange
                

            weakPlans[mo].appendChild(input)
            weakPlans[mo].appendChild(label)
            weakPlans[mo].appendChild(document.createElement('br'))
            tr2tds[mo].appendChild(weakPlans[mo])
        }
        //为每个单元格绑定属性
        if(weatherStarAtSunday){
            switch(mo){
                case 0:tr2tds[0].setAttribute('rowspan','2') ;
                    tr2tds[0].setAttribute('class','weak') ;
                break;
                case 1:tr2tds[1].setAttribute('class','sunday') ;
                break;
                case 2:tr2tds[2].setAttribute('class','monday') ;
                break;
                case 3:tr2tds[3].setAttribute('class','tuesday') ;
                break;
                case 4:tr2tds[4].setAttribute('class','wednesday') ;
                break;
                case 5:tr2tds[5].setAttribute('class','thursday') ;
                break;
                case 6:tr2tds[6].setAttribute('class','firday') ;
                break;
                case 7:tr2tds[7].setAttribute('class','saturday') ;
    
            }
        }else{
            switch(mo){
                case 0:tr2tds[0].setAttribute('rowspan','2') ;
                    tr2tds[0].setAttribute('class','weak') ;
                break;
                case 1:tr2tds[1].setAttribute('class','monday') ;
                break;
                case 2:tr2tds[2].setAttribute('class','tuesday') ;
                break;
                case 3:tr2tds[3].setAttribute('class','wednesday') ;
                break;
                case 4:tr2tds[4].setAttribute('class','thursday') ;
                break;
                case 5:tr2tds[5].setAttribute('class','firday') ;
                break;
                case 6:tr2tds[6].setAttribute('class','saturday') ;
                break;
                case 7:tr2tds[7].setAttribute('class','sunday') ;
    
            }
        }
    }
    //调成单元格顺序
    if(!weatherStarAtSunday){
        // let newtds=[]
        // newtds=[tr2tds[0],tr2tds[2],tr2tds[3],tr2tds[4],tr2tds[5],tr2tds[6],tr2tds[7],tr2tds[1]]
        // tr2tds=newtds
    }else{
        
    }
    for(var oo=0;oo<8;oo++){
        tr2.appendChild(tr2tds[oo])
    }
    
    //写入第三行log
    let tr3InnerHTML=''
    
    for(var n=0;n<log.length;n++){
        var thisDayWeekDay=weatherStarAtSunday?['sunday','monday','tuesday','wednesday','thursday','firday','saturday'][n]:['monday','tuesday','wednesday','thursday','firday','saturday','sunday'][n]
        
        tr3InnerHTML+="<td class='day "
        tr3InnerHTML+=thisDayWeekDay
        tr3InnerHTML+="  log' >"
        let logSVG=getSvgByLog(log[n],n,weatherStarAtSunday,settings)
        tr3InnerHTML+=logSVG
        tr3InnerHTML+="</td>"
    }
    tr3.innerHTML=tr3InnerHTML



    // rowspan='2' 
    return weekTable
}
function clickCheckChange(){
    let lastData=app.workspace.activeLeaf.view.lastSavedData
    let ids=this.id.split('-')
    let state=this.checked
    let str=this.labels[0].innerText
    
    let refreshData=''
    let todayReg
    if(ids[0]=='D'){
        todayReg=new RegExp('### \\s*'+ids[1]+'[^#]+','g')
        
    }else if(ids[0]=='W'){
        thisWeakNumber=ids[1].split('/')[1]
        todayReg=new RegExp('## \\s*'+thisWeakNumber+'\\s*[W|周][^#]+','g')
        
    }else if(ids[0]=='M'){
        thisMonthNumber=ids[1].split('/')[1]
        todayReg=new RegExp('# \\s*'+thisMonthNumber+'\\s*[M|月][^#]+','g')
    }
    //已办转未办
    if(state){
        refreshData=lastData.replace(todayReg,function(){
            let thisTodoReg=new RegExp('-\\s*\\[\\s*\\]\\s+('+str+')')
            let result=arguments[0].replace(thisTodoReg,"- [x] $1")
            return result
        })
    }else{
        //未办转已办
        refreshData=lastData.replace(todayReg,function(){
            let thisTodoReg=new RegExp('-\\s*\\[\\s*x\\s*\\]\\s+('+str+')')
            let result=arguments[0].replace(thisTodoReg,"- [ ] $1")
            return result
        })
    }
    app.vault.adapter.write(app.workspace.activeLeaf.view.file.name, refreshData)
    switch (ids[0]){
        case 'D':
            let daytodo=document.getElementById(ids[0]+'-'+ids[1]+'-'+ids[2]+'-weekTable')
            let daytodo1=document.getElementById(ids[0]+'-'+ids[1]+'-'+ids[2]+'-monthTable')
           if(daytodo) {daytodo.checked=state}
           if(daytodo1) {daytodo1.checked=state}
        break;
        case 'W':
        break;
        case 'M':
        break;
    }
}


//每日小计（表内） 每月小计（表脚）
function creatBillTable(year,monthNumber,settings){

    let data=app.workspace.activeLeaf.view.lastSavedData
    let table=document.createElement('table')
    let thead=document.createElement('thead')
    let tbody=document.createElement('tbody')
    let tfoot=document.createElement('tfoot')
    table.appendChild(thead)
    table.appendChild(tbody)
    table.appendChild(tfoot)
    //+++++++++++++++++++++++
    //打标签
    table.setAttribute('class','techo-bill-table techo')
    table.setAttribute('id','Bill-'+year+'-'+monthNumber)
    //++++++++++++++++++++++
    //准备数据

    let billArr=[]
    let thisMonthCost=0
    let thisMonthSalery=0
    let monthArr=monthToDay(year,monthNumber)
    //对于每一天 
    for(var daycount=0;daycount<monthArr.length;daycount++){
        let todayBill=[]
        let todayArr=[]
        let todayCose=0
        todayArr.push(monthArr[daycount])
        todayBill=getThisDayBill(monthArr[daycount],data)
        
        //对于每一天账目记录
        for(var count=0;count<todayBill.length;count++){
            todayCose+=Number(todayBill[count][0])
            if(todayBill[count][0]>0){
                thisMonthSalery+=Number(todayBill[count][0])
            }else{
                thisMonthCost+=Number(todayBill[count][0])
            }
            
        }
        
        todayArr.push(todayBill)
        

        todayArr.push(todayCose)
        billArr.push(todayArr)
    }
    
    // ++++++++++++++++++++++
    //绘制账单
    thead.innerHTML="<th class='bill-day'>日期</th><th class='bill-cost'>金额</th><th class='bill-class'>分类</th><th class='bill-note'>备注</th><th class='today-cost'>今日小计</th>"
    let tbodyInnerHTML=''
    //对于本月的每一天
    for(var i=0;i<billArr.length;i++){
        //如果有开销记录则
        if(billArr[i][1].length){
            
            //对于每一份开销记录
            for(var j=0;j<billArr[i][1].length;j++){
                
                if(j==0){
                    tbodyInnerHTML+="<tr class='hasBill'><td class='bill-day' rowspan='"
                    tbodyInnerHTML+=billArr[i][1].length
                    tbodyInnerHTML+="'>"
                    tbodyInnerHTML+=billArr[i][0]
                    tbodyInnerHTML+="</td>"
                }else{
                    tbodyInnerHTML+="<tr class='hasBill '>"
                }
                if(billArr[i][1][j][0]>0){
                    tbodyInnerHTML+="<td class='bill-cost  salery'>"
                }else{
                    tbodyInnerHTML+="<td class='bill-cost  cost' >"
                }
                
                tbodyInnerHTML+=billArr[i][1][j][0]
                tbodyInnerHTML+="</td>"

                tbodyInnerHTML+="<td class='bill-class'>"
                tbodyInnerHTML+=billArr[i][1][j][1]
                tbodyInnerHTML+="</td>"

                tbodyInnerHTML+="<td class='bill-note'>"
                tbodyInnerHTML+=billArr[i][1][j][2]
                tbodyInnerHTML+="</td>"
                //如果是当天第一天记录，将汇总小计写在这里
                if(j==0){
                    tbodyInnerHTML+="<td class='today-cost' rowspan='"
                    tbodyInnerHTML+=billArr[i][1].length
                    tbodyInnerHTML+="'>"
                    tbodyInnerHTML+=decimalNumber(billArr[i][2])
                    tbodyInnerHTML+="</td></tr>"
                }else{
                    tbodyInnerHTML+="</tr>"
                }
            }
        }else if(settings.showNobill){
        //没有开销记录
            tbodyInnerHTML+="<tr class='nobill'><td class='bill-day'>"
            tbodyInnerHTML+=billArr[i][0]
            tbodyInnerHTML+="</td><td class='bill-cost'> - </td><td class='bill-class'> - </td><td class='bill-note'> - </td><td class='today-cost'> - </td></tr>"
        }else{}
    }
    tbody.innerHTML=tbodyInnerHTML
    
    tfoot.innerHTML="<th colspan='2'> "+monthNumber+"月汇总</th><td>总收入："+decimalNumber(thisMonthSalery)+"</td><td>总支出："+decimalNumber(thisMonthCost)+"</td><td>合计："+decimalNumber(thisMonthCost+thisMonthSalery)+"</td>"




    return table
}
//将数字格式化 100.00000000096->100
//100.999999999999999 ->101
//2.856666666->2.86
function decimalNumber(number){
    if(String(number).indexOf('.')>=0){
        //有小数点
        let str=String(Math.round(number*100)/100).match(/^[+|-]?\d+\.\d{0,2}/)[0]
        let arr=str.split('')
        let length=str.indexOf('.')
        
        for(var i=arr.length-1;i>=length;i--){
            if(arr[i]=='0'||arr[i]=='.'){
               
                arr.pop()
            }else{
                
                break
            }
        }
        return arr.join('')
    }else{
        //无小数点
        let str=String(number).match(/^[+|-]?\d+/)[0]
        return str
    }
}
//根据年份、月份、data 绘制本月支出曲线
function creatBillShow(year,monthNumber,settings){
    // +++++++++++++++++
    // 准备数据

    let data=app.workspace.activeLeaf.view.lastSavedData

    let BillArr=[]
    let monthCost=0
    let monthSalary=0
    let monthSubtotal=0
    let monthArr=monthToDay(year,monthNumber)

    let maxCost=0
    let maxSalery=0
    //对于每一天 
    for(var daycount=0;daycount<monthArr.length;daycount++){
        
        
        let todayCost=0
        let todaySalary=0
        let todaySubtotal=0
        
        let todayBill=getThisDayBill(monthArr[daycount],data)
        //对于每一天账目记录
        for(var count=0;count<todayBill.length;count++){
            if(todayBill[count][0]>0){
                todaySalary+=Number(todayBill[count][0])
                monthSalary+=Number(todayBill[count][0])
                
            }else{
                todayCost+=Number(todayBill[count][0])
                monthCost+=Number(todayBill[count][0])
                

            }
            
        }
        


        maxSalery=todaySalary>maxSalery?todaySalary:maxSalery
        maxCost=(-todayCost)>maxCost?(-todayCost):maxCost
        
        
        todaySubtotal=todayCost+todaySalary
        
        BillArr.push([monthArr[daycount],decimalNumber(todayCost),decimalNumber(todaySalary),decimalNumber(todaySubtotal)])
        
        
    }

    let maxMoney=Math.max(maxSalery,maxCost)
    monthSubtotal=monthCost+monthSalary
    //BillArr
    //monthCost,  monthSalary,  monthSubtotal
    // maxCost maxSalery maxMoney
    
    // ++++++++++++++++++++
    // 修饰数据
    
    // ++++++++++++++++++++++++++++++
    // 绘图
    let width=settings.showBillWidth
    let height=width*4/9
    let result="<svg width="+width+"px  height="+height+"px  viewBox='0 0 900 400' style='background: var(--billShowBc);'> "
    
    result+="<rect width='860' height='340' x='20' y='20' fill='#ff880000' stroke='#000' />"
    result+="<line x1='40' y1='360' x2='840' y2='360'stroke='black' stroke-width='2' />"
    // result+="<polyline points='10,10 100,20 80,85 28,55' fill='#00000000' stroke='#000000' stroke-width='2'/>"
    let firstDayOfMonth=new Date(year,monthNumber-1,1)
    
    let starDay=firstDayOfMonth.getDay()
    let hstep=25
    let vstep=25
    let x0=20
    let y0=20
    let x1=800
    let y1=360
    let daysNumber=BillArr.length
    result+=draline(x0,y0,x1,y1,hstep,vstep,starDay,daysNumber)
    

    result+=draBill(BillArr,maxMoney,vstep,x0,y0+20,y1)
    result+="<text x='"
    result+=800
    result+="' y='"
    result+=45
    result+="' font-size='20' fill='var(--billDay)' >"
    result+=(year+'/'+monthNumber)
    result+="</text>"

// 看这里 添加本月合计消费多少 收入多少 结余多少
// monthCost,  monthSalary,  monthSubtotal
    result+="<text x='"
    result+=800
    result+="' y='"
    result+=80
    result+="' font-size='10' fill='var(--billDay)' >"
    result+="总支出："
    result+=decimalNumber(monthCost)
    result+="</text>"

    // ===================
    result+="<text x='"
    result+=800
    result+="' y='"
    result+=95
    result+="' font-size='10' fill='var(--billDay)' >"
    result+="总收入："
    result+=decimalNumber(monthSalary)
    result+="</text>"
    // ===============
    result+="<text x='"
    result+=800
    result+="' y='"
    result+=110
    result+="' font-size='10' fill='var(--billDay)' >"
    result+="总结余："
    result+=decimalNumber(monthSubtotal)
    result+="</text>"
    
    //starDay 输入本月内第一周开始那天 是几号？ 据此划分竖向的背景线
    
    // result+="<rect width='60' height='40' x='20' y='10' fill='#0088ff' />"
    // result+="<rect width='850' height='380' x='30' y='30' fill='#ff0000' />"
    
    result+=" </svg>"
    return result
    function  draline(x0,y0,x1,y1,hstep,vstep,starDay,daysNumber){
                        
        var lineresult=""
        var text=''
        var hlocated=0
        var vlocated=0
        var hnumber=parseInt((y1-y0)/hstep)
        var vnumber=parseInt((x1-x0)/vstep)
        // 横向
        for (var i=0;i<hnumber;i++){
            hlocated=hstep*i
            lineresult +="<line  x1='"
            lineresult +=x0
            lineresult +="'  y1='"
            lineresult +=(y1-hlocated)
            lineresult +="' x2='"
            lineresult +=x1
            lineresult +="' y2='"
            lineresult +=(y1-hlocated)
            //20*12=240
            if((i%10==0)|(i==0)){
                //非整点
                lineresult +="' stroke="
                lineresult +="var(--tenLine)"
                lineresult +=" stroke-width='1'/> \n"
            } else if(i%5==0){  //9,15,21
                lineresult +="' stroke="
                lineresult +="var(--fiveLine)"
                lineresult +=" stroke-width='1'/> \n"
            }else{//整点
                lineresult +="' stroke="
                lineresult +="var(--line)"
                lineresult +=" stroke-width='1'/> \n"
            }
        }
        // <line x1='35' y1='0' x2='35' y2='700' stroke='#333'  stroke-width='0.5'/>
        // 纵向
        for (var j=0;j<vnumber;j++){
            vlocated=vstep*j
            lineresult +="<line  x1='"
            lineresult +=(x0+vlocated)
            lineresult +="'  y1='"
            lineresult +=y0
            lineresult +="' x2='"
            lineresult +=(x0+vlocated)
            lineresult +="' y2='"
            lineresult +=y1
            //20*12=240
            if(j%7==(7-starDay)){
                //整周
                lineresult +="' stroke="
                lineresult +="var(--weekLine)"
                lineresult +=" stroke-width='1'/> \n"
            } else {
                lineresult +="' stroke="
                lineresult +="var(--line)"
                lineresult +=" stroke-width='1'/> \n"
            }
           

        }
        for(var k=0;k<daysNumber;k++){
            vlocated=vstep*k
            text+="<text x='"
            text+=(x0+vlocated)
            text+="' y='"
            text+=y1+15
            text+="' font-size='15' fill='var(--billDay)' text-anchor='middle'>"
            text+=(k+1)
            text+="</text> "

        }
        lineresult+=text
        // lineresult +="</svg>\n"

        return lineresult
    }
    function draBill(BillArr,maxMoney,vstep,x0,y0,y1){
        let BillAmend=[]
        for(var i=0;i<BillArr.length;i++){
            let todayBill=[]
            todayBill.push((i)*vstep+x0)
            todayBill.push(-(BillArr[i][1]/maxMoney)*(y1-y0))
            
            todayBill.push((BillArr[i][2]/maxMoney)*(y1-y0))
            todayBill.push((BillArr[i][3]/maxMoney)*(y1-y0))
            BillAmend.push(todayBill)
        }

        result=""
        let cost="<polyline points='"
        let salery="<polyline points='"
        let costtext=''
        let saleryText=''
        // text="<text x='10' y='50' font-size='4'  text-anchor='middle'>Hello_world</text> "
        for(var j=0;j<BillAmend.length;j++){
            cost+=BillAmend[j][0]+','+(y1-BillAmend[j][1])+'  '
            salery+=BillAmend[j][0]+","+(y1-BillAmend[j][2])+'  '

            if(settings.showBillLable){
                costtext+="<rect width='25' height='20' x='"
                costtext+=BillAmend[j][0]-12
                costtext+="' y='"
                costtext+=(y1-BillAmend[j][1])-27
                costtext+="' fill='var(--billCostLableBc)' rx='3' ry='8'/>"

                costtext+="<text x='"
                costtext+=BillAmend[j][0]
                costtext+="' y='"
                costtext+=(y1-BillAmend[j][1])-12
                costtext+="' font-size='15' fill='var(--billCostLable)'  text-anchor='middle'>"
                costtext+=BillArr[j][1]
                costtext+="</text> "


                saleryText+="<rect width='25' height='20' x='"
                saleryText+=BillAmend[j][0]-12
                saleryText+="' y='"
                saleryText+=(y1-BillAmend[j][2])-27
                saleryText+="' fill='var(--billSaleryLableBc)' rx='3' ry='8'/>"

                saleryText+="<text x='"
                saleryText+=BillAmend[j][0]
                saleryText+="' y='"
                saleryText+=(y1-BillAmend[j][2])-12
                saleryText+="' font-size='15'  fill='var(--billSaleryLable)' text-anchor='middle'>"
                saleryText+=BillArr[j][2]
                saleryText+="</text> "
            }
            
           
        }
        
        cost+="' fill='#00000000' stroke='var(--costLine)' stroke-width='2'/>"
        salery+="' fill='#00000000' stroke='var(--saleryLine)' stroke-width='2'/>"
        result+=cost
        result+=salery
        if(settings.showBillLable){
            result+=costtext
            result+=saleryText
        }
        return result
    }

}
function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}
// 将字符串转换成数字构成的数组
// "1,3-8,10"=>1,3,4,5,6,7,8,10
function getNumberByString(str,MorW,settings){
    var arr=str.split(',')
    var resultNumber=[]
    for(var i=0;i<arr.length;i++){
        //形如1-8
        if(/\d+-\d+/.test(arr[i])){
            var numbers=arr[i].match(/\d+/g).map(Number).sort()
            
            var starNumber=numbers[1]<numbers[0]?numbers[1]:numbers[0]
            var endNumber=numbers[1]>numbers[0]?numbers[1]:numbers[0]
            for(var j=starNumber;j<=endNumber;j++){
                resultNumber.push(j)
            }
        }else if(/\d+/.test(arr[i])){
            resultNumber.push(Number(arr[i]))
        }else if(/current/.test(arr[i])) {
            let today=new Date()
            
            switch(MorW){
                case 'M':
                    resultNumber.push(today.getMonth()+1)
                break;
                case 'W':
                    
                    let StarAtSunday=(settings.weekStartDay=='周日')?1:0
                    let weakNumber=todayWeekNumber(today,StarAtSunday)
                    resultNumber.push(weakNumber)
                break;
            }
        }else{
            console.log(arr[i]+'是个啥？')
        }
        
    }
    return resultNumber
}


// 定义函数，返回当前页面的文本
// 定义函数，统计当前页面需要的数据
// 定义函数，

class MyPlugin extends obsidian.Plugin {
    async onload() {
        await this.loadSettings();
        
        
        this.addStatusBarItem().setText('Status Bar Text');
        this.addCommand({
            id: 'open-sample-modal',
            name: 'Open Sample Modal',
            checkCallback: (checking) => {
                let leaf = this.app.workspace.activeLeaf;
                if (leaf) {
                    if (!checking) {
                        new SampleModal(this.app).open();
                    }
                    return true;
                }
                return false;
            }
        });
        
        this.registerMarkdownPostProcessor((el, ctx) => __awaiter(this, void 0, void 0, function* () {
            let sourcePathStr=ctx.sourcePath
            let weatherTechoReg=/techo\d{4}\S*\.md$/
            
            //如果是规定的标题格式则
            if(sourcePathStr.match(weatherTechoReg)){
                let settings=app.plugins.plugins["ObsidianPlugin-Techo"].settings
                let year=Number(sourcePathStr.match(/\d{4}/))
                
                if (el.querySelector('code.language-techo-month')) {
                    let commandArr=getNumberByString(el.innerText.replace(/\n\s*复制/,''),'M',settings)

                    for (var count=0;count<commandArr.length;count++){
                        let monthArr=monthToMoreDay(year,commandArr[count],(settings.weekStartDay=='周日'))
                        let table=creatMonthTable(year,commandArr[count],monthArr,settings)
                        
                        el.appendChild(table)
                    }
                    
                }else if(el.querySelector('code.language-techo-week')){
                //    console.log('更新了吗？')
                    let commandArr=getNumberByString(el.innerText.replace(/\n\s*复制/,''),'W',settings) 
                    
                    for (var count=0;count<commandArr.length;count++){
                        let table=creatweekTable(year,commandArr[count],settings)
                        el.appendChild(table)
                    }
                }else if(el.querySelector('code.language-techo-bill')){
                    let commandArr=getNumberByString(el.innerText.replace(/\n\s*复制/,''),'M',settings) 
                   
                    for (var count=0;count<commandArr.length;count++){
                        let table=creatBillTable(year,commandArr[count],settings)
                        el.appendChild(table)
                    }
                    
                }else if(el.querySelector('code.language-techo-showBill')){
                    let commandArr=getNumberByString(el.innerText.replace(/\n\s*复制/,''),'M',settings) 
                    
                    for (var count=0;count<commandArr.length;count++){
                        let SVG=document.createElement("svg")
                        SVG.setAttribute('class','techo-month-showBill  techo')
                        SVG.setAttribute('id','showBill-'+year+'-'+commandArr[count])
                        
                        SVG.innerHTML=creatBillShow(year,commandArr[count],settings)
                        el.appendChild(SVG)
                        
                    }
                }else if(document.getElementsByClassName('techo').length){
                    
                    
                    let monthTables=document.getElementsByClassName('techo-month-table')
                    let weakTables=document.getElementsByClassName('techo-week-table')
                    let billTables=document.getElementsByClassName('techo-bill-table')
                    let billSVGs=document.getElementsByClassName('techo-month-showBill')

                    for(var i=0;i<monthTables.length;i++){
                        
                        let id=monthTables[i].id.split('-')
                        let monthArr=monthToMoreDay(year,id[2],(settings.weekStartDay=='周日'))
                        monthTables[i].parentNode.replaceChild(creatMonthTable(year,id[2],monthArr,settings),monthTables[i])
                        el.innerHTML=''
                    }
                    for(var j=0;j<weakTables.length;j++){
                        
                        let id=weakTables[j].id.split('-')
                        //数字 32+1=33 字符串32+1=321
                        weakTables[j].parentNode.replaceChild(creatweekTable(year,Number(id[2]),settings),weakTables[j])
                    }
                    for(var k=0;k<billTables.length;k++){
                        
                        let id=billTables[k].id.split('-')
                        billTables[k].parentNode.replaceChild(creatBillTable(year,id[2],settings),billTables[k])
                    }
                    for(var l=0;l<billSVGs.length;l++){
                        
                        let id=billSVGs[l].id.split('-')
                        billSVGs[l].innerHTML=creatBillShow(year,id[2],settings)
                        // billSVGs[l].parentNode.replaceChild(creatBillShow(year,id[2],settings),billSVGs[l])

                    }
                }else{
                    el.innerHTML=''
                }
            }
            // var weatherTecho=
            
            
        }));

        // await this.loadOptions();
        this.addSettingTab(new SampleSettingTab(this.app, this));
        
        
    }
    async writeOptions(changeOpts) {
        this.settings.update((old) => (Object.assign(Object.assign({}, old), changeOpts(old))));
        await this.saveData(this.options);
    }
    
    loadProcessor() {
        this.registerMarkdownPostProcessor((el, ctx) => __awaiter(this, void 0, void 0, function* () {
            // Only process the frontmatter
            if (!el.querySelector('code.language-techo-weak')) {
                return;
            }
            const banner = document.createElement('div');
        }));
    }
    loadSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            this.settings = Object.assign({}, DEFAULT_SETTINGS, yield this.loadData());
        });
        
    }
    saveSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.saveData(this.settings);
        });
    }
    
}
// class SampleModal extends obsidian.Modal {
//     constructor(app) {
//         super(app);
//     }
//     onOpen() {
//         let { contentEl } = this;
//         contentEl.setText('Woah!');
//     }
//     onClose() {
//         let { contentEl } = this;
//         contentEl.empty();
//     }
// }
class SampleSettingTab extends obsidian.PluginSettingTab {
    constructor(app, plugin) {
        super(app, plugin);
        this.plugin = plugin;
    }
    saveSettings({ rerenderSettings = false, refreshViews = false } = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.plugin.saveData(this.plugin.settings);
           
            if (rerenderSettings) {
                this.display();
            }
        });
    }
    display() {
        const weekdays = ["周日","周一"];
        const weekBackground=["空白","横线","网格"]
        const hh="2"



        let { containerEl } = this;
        containerEl.empty();
        containerEl.createEl('h1', { text: 'techo设置' });
        containerEl.createEl('h2', { text: '周计划视图' });
        containerEl.createEl('h3', { text: '常规' });

        new obsidian.Setting(containerEl)
        .setName('每周第一天')
        .setDesc("当前值： "+this.plugin.settings.weekStartDay)

        .addDropdown(dropdown => {
            // dropdown.setValue(this.plugin.settings.weekStartDay)
            dropdown.addOption('周日','请选择')
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.weekStartDay= value;
                yield this.plugin.saveSettings();
            }))
            weekdays.forEach((arr) => {//day 数组元素 i索引
                dropdown.addOption(arr,arr);//实际值、显示
            })
        });

        new obsidian.Setting(containerEl)
        .setName('背景图案')
        .setDesc("当前值： "+this.plugin.settings.backPattern)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.backPattern= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption('空白','请选择')
            weekBackground.forEach((arr, i) => {//day 数组元素 i索引
                dropdown.addOption(arr, weekBackground[i]);//实际值、显示
            })
        });
        new obsidian.Setting(containerEl)
            .setName('时间轴svg宽度')
            .setDesc('输入数字，建议200')
            .addText(text => {
            text.inputEl.type = 'number';
            text.setValue(String(this.plugin.settings.svgWidth));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.svgWidth = val ? parseInt(val) : null;
                yield this.saveSettings();
            }));
        });
    //     "sumWidth":200,
    // "sumHeight":300,

        // .showNightSVG
        new obsidian.Setting(containerEl)
                .setName('是否显示夜间0~6时')
                .setDesc('')
                .addToggle(toggle => toggle
                    .setValue(this.plugin.settings.showNightSVG)
                    .onChange((val) => __awaiter(this, void 0, void 0, function* () {
                        this.plugin.settings.showNightSVG = val;
                        yield this.saveSettings({ rerenderSettings: true, refreshViews: true });
                    }
                )));
            
        new obsidian.Setting(containerEl)
            .setName('是否开启周计划统计')
            .setDesc('')
            .addToggle(toggle => toggle
                .setValue(this.plugin.settings.weatherWeekSum)
                .onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.weatherWeekSum = val;
                    yield this.saveSettings({ rerenderSettings: true, refreshViews: true });
                }
            )));
            
        if (this.plugin.settings.weatherWeekSum) {
            new obsidian.Setting(containerEl)
                .setName('统计类名')
                .setDesc('输入一个英文字母')
                .addText(text => {
                // text.inputEl.type = 'string';
                text.setValue(String(this.plugin.settings.weekClassName));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.weekClassName = val;
                    yield this.saveSettings();
                }));
            });
           
        }

        containerEl.createEl('h3', { text: '时间轴样式' });
        // +==========================\
        new obsidian.Setting(containerEl)
            .setName('是否改用点线形式的时间轴')
            .setDesc('默认使用圆角矩形样式的时间轴')
            .addToggle(toggle => toggle
                .setValue(this.plugin.settings.weatherUseLine)
                .onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.weatherUseLine = val;
                    yield this.saveSettings({ rerenderSettings: true, refreshViews: true });
                }
            )));
    // Embed banner height
        if (this.plugin.settings.weatherUseLine) {
            new obsidian.Setting(containerEl)
                .setName('点半径')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeLineRadius));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeLineRadius = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
            new obsidian.Setting(containerEl)
                .setName('线宽')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeLineWidth));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeLineWidth = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
            new obsidian.Setting(containerEl)
                .setName('虚线线型')
                .setDesc('用英文逗号分割数字。0代表实线，5,5代表每隔5像素画一段虚线')
                .addText(text => {
                text.setValue(String(this.plugin.settings.timeLineDash));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    var reg=/^\d+(,\d+)*$/
                    
                    this.plugin.settings.timeLineDash = reg.test(val) ?  val: this.plugin.settings.timeLineDash;
                    // this.plugin.settings.timeLineDash = reg.test(val)? val:this.plugin.settings.timeLineDash
                    // this.plugin.settings.timeLineDash = val? val:this.plugin.settings.timeLineDash
                    yield this.saveSettings();
                }));
            });
            new obsidian.Setting(containerEl)
                .setName('位置')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeLineLocated));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeLineLocated = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
        }else{
            new obsidian.Setting(containerEl)
                .setName('时间轴宽度')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeBarWidth));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeBarWidth = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
            new obsidian.Setting(containerEl)
                .setName('时间轴圆角')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeBarRadius));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeBarRadius = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
            new obsidian.Setting(containerEl)
                .setName('时间轴位置')
                .setDesc('输入数字，建议0-10')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.timeBarLocated));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.timeBarLocated = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
        }
        containerEl.createEl('h3', { text: '时间备注样式' });
        new obsidian.Setting(containerEl)
            .setName('宽度')
            .setDesc('输入数字，建议0-10')
            .addText(text => {
            text.inputEl.type = 'number';
            text.setValue(String(this.plugin.settings.timeNoteWidth));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeNoteWidth = val ? parseInt(val) : null;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('位置')
            .setDesc('输入数字，建议0-10')
            .addText(text => {
            text.inputEl.type = 'number';
            text.setValue(String(this.plugin.settings.timeNoteLocated));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeNoteLocated = val ? parseInt(val) : null;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('圆角')
            .setDesc('输入数字，建议0-10')
            .addText(text => {
            text.inputEl.type = 'number';
            text.setValue(String(this.plugin.settings.timeNoteRadius));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeNoteRadius = val ? parseInt(val) : null;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('描边宽度')
            .setDesc('输入数字，建议0-10')
            .addText(text => {
            text.inputEl.type = 'number';
            text.setValue(String(this.plugin.settings.timeNoteStrokewidth));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeNoteStrokewidth = val ? parseInt(val) : null;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('描边线型')
            .setDesc('用英文逗号分割数字。0代表实线，5,5代表每隔5像素画一段虚线，n代表无描边')
            .addText(text => {
            text.setValue(String(this.plugin.settings.timeNoteStrokeDash));
            text.setPlaceholder(`文本框提示文字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                var reg=/^\d+(,\d+)*$|n/
                this.plugin.settings.timeNoteStrokeDash = reg.test(val) ? val: this.plugin.settings.timeLineDash;
                yield this.saveSettings();
            }));
        });

        containerEl.createEl('h2', { text: '月历视图' });
        new obsidian.Setting(containerEl)
        .setName(("是否显示待办"))
        .setDesc(("在月历视图显示待办"))
        .addToggle((containerEl=>{containerEl.setValue(this.plugin.settings.showTodo),
            containerEl.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.showTodo= value;
                yield this.plugin.saveSettings();
            }))
        }))
        new obsidian.Setting(containerEl)
        .setName(("是否显示日程"))
        .setDesc(("在月历视图显示日程"))
        .addToggle((containerEl=>{containerEl.setValue(this.plugin.settings.showSchedule),
            containerEl.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.showSchedule= value;
                yield this.plugin.saveSettings();
            }))
        }))

        containerEl.createEl('h2', { text: '账单设置' });
        new obsidian.Setting(containerEl)
        .setName(("是否显示无开销日"))
        .setDesc(("开启后若当日无记录则显示--"))
        .addToggle((containerEl=>{containerEl.setValue(this.plugin.settings.showNobill),
            containerEl.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.showNobill= value;
                yield this.plugin.saveSettings();
            }))
        }))
        // showBillLable
        new obsidian.Setting(containerEl)
        .setName(("是否显示账单数据标签"))
        .setDesc(("绘制月账单折线图"))
        .addToggle((containerEl=>{containerEl.setValue(this.plugin.settings.showBillLable),
            containerEl.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.showBillLable= value;
                yield this.plugin.saveSettings();
            }))
        }))

        new obsidian.Setting(containerEl)
                .setName('账单统计图宽度')
                .setDesc('输入数字，建议1000')
                .addText(text => {
                text.inputEl.type = 'number';
                text.setValue(String(this.plugin.settings.showBillWidth));
                text.setPlaceholder(`文本框提示文字`);
                text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                    this.plugin.settings.showBillWidth = val ? parseInt(val) : null;
                    yield this.saveSettings();
                }));
            });
    }
}


module.exports = MyPlugin;