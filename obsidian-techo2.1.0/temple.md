---
tags: diary
exercise: 5
bilOut：0
---
yaml区域给该笔记打上`diary`标签，记录当日跑步几公里，账单合计支出多少。
随后可用dataview进行汇总



本文件是一个日常记录的模板，仅供参考
```techo
mode:W
time: currentF
type: log
filter: 
```
展示当前文件所在周的周历视图

```techo
mode:M
time: currentF
type: check
```
展示当前文件所在月的月打卡情况

# check
- [x] 早起
- [ ] 吃早饭
- [ ] 跑步
- [ ] 刷牙

这里是每日打卡
# schedule
这里写日程，就是那些指定在某天特定时间一定会发生的任务，如约会、上课、deadLine等。
这一块我还没想好怎样展示，因此对应的视图还没做
# todo
这里可以记录一些当日待办，也可以没有该内容

# log
这里记录当日的时间 也可以没有该内容
0000-0900 b 睡觉
0900-1000 y 吃饭洗漱通勤
1000-1200 w 工作
1200-1300 y 吃完
1330-1500 g 处理文件
...
在这里写的非规定格式的内容会被忽略
# bill
-5 a 早饭
-10 a 午饭
+100 t 差旅报销
-20.5 a 买零食
-.5 a 馒头 

# diarry
这里可以写一些日记 也可以没有该内容


```techo
mode:M
time: currentF
type: bill
filter: [a-c,e]
export: true
```
filter的值为一个正则表达式，对记录类别的单个字母进行筛选
[^d]表示除d以外的类别
[a-c,e]表示a、b、c、e的账单
上述代码块将只显示账单类别为a、b、c、e的账单，并将该账单导出

