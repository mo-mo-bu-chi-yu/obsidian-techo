var obsidian = require('obsidian');
var fs=require('fs')
class DefaultSetting {
    constructor() {

                
        this.starAtSunday=true;
        this.shoeBillStat=true;
        // 格式详见
        // http://momentjs.cn/docs/#/displaying/
        this.dailyNoteFormat="YYYYMMDD";
        this.weeklyNoteFormat="YYYY-[W]ww";  //2023-W10
        this.monthNoteFormat="YYYY-MM";
        this.sourcePath='techo';
        // a-c,z    时间、账本状态栏 统计
    }
};

// 数据解析器：将源数据解析为json格式
// svg模板
// svg绘图器,根据数据返回绘制的svg图
// 数据导出功能


// command sourceText  dataAnalyse drawSvg
// 定义类
// 生成示例
// 实例的方法返回一个新对象
// 最后drawSvg方法返回一个svg对象
// let command=new Command
// command.sourceText().dataAnalyse().drawSvg()

// 命令解析器，根据代码块拆分参数
class Techo{
    constructor(source,ctx,plugin){
        this.command=new Command(source,plugin,this)
        if(this.command.errMag){
            // 有错误返回错误渲染页面


            this.drawer=new Drawer(ctx,this)
            this.drawer.update(this.command.errMag)
        }else{
        // 无错误再继续建立绘图数据
            this.drawer=new Drawer(ctx,this)
            this.data=new Data(this.command,this.drawer,this)

        }
        // thie.drawer=newDrawer()
    }
}


class Command{
    constructor(source,plugin,parent){
        this.parent=parent
        this.basePath=app.vault.adapter.basePath.replace('\\{2}','//')
        this.commandTxt=source
        this.settings=plugin.settings
        // 必填项： time type
        // 默认filer不筛选 view为a
        this.time= ""
        this.type= ""
        this.mode= ""
        this.filter= ""
        this.errMag=""
        // this.export=false
        // W M  currentM currentW 

        let splitArr=source.split('\n').filter(thisArr=>/[mode|time|type|filter]\s*:\s*[^\n]+/.test(thisArr))
        for(var i=0;i<splitArr.length;i++){
            splitArr[i]=splitArr[i].split(/\s*:\s*/)
            this[splitArr[i][0].trim()]=splitArr[i][1].trim()
        }
        // test未定义 错误定义
        if(!this.mode){
            this.errMag+="mode未定义\n"
        }else if(!(/(M|W|D)/.test(this.mode))){
            this.errMag+=this.mode+"____mode只能为M/W/D\n"
        }
        
        if(!this.type){
            this.errMag+="type未定义\n"
        }else if(!(/^(log|bill|check|schedule)$/.test(this.type))){
            this.errMag+=this.type+"____type只能为log/bill/check/schedule\n"
            // mysql
        }
        if(!this.time){
            this.time+="time未定义\n"
        }else if(!/^(current|currentF)$/.test(this.time)){
            this.errMag+="time只能为current/currentF\n"
            // mysql
        }else if(/^currentF$/.test(this.time)&&/^[M|W|D]$/.test(this.mode)){
          
            let thisName=app.workspace.getActiveFile().basename
            let timeFormat={
                // this.dailyNoteFormat="YYYYMMDD";
                // this.weeklyNoteFormat="YYYY-[W]ww";  //2023-W10
                // this.monthNoteFormat="YYYY-MM";
                "M":this.settings.monthNoteFormat,
                "W":this.settings.weeklyNoteFormat,
                "D":this.settings.dailyNoteFormat
            }
            // [this.mode]
            this.currentFileMode=moment(thisName,timeFormat['D'],true).isValid()?"D":
                                moment(thisName,timeFormat['W'],true).isValid()?"W":
                                moment(thisName,timeFormat["M"],true).isValid()?"M":false
            if(this.currentFileMode===false){
                this.errMag+="当前文件名:"+thisName+"不是规定的月/周/日的时间格式,不可使用techo查询 "
            }else{
                this.currentTimeFormat=timeFormat[this.currentFileMode]
                let codeInFileCould={
                    "M":['M'],
                    "W":['W'],
                    "D":['D',"M","W"],
                }
                if(!codeInFileCould[this.currentFileMode].includes(this.mode)){
                    
                    this.errMag+="当前文件名:"+thisName+"不可生成"+this.mode+"级别的视图"
                }
            }

            // M->M  W->W  D->D/W/M
            // 当前文件权限  要求数据的权限超出权限报错  当日日记权限最高
           
            
        }
        this.export=/^true$/.test(this.export)
    }
}


// 数据搜集器：根据参数获取相应记录数据的源数据
// 创建时 输入 source settings=>
// 文件范围timeRegion: fileMap  202302M  202315W 20230228D currentM currentW currentD currentFile 
// 时间跨度：M W D
// 视图种类viewType：log bill schedule check diarry
// filter: 时间、账本类型 [小写字母组成的数组]   [a,c,d-k,n]
// 可视化方案：[a-z]    根据视图种类、时间跨度而定 2维表格
//          Wlog_a  Wlog_b  Mcheck_a
// 

class Data{
    constructor(command,drawer,parent){
        // async
        
        Number.prototype.padLeft = function (n,str){
            return Array(n-String(this).length+1).join(str||'0')+this;
        }
        this.parent=parent
        this.fileNames=[]
        this.fileTxts=[]
        this.headTxt=[]
        this.daysData=[]

        // 当前文件采用的时间格式
        let formatStr=this.parent.command.currentTimeFormat
        let formatDStr=this.parent.command.settings.dailyNoteFormat
        // 准备好要读取的文件名列表
        if(/^current$/.test(command.time)){
            switch(command.mode){
                case "M":{
                    let today=moment().date(1)

                    let daynumber=moment().daysInMonth()
                    for(var date=1;date<=daynumber;date++){
                        today=moment().date(date)
                        this.fileNames.push(today.format(formatDStr))
                    }
                    break
                }
                case "W":{
                    let dayArr=command.settings.starAtSunday?[0,1,2,3,4,5,6]:[1,2,3,4,5,6,7]
                    for(var j=0;j<7;j++){
                        this.fileNames.push(moment().day(dayArr[j]).format(formatDStr))
                    }
                    break
                }
                case "D":{
                    this.fileNames.push(moment().format(formatDStr))
                    break
                }
            }
            // 是当前日期就好办了
        }else{
            // 只考虑以下情况
            // D->DWM  W->W M->M
            let thisName=app.workspace.getActiveFile().basename
            // 确保当前文件名合法
            // if(moment(thisName,formatStr).isValid()){
            switch(command.mode){
                case "M":{
                    let today=moment(thisName,formatStr)
                    let daynumber=today.daysInMonth()
                    for(var date=1;date<=daynumber;date++){
                        today=today.date(date)
                        this.fileNames.push(today.format(formatDStr))
                    }
                    break
                }
                case "W":{
                    let dayArr=command.settings.starAtSunday?[0,1,2,3,4,5,6]:[1,2,3,4,5,6,7]
                    for(var j=0;j<7;j++){
                        this.fileNames.push(moment(thisName,formatStr).day(dayArr[j]).format(formatDStr))
                    }
                    break
                }
                case "D":{
                    this.fileNames.push(moment(thisName,formatStr).format(formatDStr))
                    break
                }
            }
        }
        // promise.all
        let allPromise=[]
        for(var i=0;i<this.fileNames.length;i++){
            allPromise[i]=new Promise((resolve,reject)=>{
                // if()
                command.settings.sourcePath=command.settings.sourcePath=="/"?"":command.settings.sourcePath
                let fullPath=command.basePath+'/'+command.settings.sourcePath+'/'+this.fileNames[i]+".md"
    
                fs.readFile(fullPath,'utf-8',function(err,dataStr){
                    if(err){
                        
                        reject('')
                    }else{
                        resolve(dataStr)
                    }
                })     
            })
        }
        // const promises = [promise1, promise2];
        Promise.allSettled(allPromise)
        .then((results) => results.forEach((result,resultIndex,all) => {
            let txt=result.status=="rejected"?result.reason:result.value
            this.fileTxts.push(txt)
            let thisHeadTxt=getTxtByHead(txt,command.type)
            this.headTxt.push(thisHeadTxt)
            let todayData=getDataByType(thisHeadTxt,command.type,resultIndex,all.length,this.fileNames[resultIndex])
            for(var i=0;i<todayData.length;i++){
                this.daysData.push(todayData[i])
            }
        }))
        .then(()=>{
           
            drawer.update(this.daysData)
        })
        function getTxtByHead(txt,head){
            let txtLines=txt.split('\n')
            let resultTxt=''
            let stat=0
            // 0 表示尚未遇到
            // 1 表示正在进行
            // 2 表示已经完成
            let targetStarReg=new RegExp('^#{1,6}\x20'+head+'$','i')
            let targetEndReg=new RegExp('^#{1,6}\x20[^\n]+$','i')
            txtLines.forEach(line=>{
                switch (stat){
                    case 0:
                    stat=Number(targetStarReg.test(line))
                    break
                    case 1:
                    targetEndReg.test(line)?stat+=1:resultTxt+=(line+'\n')
                    break
                    case 2:
                    break
                }
            })
            return resultTxt
        }
        function getDataByType(thisHeadTxt,dataType,todayIndex,dayNumber,todayStr){
            // 开头 元信息 表明数据出处、数据集中第几天、类型、可视化方案

            // 数据集类型：bill log schedule check diarry
            // 数据集天数：28.29，30，31，7，1
            // 该条数据是第几天？
            // 该条数据日期字符串

            // log
            // starTime endTime timeClass tineNote

            // bill
            // in/out number billclass billNote

            // schedule
            // planNote state

            // check
            // dataType(布尔值 数字)  colorStyle([a-z]26个渐变色方案)  habitName key
            
            // diarry
            // 原样接受 不做更改
            // +5.2 -2 5 .2

            if(dataType=='diarry'){
                return thisHeadTxt?[[dataType,  dayNumber-1, todayIndex, todayStr,thisHeadTxt]]:[]
            }
            const logReg=/(\d{4})-(\d{4})\x20+([a-z])\x20+([^\n]+)/
            const billReg=/([+-])([\d|\.]+)\x20+([a-z])\x20+([^\n]+)/
            const scheduleReg=/-\x20+\[\x20*([\x20|x])\x20*\]\x20+([^\n])+/
            // - [ ] svjhj sdvfv
            const checkReg=/-\x20+\[\x20*([\x20|x])\x20*\]\x20+([^\n]+)/
            // const diarryReg=/[\s\S]+/

            // const billReg
            let result=[]
            // 参数分别为获得的标题下原文  log  0  promise请求个数
            let splitLines=thisHeadTxt.split('\n')
            let typeStrToReg={
                "log":logReg,
                "bill": billReg,
                "schedule": scheduleReg,
                "check": checkReg
            }
            let currentReg=typeStrToReg[dataType]?typeStrToReg[dataType]:/([\s\S]+)/
            splitLines.forEach((element,index)=>{
                element.replace(currentReg,pushData)
            })

            function pushData(){
                let thisLog=[dataType,  dayNumber-1, todayIndex, todayStr]
                let n=arguments.length-2
                for(var i=1;i<n;i++){
                    thisLog.push(arguments[i])
                }
                result.push(thisLog)
                return
            }
            return result


        }
        
    }
    // 输入文件名 文件内容
    expotrData(txt,modeType){
        let command=this.parent.command

        let thisName=app.workspace.getActiveFile().basename
        command.settings.sourcePath=command.settings.sourcePath=="/"?"":command.settings.sourcePath
        let fullPath=command.basePath+'/'+command.settings.sourcePath+'/data/'+thisName+modeType+".txt"
        // fs.appendFile(‘<fileName>’,<contenet>,callbackFunction)

        let p=new Promise((resolve,reject)=>{
            fs.appendFile(fullPath,txt,function(err,dataStr){
                if(err){
                    reject(err)
                }else{
                    resolve(dataStr)
                }
            })
        })
        p.then(value => {
            new Notice(fullPath+'数据已导出')
          }, reason => {
            app.vault.createFolder(command.settings.sourcePath+'/data')
            new Notice('数据导出失败:'+reason)
          }); 
        // p.reject(err).then(resolved, rejected);
    }
    
}
function timeStrToHours(timeStr){
    let hours=Number(timeStr.slice(0,2))
    let minuts=Number(timeStr.slice(2,4))
    let allTimeHours=hours+Number((minuts/60).toFixed(2))
    return allTimeHours
}
function timeStrToMinutes(timeStr){
    let hours=Number(timeStr.slice(0,2))
    let minuts=Number(timeStr.slice(2,4))
    let allTimeMinutes=hours*60+minuts
    return allTimeMinutes
}

function timeNoteToHtml(timeNote){
    // 将可能存在的内部、外部连接、斜体、加粗、高亮转化
    return timeNote
}

SVGTemplate={
    // ###weekLine###
    // ###weekTitle###
    // ###weekNote###
    // <svg width='90vw' height='52.9vw' viewBox='0 0 6300 3700' class='techo' xmlns='http://www.w3.org/2000/svg'>
    // </svg>
    "weekLog":"<defs><clipPath id='weekmask'><rect x='0' y='100' width='6300' height='3600'/></clipPath></defs><g style='display:inline' clip-path='url(#weekmask)'>###weekLine###</g><path d='M0 100 h6300' class='cls-techoPowerLine' /><path d='M0 300 h6300' class='cls-techoNomalLine' /><path d='M0 500 h6300' class='cls-techoNomalLine' /><path d='M0 700 h6300' class='cls-techoNomalLine' /><path d='M0 900 h6300' class='cls-techoNomalLine' /><path d='M0 1100 h6300' class='cls-techoNomalLine' /><path d='M0 1300 h6300' class='cls-techoNomalLine' /><path d='M0 1500 h6300' class='cls-techoNomalLine' /><path d='M0 1700 h6300' class='cls-techoNomalLine' /><path d='M0 1900 h6300' class='cls-techoNomalLine' /><path d='M0 2100 h6300' class='cls-techoNomalLine' /><path d='M0 2300 h6300' class='cls-techoNomalLine' /><path d='M0 2500 h6300' class='cls-techoNomalLine' /><path d='M0 2700 h6300' class='cls-techoNomalLine' /><path d='M0 2900 h6300' class='cls-techoNomalLine' /><path d='M0 3100 h6300' class='cls-techoNomalLine' /><path d='M0 3300 h6300' class='cls-techoNomalLine' /><path d='M0 3500 h6300' class='cls-techoNomalLine' /><path d='M100 200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 0 v3700' class='cls-techoNomalLine' /><path d='M200 0 v3700' class='cls-techoNomalLine' /><path d='M300 0 v3700' class='cls-techoNomalLine' /><path d='M400 0 v3700' class='cls-techoNomalLine' /><path d='M500 0 v3700' class='cls-techoNomalLine' /><path d='M600 0 v3700' class='cls-techoNomalLine' /><path d='M700 0 v3700' class='cls-techoNomalLine' /><path d='M800 0 v3700' class='cls-techoNomalLine' /><path d='M900 0 v3700' class='cls-techoPowerLine' /><path d='M1000 0 v3700' class='cls-techoNomalLine' /><path d='M1100 0 v3700' class='cls-techoNomalLine' /><path d='M1200 0 v3700' class='cls-techoNomalLine' /><path d='M1300 0 v3700' class='cls-techoNomalLine' /><path d='M1400 0 v3700' class='cls-techoNomalLine' /><path d='M1500 0 v3700' class='cls-techoNomalLine' /><path d='M1600 0 v3700' class='cls-techoNomalLine' /><path d='M1700 0 v3700' class='cls-techoNomalLine' /><path d='M1800 0 v3700' class='cls-techoPowerLine' /><path d='M1900 0 v3700' class='cls-techoNomalLine' /><path d='M2000 0 v3700' class='cls-techoNomalLine' /><path d='M2100 0 v3700' class='cls-techoNomalLine' /><path d='M2200 0 v3700' class='cls-techoNomalLine' /><path d='M2300 0 v3700' class='cls-techoNomalLine' /><path d='M2400 0 v3700' class='cls-techoNomalLine' /><path d='M2500 0 v3700' class='cls-techoNomalLine' /><path d='M2600 0 v3700' class='cls-techoNomalLine' /><path d='M2700 0 v3700' class='cls-techoPowerLine' /><path d='M2800 0 v3700' class='cls-techoNomalLine' /><path d='M2900 0 v3700' class='cls-techoNomalLine' /><path d='M3000 0 v3700' class='cls-techoNomalLine' /><path d='M3100 0 v3700' class='cls-techoNomalLine' /><path d='M3200 0 v3700' class='cls-techoNomalLine' /><path d='M3300 0 v3700' class='cls-techoNomalLine' /><path d='M3400 0 v3700' class='cls-techoNomalLine' /><path d='M3500 0 v3700' class='cls-techoNomalLine' /><path d='M3600 0 v3700' class='cls-techoPowerLine' /><path d='M3700 0 v3700' class='cls-techoNomalLine' /><path d='M3800 0 v3700' class='cls-techoNomalLine' /><path d='M3900 0 v3700' class='cls-techoNomalLine' /><path d='M4000 0 v3700' class='cls-techoNomalLine' /><path d='M4100 0 v3700' class='cls-techoNomalLine' /><path d='M4200 0 v3700' class='cls-techoNomalLine' /><path d='M4300 0 v3700' class='cls-techoNomalLine' /><path d='M4400 0 v3700' class='cls-techoNomalLine' /><path d='M4500 0 v3700' class='cls-techoPowerLine' /><path d='M4600 0 v3700' class='cls-techoNomalLine' /><path d='M4700 0 v3700' class='cls-techoNomalLine' /><path d='M4800 0 v3700' class='cls-techoNomalLine' /><path d='M4900 0 v3700' class='cls-techoNomalLine' /><path d='M5000 0 v3700' class='cls-techoNomalLine' /><path d='M5100 0 v3700' class='cls-techoNomalLine' /><path d='M5200 0 v3700' class='cls-techoNomalLine' /><path d='M5300 0 v3700' class='cls-techoNomalLine' /><path d='M5400 0 v3700' class='cls-techoPowerLine' /><path d='M5500 0 v3700' class='cls-techoNomalLine' /><path d='M5600 0 v3700' class='cls-techoNomalLine' /><path d='M5700 0 v3700' class='cls-techoNomalLine' /><path d='M5800 0 v3700' class='cls-techoNomalLine' /><path d='M5900 0 v3700' class='cls-techoNomalLine' /><path d='M6000 0 v3700' class='cls-techoNomalLine' /><path d='M6100 0 v3700' class='cls-techoNomalLine' /><path d='M6200 0 v3700' class='cls-techoNomalLine' /><path d='M100 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M1000 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M1900 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M2800 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M3700 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M4600 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M5500 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><text x='50' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='50' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='50' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='50' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='50' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='50' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='50' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='50' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='50' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='50' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='50' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='50' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='50' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='50' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='50' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='50' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='50' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='50' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='950' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='950' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='950' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='950' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='950' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='950' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='950' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='950' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='950' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='950' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='950' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='950' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='950' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='950' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='950' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='950' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='950' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='950' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='1850' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='1850' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='1850' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='1850' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='1850' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='1850' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='1850' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='1850' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='1850' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='1850' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='1850' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='1850' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='1850' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='1850' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='1850' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='1850' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='1850' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='1850' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='2750' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='2750' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='2750' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='2750' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='2750' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='2750' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='2750' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='2750' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='2750' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='2750' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='2750' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='2750' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='2750' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='2750' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='2750' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='2750' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='2750' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='2750' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='3650' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='3650' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='3650' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='3650' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='3650' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='3650' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='3650' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='3650' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='3650' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='3650' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='3650' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='3650' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='3650' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='3650' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='3650' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='3650' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='3650' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='3650' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='4550' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='4550' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='4550' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='4550' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='4550' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='4550' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='4550' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='4550' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='4550' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='4550' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='4550' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='4550' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='4550' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='4550' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='4550' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='4550' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='4550' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='4550' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='5450' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='5450' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='5450' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='5450' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='5450' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='5450' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='5450' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='5450' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='5450' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='5450' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='5450' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='5450' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='5450' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='5450' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='5450' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='5450' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='5450' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='5450' y='3625' font-size='50'  text-anchor='middle'>23</text> ###weekTitle###<g style='display:inline' clip-path='url(#weekmask)'>###weekNote###</g>",
    // ###weekLine###
    // ###weekTitle###
    // ###weekNote###
    "palSvg":"<svg width='90vw' height='52.9vw' viewBox='0 0 6300 3700' class='techo' xmlns='http://www.w3.org/2000/svg'><defs><clipPath id='weekmask'><rect x='0' y='100' width='6300' height='3600'/></clipPath></defs><text x='0' y='80'  text-anchor='star' class='weekTitle'>88</text> <text x='450' y='80'  text-anchor='middle' class='weekTitle'>阴云密布</text> <text x='900' y='80'  text-anchor='end' class='weekTitle'>日</text> <g style='display:inline' clip-path='url(#weekmask)'>###weekLine###</g><path d='M0 100 h6300' class='cls-2' /><path d='M0 300 h6300' class='cls-1' /><path d='M0 500 h6300' class='cls-1' /><path d='M0 700 h6300' class='cls-1' /><path d='M0 900 h6300' class='cls-1' /><path d='M0 1100 h6300' class='cls-1' /><path d='M0 1300 h6300' class='cls-1' /><path d='M0 1500 h6300' class='cls-1' /><path d='M0 1700 h6300' class='cls-1' /><path d='M0 1900 h6300' class='cls-1' /><path d='M0 2100 h6300' class='cls-1' /><path d='M0 2300 h6300' class='cls-1' /><path d='M0 2500 h6300' class='cls-1' /><path d='M0 2700 h6300' class='cls-1' /><path d='M0 2900 h6300' class='cls-1' /><path d='M0 3100 h6300' class='cls-1' /><path d='M0 3300 h6300' class='cls-1' /><path d='M0 3500 h6300' class='cls-1' /><path d='M0 3700 h6300' class='cls-2' /><path d='M100 200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 0 v3700' class='cls-1' /><path d='M200 0 v3700' class='cls-1' /><path d='M300 0 v3700' class='cls-1' /><path d='M400 0 v3700' class='cls-1' /><path d='M500 0 v3700' class='cls-1' /><path d='M600 0 v3700' class='cls-1' /><path d='M700 0 v3700' class='cls-1' /><path d='M800 0 v3700' class='cls-1' /><path d='M900 0 v3700' class='cls-2' /><path d='M1000 0 v3700' class='cls-1' /><path d='M1100 0 v3700' class='cls-1' /><path d='M1200 0 v3700' class='cls-1' /><path d='M1300 0 v3700' class='cls-1' /><path d='M1400 0 v3700' class='cls-1' /><path d='M1500 0 v3700' class='cls-1' /><path d='M1600 0 v3700' class='cls-1' /><path d='M1700 0 v3700' class='cls-1' /><path d='M1800 0 v3700' class='cls-2' /><path d='M1900 0 v3700' class='cls-1' /><path d='M2000 0 v3700' class='cls-1' /><path d='M2100 0 v3700' class='cls-1' /><path d='M2200 0 v3700' class='cls-1' /><path d='M2300 0 v3700' class='cls-1' /><path d='M2400 0 v3700' class='cls-1' /><path d='M2500 0 v3700' class='cls-1' /><path d='M2600 0 v3700' class='cls-1' /><path d='M2700 0 v3700' class='cls-2' /><path d='M2800 0 v3700' class='cls-1' /><path d='M2900 0 v3700' class='cls-1' /><path d='M3000 0 v3700' class='cls-1' /><path d='M3100 0 v3700' class='cls-1' /><path d='M3200 0 v3700' class='cls-1' /><path d='M3300 0 v3700' class='cls-1' /><path d='M3400 0 v3700' class='cls-1' /><path d='M3500 0 v3700' class='cls-1' /><path d='M3600 0 v3700' class='cls-2' /><path d='M3700 0 v3700' class='cls-1' /><path d='M3800 0 v3700' class='cls-1' /><path d='M3900 0 v3700' class='cls-1' /><path d='M4000 0 v3700' class='cls-1' /><path d='M4100 0 v3700' class='cls-1' /><path d='M4200 0 v3700' class='cls-1' /><path d='M4300 0 v3700' class='cls-1' /><path d='M4400 0 v3700' class='cls-1' /><path d='M4500 0 v3700' class='cls-2' /><path d='M4600 0 v3700' class='cls-1' /><path d='M4700 0 v3700' class='cls-1' /><path d='M4800 0 v3700' class='cls-1' /><path d='M4900 0 v3700' class='cls-1' /><path d='M5000 0 v3700' class='cls-1' /><path d='M5100 0 v3700' class='cls-1' /><path d='M5200 0 v3700' class='cls-1' /><path d='M5300 0 v3700' class='cls-1' /><path d='M5400 0 v3700' class='cls-2' /><path d='M5500 0 v3700' class='cls-1' /><path d='M5600 0 v3700' class='cls-1' /><path d='M5700 0 v3700' class='cls-1' /><path d='M5800 0 v3700' class='cls-1' /><path d='M5900 0 v3700' class='cls-1' /><path d='M6000 0 v3700' class='cls-1' /><path d='M6100 0 v3700' class='cls-1' /><path d='M6200 0 v3700' class='cls-1' /><path d='M6300 0 v3700' class='cls-2' /><path d='M100 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M1000 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M1900 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M2800 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M3700 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M4600 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M5500 196 v3500' stroke-dasharray='8 192' class='cls-3'/><text x='50' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='50' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='50' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='50' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='50' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='50' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='50' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='50' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='50' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='50' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='50' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='50' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='50' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='50' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='50' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='50' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='50' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='50' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='950' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='950' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='950' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='950' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='950' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='950' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='950' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='950' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='950' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='950' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='950' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='950' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='950' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='950' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='950' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='950' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='950' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='950' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='1850' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='1850' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='1850' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='1850' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='1850' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='1850' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='1850' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='1850' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='1850' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='1850' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='1850' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='1850' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='1850' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='1850' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='1850' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='1850' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='1850' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='1850' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='2750' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='2750' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='2750' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='2750' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='2750' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='2750' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='2750' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='2750' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='2750' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='2750' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='2750' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='2750' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='2750' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='2750' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='2750' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='2750' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='2750' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='2750' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='3650' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='3650' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='3650' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='3650' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='3650' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='3650' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='3650' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='3650' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='3650' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='3650' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='3650' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='3650' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='3650' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='3650' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='3650' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='3650' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='3650' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='3650' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='4550' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='4550' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='4550' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='4550' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='4550' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='4550' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='4550' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='4550' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='4550' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='4550' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='4550' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='4550' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='4550' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='4550' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='4550' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='4550' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='4550' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='4550' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='5450' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='5450' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='5450' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='5450' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='5450' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='5450' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='5450' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='5450' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='5450' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='5450' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='5450' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='5450' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='5450' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='5450' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='5450' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='5450' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='5450' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='5450' y='3625' font-size='50'  text-anchor='middle'>23</text> ###weekTitle###<g style='display:inline' clip-path='url(#weekmask)'>###weekNote###</g></svg>",
    // ###logRing###
    // ###weather###
    // ###day###
    // ###logNote###
    "ringSvg":"###logRing###<circle cx='700' cy='400' r='240'  class='cls-techoNomalLine' /><circle cx='700' cy='400' r='200'  class='cls-techoNomalLine' /><circle cx='700' cy='400' r='190'  class='cls-techoNomalLine' /><text x='700' y='360' font-size='30' text-anchor='middle' font-family='Arial Black' >###weekDay###</text><text x='700' y='460' font-size='30' text-anchor='middle' font-family='Arial Black' >###day###</text><path  d='M00 	0  h1400 v800 h-1400 z'  class='cls-techoNomalLine'/><path  d='M648 	593  l-10	39'  class='cls-techoNomalLine'/><path  d='M600 	573  l-20	35'  class='cls-techoNomalLine'/><path  d='M559 	541  l-28	28'  class='cls-harderLine'/><path  d='M527 	500  l-35	20'  class='cls-techoNomalLine'/><path  d='M507 	452  l-39	10'  class='cls-techoNomalLine'/><path  d='M510 	400  l-50	0'  class='cls-techoPowerLine'/><path  d='M507 	348  l-39	-10'  class='cls-techoNomalLine'/><path  d='M527 	300  l-35	-20'  class='cls-techoNomalLine'/><path  d='M559 	259  l-28	-28'  class='cls-harderLine'/><path  d='M600 	227  l-20	-35'  class='cls-techoNomalLine'/><path  d='M648 	207  l-10	-39'  class='cls-techoNomalLine'/><path  d='M700 	210  l0	    -50'  class='cls-techoPowerLine'/><path  d='M752 	207  l10	-39'  class='cls-techoNomalLine'/><path  d='M800 	227  l20	-35'  class='cls-techoNomalLine'/><path  d='M841 	259  l28	-28'  class='cls-harderLine'/><path  d='M873 	300  l35	-20'  class='cls-techoNomalLine'/><path  d='M893 	348  l39	-10'  class='cls-techoNomalLine'/><path  d='M890 	400  l50	0'  class='cls-techoPowerLine'/><path  d='M893 	452  l39	10'  class='cls-techoNomalLine'/><path  d='M873 	500  l35	20'  class='cls-techoNomalLine'/><path  d='M841 	541  l28	28'  class='cls-harderLine'/><path  d='M800 	573  l20	35'  class='cls-techoNomalLine'/><path  d='M752 	593  l10	39'  class='cls-techoNomalLine'/><path  d='M700 	590  l0	    50'  class='cls-techoPowerLine'/>###logNote###",
    // ###replace###
    "tinyLineSVG":""
        
}
class Drawer{
    constructor(ctx,parent){
        this.parent=parent
        this.ctx=ctx;
        
    }
    
    Mlog(data){

        return 
    }
    Wlog(_this,data){
        // ###weekLine###
        // ###weekTitle###
        // ###weekNote###
        let weekLogTemplate=SVGTemplate.weekLog
        let svg=document.createElementNS('http://www.w3.org/2000/svg','svg'); 	
		// svg.setAttribute("style","width:90vw;height:52.9vw;");	
		svg.setAttribute("style","width:100%;");
		svg.setAttribute("viewBox","0 0 6300 3700");	
        svg.setAttribute("class","techo");	
        
        let weekdayName=_this.parent.command.settings.starAtSunday?["日","一","二","三","四","五","六"]:["一","二","三","四","五","六","日"]
        let BG1="<path d='M0 50 h900' class='weekdayTitleBG'/><path d='M900 50 h4500' class='workdayTitleBG'/><path d='M5400 50 h900' class='weekdayTitleBG'/>"
        let BG2="<path d='M0 50 h4500' class='workdayTitleBG'/><path d='M4500 50 h1800' class='weekdayTitleBG'/>"
        let weektitle=_this.parent.command.settings.starAtSunday?BG1:BG2
        let weekTimeLine=""
        let weekTimeNote=""

        // 生成标题
        for(var i=0;i<_this.parent.data.fileNames.length;i++){
            weektitle+="<text x='"+(900*i+450)+"' y='80'  text-anchor='middle' class='weekTitle'>"+_this.parent.data.fileNames[i]+"</text>"
        }
        // 生成时间轴
        for(var j=0;j<data.length;j++){
            let lineX=data[j][2]*900
            let textStarX=lineX+200
            let textEndX=(data[j][2]+1)*900

            let starTimeStr
            let endTimeStr
            if(Number(data[j][4])<Number(data[j][5])){
                starTimeStr=data[j][4]
                endTimeStr=data[j][5]
            }else{
                starTimeStr=data[j][5]
                endTimeStr=data[j][4]
            }
            let Timeclass=data[j][6]
            let timeNote=data[j][7]
            
            let starY=Math.round((timeStrToHours(starTimeStr)-5.5)*200)
            let endY=Math.round((timeStrToHours(endTimeStr)-5.5)*200)
            let timeNoteHtml=timeNoteToHtml(timeNote)
            // 露出才考虑绘制
            if(endY>100){
                starY=starY<100?100:starY
                weekTimeLine+="<line x1='"+(lineX+125)+"' y1='"+starY+"' x2='"+(lineX+125)+"' y2='"+endY+"' stroke='var(--"+Timeclass+"time)' stroke-width='50' />"
                weekTimeNote+="<foreignObject x='"+textStarX+"' y='"+starY+"' width='"+"700"+"' height='"+Math.abs(endY-starY)+"'><div xmlns='http://www.w3.org/1999/xhtml'>"+timeNoteHtml+"</div></foreignObject>"
            }
            

            // ###weekTitle###   ###weekLine###   ###weekNote###
            
            // <text x='0' y='80'  text-anchor='star' class='weekTitle'>88</text> 
            // <text x='450' y='80'  text-anchor='middle' class='weekTitle'>阴云密布</text> 
            // <text x='900' y='80'  text-anchor='end' class='weekTitle'>日</text> 


        }
        weekLogTemplate=weekLogTemplate.replace("###weekTitle###",weektitle)
        weekLogTemplate=weekLogTemplate.replace("###weekLine###",weekTimeLine)
        weekLogTemplate=weekLogTemplate.replace("###weekNote###",weekTimeNote)
        
        svg.innerHTML=weekLogTemplate
        
        if(_this.parent.command.export){
            let exportTxt="数据类型;数据天数;当前数据天数;数据来源文件名;起始时间;终止时间;时间类型;时间笔记;\n"
            for(var z=0;z<data.length;z++){
                exportTxt+=data[z].join(";")
                exportTxt+="\n"
            }
            _this.parent.data.expotrData(exportTxt,'Wlog')
        }
        return svg
    }
    Dlog(_this,data){
        let viewType=_this.parent.command.settings.dayLogView
        let switchView={
            "tinyLine":tinyLine,
            "ring":ring,
            "default":tinyLine
        }
        let thisView=switchView[viewType]?switchView[viewType]:switchView.default

        // dayLogView="tinyLine";
        // monthLogView="brickWall";
        // switch(){

        // }
        // 返回一个svg节点
        function ring(){
            let SVG=SVGTemplate.ringSvg
            let fileName=_this.parent.data.fileNames[0]
            let weekDay=['日','一','二','三','四','五','六'][moment(fileName).day()]
            let timeSort=0
            let ringSvg=""
            let noteSvg=""

            let resultSvg=document.createElementNS('http://www.w3.org/2000/svg','svg'); 	
            resultSvg.setAttribute("style","width:70vw;height:40vw;");
            resultSvg.setAttribute("viewBox","0 0 1400 800");	
            resultSvg.setAttribute("class","techo");	

            let countClassReg=new RegExp(_this.parent.command.filter)
            for(var i=0;i<data.length;i++){

                let starTimeStr
                let endTimeStr
                if(Number(data[i][4])<Number(data[i][5])){
                    starTimeStr=data[i][4]
                    endTimeStr=data[i][5]
                }else{
                    starTimeStr=data[i][5]
                    endTimeStr=data[i][4]
                }
                let starHour=timeStrToHours(starTimeStr)
                let endHour=timeStrToHours(endTimeStr)
                let timeClass=data[i][6]
                let note=data[i][7]
                let timeLast=Math.abs(endHour-starHour)
                let midTime=0.5*(starHour+endHour)

                
                if(countClassReg.test(timeClass)){
                    timeSort+=timeLast
                }
                ringSvg+="<circle  cx='700' cy='400' r='220' stroke='var(--"+timeClass+"time)' stroke-dasharray='"+((55*3.14159*timeLast)/3).toFixed(0)+",4000' stroke-dashoffset='-"+((55*3.14159*(starHour))/3).toFixed(2)+"' class='logRing'/>"

                let moveArry=timeToSlogonLoca(midTime)
                let X1=(700+moveArry[0]).toFixed(0)
                let Y1=(400+moveArry[1]).toFixed(0)
                let X2=(700+moveArry[2]).toFixed(0)
                let Y2=(400+moveArry[3]).toFixed(0)
                let dh=(moveArry[4]*100)
                let X3=Number(X2)+dh
                let endOrStar=moveArry[4]>0?"star":"end"
                
                noteSvg+="<path  d='M"+X1+" "+Y1+"	L"+X2+" "+Y2+" h"+dh+"'  class='redLine'/><text x='"+X3+"' y='"+Y2+"' font-size='30' text-anchor='"+endOrStar+"'  >"+note+"</text>"
                
                
                // timeclass\star\end\timenote
                // stroke\dash\offset
            }
            if(timeSort){
                ringSvg+="<circle  cx='700' cy='400' r='195' stroke='var(--SortTime)' stroke-dasharray='"+((65*3.14159*timeSort)/4).toFixed(0)+",4000'  class='countTime'/> "
            }
            
            ringSvg="<g transform='rotate(90,700,400)'>"+ringSvg+"</g>"
            SVG=SVG.replace("###logRing###",ringSvg)
            // SVG=SVG.replace("###weather###",dataObj.weather)
            
            // SVG=SVG.replace("###weather###","")
            SVG=SVG.replace("###weekDay###",weekDay)
            SVG=SVG.replace("###day###",fileName)
            SVG=SVG.replace("###logNote###",noteSvg)
            
            resultSvg.innerHTML=SVG

            function timeToSlogonLoca(hours){
                let r=240
                let alpha=hours*Math.PI/12
                let xPOrN=hours%24>=12?1:-1
                let yPOrN=hours%24<18&&hours%24>6?-1:1
                let thata=Math.abs(alpha%Math.PI-0.5*Math.PI)
                let r_thata=thata==0?r:r*thata/Math.sin(thata)
                let dx1=xPOrN*r*Math.cos(thata)
                let dy1=yPOrN*r*Math.sin(thata)
                let dx2=xPOrN*r_thata*Math.cos(thata)
                let dy2=yPOrN*r_thata*Math.sin(thata)
                let dx_dys=[dx1,dy1,dx2,dy2,xPOrN]
                return dx_dys
            }
            return resultSvg
        }
        function tinyLine(){
            let resultSvg=document.createElementNS('http://www.w3.org/2000/svg','svg'); 	
            // style="width:100%;" viewBox="0 0 1500 120" xmlns="http://www.w3.org/2000/svg" 
            // id="sorry_item_not_found">
  
            
            resultSvg.setAttribute("style","width:100%;");
            resultSvg.setAttribute("viewBox","0 0 1500 120");	
            resultSvg.setAttribute("class","techo");	
            // 线条全长1440=24*60  高60 所有留边30
            // let SVG=SVGTemplate.tinyLineSVG
            let top=30
            let left=30
            let bottom=30
            let right=30
            let SVG=''
            for(var i=0;i<data.length;i++){
                let starTimeStr=data[i][4]
                let endTimeStr=data[i][5]
                let timeClass=data[i][6]
                let starMin=timeStrToMinutes(starTimeStr)
                let endMin=timeStrToMinutes(endTimeStr)
                let timeLast=Math.abs(endMin-starMin).toFixed(0)
                let thisLineStarLocal=String(left+starMin)+" "+String(top+30)
                SVG+="<path stroke-width='60'  d='m"+thisLineStarLocal+" h"+timeLast+"'  stroke=var(--"+timeClass+"time) fill='#00000000' />"
            }
            // <rect width="1440" height="60" x="20" y="10" fill="#ff8800" stroke='#fff' stroke-width='5' />
            SVG+="<rect width='1440' height='60' x='"+left+"' y='"+top+"' stroke='#888' fill='#00000000' stroke-width='2' />"
            // 3 6 9 12 15 18 21  添加刻度线
            let rulerY=15
            let timeRulerArr=[3,6,9,12,15,18,21]
            for(var j=0;j<timeRulerArr.length;j++){
                let lineWidth=j%2==0?"3":"3"
                let x=timeRulerArr[j]*60+left
                // --tinyBigHour:#f00;
                // --tinyNoemalHour:#888;
                let color=j%2?"var(--tinyBigHour)":"var(--tinyNoemalHour)"
                SVG+="<path stroke-width='"+lineWidth+"'  d='m"+x+" 20 v80'  stroke='"+color+"' fill='#00000000' /> "
            }
            // SVG+="<path stroke-width='1'  d='m180 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='3'  d='m360 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='1'  d='m 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='3'  d='m 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='1'  d='m 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='3'  d='m 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG+="<path stroke-width='1'  d='m 30 v60'  stroke='#f00' fill='#00000000' /> "
            // SVG=SVG.replace("###weekDay###",'')
            // SVG=SVG.replace("###day###",'')
            // SVG=SVG.replace("###logNote###",'')
            resultSvg.innerHTML=SVG
            return resultSvg
        }
        if(_this.parent.command.export){
            let exportTxt="数据类型;数据天数;当前数据天数;数据来源文件名;起始时间;终止时间;时间类型;时间笔记;\n"
            let arry1=[]
            for(var z=0;z<data.length;z++){
                exportTxt+=data[z].join(";")
                exportTxt+="\n"
            }
            _this.parent.data.expotrData(exportTxt,'Dlog')
        }
        return thisView()
    }
    bill(_this,data){
        let countClassReg=new RegExp(_this.parent.command.filter)

        data=data.filter(element=>{
            return countClassReg.test(element[6])
        })

        
        let billTable=document.createElement('table')
        billTable.setAttribute('class','techo-bill')

        let inBill=0
        let outBill=0

        const head = billTable.createEl("thead").createEl("tr");
        head.createEl("th", { text: '日期' })
        head.createEl("th", { text: '收支' })
        head.createEl("th", { text: '金额' })
        head.createEl("th", { text: '类型' })
        head.createEl("th", { text: '备注' })

        const body = billTable.createEl("tbody");
        for (let i = 0; i < data.length; i++){
            const row = body.createEl("tr");
            for (let j = 0; j < 5; j++) { 
                // j 0,1,2,3,4
                //   3,4,5,6,7
                row.createEl("td", { text: data[i][(j+3)] });
            } 
            if(data[i][4]=='+'){
                inBill+=Number(data[i][5])
            }else{
                outBill+=Number(data[i][5])
            }
        }
        // const summaryRow=body.createEl("tr").createEl("td", { text: "收："+inBill+"\n支："+outBill+"\n合："+(inBill-outBill)})
        const summaryRow=body.createEl("tr").createEl("td")
        summaryRow.setAttribute('colspan','5')
        summaryRow.setAttribute('class','summary')
        // , { text: "收："+inBill+"\n支："+outBill+"\n合："+(inBill-outBill)}
        summaryRow.createEl("p",{text: "收："+inBill.toFixed(2)})
        summaryRow.createEl("p",{text: "支："+outBill.toFixed(2)})
        summaryRow.createEl("p",{text: "合："+(inBill-outBill).toFixed(2)})
        
        if(_this.parent.command.export){
            let modeType=data[0][1]>27?"Mbill":
                        data[0][1]>5?"Wbill":"Dbill"

            let exportTxt="数据类型;数据天数;当前数据天数;数据来源文件名;收支类型;金额;类型;备注;\n"
            
            for(var z=0;z<data.length;z++){
                exportTxt+=data[z].join(";")
                exportTxt+="\n"
            }
            _this.parent.data.expotrData(exportTxt,modeType)
        }
        return billTable
    }
    Mcheck(_this,data){
        
        let formatData={}
        let thisMonthDayIndex=_this.parent.data.fileNames.length
        // 建立空数组时记得减一
        // let habitNames=[]
        // let habitCheck=[]
        // for(var j=0;j<thisMonthDayNumber;j++){

        // }
        for(var i=0;i<data.length;i++){
            let thisHabit=data[i][5].trim()
            if(!formatData[thisHabit]){
                formatData[thisHabit]=new Array(thisMonthDayIndex).fill(false)
            }
            formatData[thisHabit][data[i][2]]=/\S/.test(data[i][4])
        }
        // 获取第一天是星期几 0-6  日-天
        let settings=_this.parent.command.settings
        let dayOfFirst=moment(_this.parent.data.fileNames[0],settings.dailyNoteFormat).days()
        // 遇到当日周数为此的跳远一点
        
        let svgTxt=""
        let jumpLogo=settings.starAtSunday?0:1
        // 遍历对象属性
        let lines=0
        for(var habit in formatData){
            let starX=45
            let thisLineY=lines*12+10
            svgTxt+="<text x='"+(starX+5)+"' y='"+(thisLineY+8)+"' font-size='10'  text-anchor='end'>"+habit+"</text> "
            
            for(let j=0;j<formatData[habit].length;j++){
                let gap=(dayOfFirst+j)%7==jumpLogo?15:12
                
                starX+=gap
                let color=formatData[habit][j]?"var(--done)":"var(--undo)"
                svgTxt+="<rect width='10' height='10' x='"+starX+"' y='"+thisLineY+"' fill='"+color+"'  stroke-width='0' />"
                
            }
            lines++
            
        }
        let svg=document.createElementNS('http://www.w3.org/2000/svg','svg'); 	
        // svg.setAttribute("style","width:90vw;height:52.9vw;");	
        svg.setAttribute("style","width:100%;");
        svg.setAttribute("class","techo");	
        svg.setAttribute("viewBox","0 0 450 "+(lines*12+20));	
        svg.innerHTML=svgTxt
        
        if(_this.parent.command.export){
            let modeType="Mcheck"

            let exportTxt="习惯;每日打卡\n"
            
            for(var habit in formatData){
                exportTxt+=habit+";"
                exportTxt+=formatData[habit].join(";")
                exportTxt+="\n"
            }
            _this.parent.data.expotrData(exportTxt,modeType)
        }
    return  svg
    }
    // 待办
    schedule(_this,data){
        if(_this.parent.command.export){
            // console.log(data)
        }
        return 
    }
    diary(_this,data){

        return 
    }
    renderErr(errMag){
        let Err=document.createElement('div')
        let messages=errMag.split('\n')
        for(var i=0;i<messages.length;i++){
            Err.createEl("p",{text: messages[i]})

        }
        Err.setAttribute('class','techoErr')
        return Err
        
    }
    // 220行
    update(data){

        let Library ={
            // "Mlog":this.Mlog,
            "Wlog":this.Wlog,
            "Dlog":this.Dlog,

            "Mbill":this.bill,
            "Wbill":this.bill,
            "Dbill":this.bill,

            "Mcheck":this.Mcheck ,
            // "Wcheck":this.Mcheck ,
            // "Dcheck":this.Mcheck ,

            // "Wschedule": this.schedule,
            // "Mschedule": this.schedule,
            // "Dschedule": this.schedule,

            // "Wdiary": this.diary,
            // "Mdiary": this.diary,
            // "Ddiary": this.diary

           
        }
        // 定义一个报错页面
        let renderIsWorkable=true
        if( this.parent.command.errMag){
            renderIsWorkable=false
        }
        
        let call=this.parent.command.mode+this.parent.command.type
        if(data.length==0){
            this.parent.command.errMag+="未找到相关数据"
            renderIsWorkable=false
        }else if(!Library[call]){
            this.parent.command.errMag+="未定义的数据模式：{mode:"+this.parent.command.mode+", type:"+this.parent.command.type+"}"

            renderIsWorkable=false
        }
        let renderedView=renderIsWorkable?Library[call](this,data):this.renderErr(this.parent.command.errMag)
       
        
        
        this.ctx.setAttribute('class','techo-'+this.parent.command.type)
        this.ctx.appendChild(renderedView)

    }
}


// 模块：
//  命令解析模块
//  文件读取、写入模块
//  数据提取模块
//  绘图可视化模块
class MyPlugin extends obsidian.Plugin {
    constructor() {
        super(...arguments);
        this.codeMirror = (cm) => {
            cm.on('change', async () => {
                if (this.file.hasTodayNote()) {
                    this.plannerMD.checkIsDayPlannerEditing();
                }
            });
        };

        this.vault = this.app.vault;
        this.BaseFolder=[]
        let files=this.vault.adapter.files
        for(var el in files){
            if(files[el].type=="folder"){
                this.BaseFolder.push(files[el].realpath)
            }
        }

        }
    onunload() {
        console.log("Unloading techo plugin");
    }
    async onload() {
        console.log("Loading Day Planner plugin");

        this.settings = (await this.loadData()) || new DefaultSetting();
        // this.registerEvent(this.app.on("codemirror", this.codeMirror));
        
        
        this.registerMarkdownCodeBlockProcessor("techo",async (source,ctx)=>{
            new Techo(source,ctx,this)
            // console.log(techo)
        })
        this.addSettingTab(new MypluginSettingTab(this.app, this));
        
        
    }
    
    async updateSettings(settings) {
        Object.assign(this.settings, settings);
        this.updateRefreshSettings();
        await this.saveData(this.settings);
    }
}


class MypluginSettingTab extends obsidian.PluginSettingTab {
    constructor(app, plugin) {
        super(app, plugin);
        this.plugin = plugin;
    }
    display() {
        // 参考
        const folders=this.plugin.BaseFolder

        this.containerEl.empty()
        this.containerEl.createEl("h2", { text: "通用设置" })
        
        new obsidian.Setting(this.containerEl)
        .setName('是否以周日作为一周起始')
        .setDesc('介绍')
        .addToggle(toggle => toggle
        .setValue(this.plugin.settings.starAtSunday)
        .onChange((value)=>{
            this.plugin.settings.starAtSunday = value;
            this.plugin.saveData(this.plugin.settings);
        })
        )
        
        new obsidian.Setting(this.containerEl)
        .setName("日记名格式")
        .setDesc("定义日期格式,建议与日记插件保持一致,时间格式参见\n`http://momentjs.cn/docs/#/displaying/format/`")
        .addText((text) => text
        // .setValue('this.plugin.settings.dailyNoteFormat')
        .setPlaceholder(this.plugin.settings.dailyNoteFormat)
        .onChange(async (value) => {
            this.plugin.settings.dailyNoteFormat = value;
            this.plugin.saveData(this.plugin.settings);
        })
        );

        new obsidian.Setting(this.containerEl)
        .setName("周记名格式")
        .setDesc("定义周时间格式,建议与cleneder插件保持一致,时间格式同上")
        .addText((text) => text
        // .setValue('this.plugin.settings.dailyNoteFormat')
        .setPlaceholder(this.plugin.settings.weeklyNoteFormat)
        .onChange(async (value) => {
            this.plugin.settings.weeklyNoteFormat = value;
            this.plugin.saveData(this.plugin.settings);
        })
        );

        new obsidian.Setting(this.containerEl)
        .setName("月记名格式")
        .setDesc("定义月日期格式,时间格式同上")
        .addText((text) => text
        // .setValue('this.plugin.settings.dailyNoteFormat')
        .setPlaceholder(this.plugin.settings.monthNoteFormat)
        .onChange(async (value) => {
            this.plugin.settings.monthNoteFormat = value;
            this.plugin.saveData(this.plugin.settings);
        })
        );

        

        new obsidian.Setting(this.containerEl)
            .setName("forder:")
            .setDesc("选择文件夹")
            .addDropdown((dropdown) => {
            // dropdown.addOption(this.plugin.settings.sourcePath,this.plugin.settings.sourcePath);
            for(var i=0;i<folders.length;i++){
                dropdown.addOption(folders[i],folders[i]);

            }
            dropdown.setValue(this.plugin.settings.sourcePath);
            dropdown.onChange(async (value) => {
                this.plugin.settings.sourcePath = value;
                this.plugin.saveData(this.plugin.settings);
            });
        });

    }
}
module.exports = MyPlugin;