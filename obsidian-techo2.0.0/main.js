var obsidian = require('obsidian');
//必要的预先定义的值
const DEFAULT_SETTINGS = {
    "starAtSunday":true,
    "wlog":'手账时间轴',
    "dlog":'时间环',
    "timeSortClass":'a',
    "weatherCopy":true,
    "beforOutputLog":'Log',
    "afterOutputLog":'',
    "beforOutputBill":'Bill',
    "afterOutputBill":'',
};
let timeAccount={
    "weekLog":"<svg width='90vw' height='52.9vw' viewBox='0 0 6300 3700' class='techo' xmlns='http://www.w3.org/2000/svg'><defs><clipPath id='weekmask'><rect x='0' y='100' width='6300' height='3600'/></clipPath></defs><g style='display:inline' clip-path='url(#weekmask)'>###weekLine###</g><path d='M0 100 h6300' class='cls-techoPowerLine' /><path d='M0 300 h6300' class='cls-techoNomalLine' /><path d='M0 500 h6300' class='cls-techoNomalLine' /><path d='M0 700 h6300' class='cls-techoNomalLine' /><path d='M0 900 h6300' class='cls-techoNomalLine' /><path d='M0 1100 h6300' class='cls-techoNomalLine' /><path d='M0 1300 h6300' class='cls-techoNomalLine' /><path d='M0 1500 h6300' class='cls-techoNomalLine' /><path d='M0 1700 h6300' class='cls-techoNomalLine' /><path d='M0 1900 h6300' class='cls-techoNomalLine' /><path d='M0 2100 h6300' class='cls-techoNomalLine' /><path d='M0 2300 h6300' class='cls-techoNomalLine' /><path d='M0 2500 h6300' class='cls-techoNomalLine' /><path d='M0 2700 h6300' class='cls-techoNomalLine' /><path d='M0 2900 h6300' class='cls-techoNomalLine' /><path d='M0 3100 h6300' class='cls-techoNomalLine' /><path d='M0 3300 h6300' class='cls-techoNomalLine' /><path d='M0 3500 h6300' class='cls-techoNomalLine' /><path d='M100 200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 1800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 2800 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3000 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3200 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3400 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 3600 h6300' stroke-dasharray='800 100' class='cls-techoNomalLine' /><path d='M100 0 v3700' class='cls-techoNomalLine' /><path d='M200 0 v3700' class='cls-techoNomalLine' /><path d='M300 0 v3700' class='cls-techoNomalLine' /><path d='M400 0 v3700' class='cls-techoNomalLine' /><path d='M500 0 v3700' class='cls-techoNomalLine' /><path d='M600 0 v3700' class='cls-techoNomalLine' /><path d='M700 0 v3700' class='cls-techoNomalLine' /><path d='M800 0 v3700' class='cls-techoNomalLine' /><path d='M900 0 v3700' class='cls-techoPowerLine' /><path d='M1000 0 v3700' class='cls-techoNomalLine' /><path d='M1100 0 v3700' class='cls-techoNomalLine' /><path d='M1200 0 v3700' class='cls-techoNomalLine' /><path d='M1300 0 v3700' class='cls-techoNomalLine' /><path d='M1400 0 v3700' class='cls-techoNomalLine' /><path d='M1500 0 v3700' class='cls-techoNomalLine' /><path d='M1600 0 v3700' class='cls-techoNomalLine' /><path d='M1700 0 v3700' class='cls-techoNomalLine' /><path d='M1800 0 v3700' class='cls-techoPowerLine' /><path d='M1900 0 v3700' class='cls-techoNomalLine' /><path d='M2000 0 v3700' class='cls-techoNomalLine' /><path d='M2100 0 v3700' class='cls-techoNomalLine' /><path d='M2200 0 v3700' class='cls-techoNomalLine' /><path d='M2300 0 v3700' class='cls-techoNomalLine' /><path d='M2400 0 v3700' class='cls-techoNomalLine' /><path d='M2500 0 v3700' class='cls-techoNomalLine' /><path d='M2600 0 v3700' class='cls-techoNomalLine' /><path d='M2700 0 v3700' class='cls-techoPowerLine' /><path d='M2800 0 v3700' class='cls-techoNomalLine' /><path d='M2900 0 v3700' class='cls-techoNomalLine' /><path d='M3000 0 v3700' class='cls-techoNomalLine' /><path d='M3100 0 v3700' class='cls-techoNomalLine' /><path d='M3200 0 v3700' class='cls-techoNomalLine' /><path d='M3300 0 v3700' class='cls-techoNomalLine' /><path d='M3400 0 v3700' class='cls-techoNomalLine' /><path d='M3500 0 v3700' class='cls-techoNomalLine' /><path d='M3600 0 v3700' class='cls-techoPowerLine' /><path d='M3700 0 v3700' class='cls-techoNomalLine' /><path d='M3800 0 v3700' class='cls-techoNomalLine' /><path d='M3900 0 v3700' class='cls-techoNomalLine' /><path d='M4000 0 v3700' class='cls-techoNomalLine' /><path d='M4100 0 v3700' class='cls-techoNomalLine' /><path d='M4200 0 v3700' class='cls-techoNomalLine' /><path d='M4300 0 v3700' class='cls-techoNomalLine' /><path d='M4400 0 v3700' class='cls-techoNomalLine' /><path d='M4500 0 v3700' class='cls-techoPowerLine' /><path d='M4600 0 v3700' class='cls-techoNomalLine' /><path d='M4700 0 v3700' class='cls-techoNomalLine' /><path d='M4800 0 v3700' class='cls-techoNomalLine' /><path d='M4900 0 v3700' class='cls-techoNomalLine' /><path d='M5000 0 v3700' class='cls-techoNomalLine' /><path d='M5100 0 v3700' class='cls-techoNomalLine' /><path d='M5200 0 v3700' class='cls-techoNomalLine' /><path d='M5300 0 v3700' class='cls-techoNomalLine' /><path d='M5400 0 v3700' class='cls-techoPowerLine' /><path d='M5500 0 v3700' class='cls-techoNomalLine' /><path d='M5600 0 v3700' class='cls-techoNomalLine' /><path d='M5700 0 v3700' class='cls-techoNomalLine' /><path d='M5800 0 v3700' class='cls-techoNomalLine' /><path d='M5900 0 v3700' class='cls-techoNomalLine' /><path d='M6000 0 v3700' class='cls-techoNomalLine' /><path d='M6100 0 v3700' class='cls-techoNomalLine' /><path d='M6200 0 v3700' class='cls-techoNomalLine' /><path d='M100 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M1000 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M1900 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M2800 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M3700 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M4600 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><path d='M5500 196 v3500' stroke-dasharray='8 192' class='cls-techoDotLine'/><text x='50' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='50' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='50' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='50' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='50' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='50' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='50' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='50' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='50' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='50' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='50' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='50' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='50' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='50' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='50' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='50' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='50' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='50' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='950' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='950' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='950' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='950' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='950' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='950' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='950' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='950' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='950' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='950' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='950' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='950' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='950' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='950' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='950' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='950' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='950' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='950' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='1850' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='1850' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='1850' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='1850' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='1850' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='1850' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='1850' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='1850' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='1850' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='1850' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='1850' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='1850' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='1850' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='1850' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='1850' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='1850' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='1850' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='1850' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='2750' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='2750' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='2750' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='2750' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='2750' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='2750' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='2750' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='2750' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='2750' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='2750' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='2750' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='2750' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='2750' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='2750' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='2750' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='2750' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='2750' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='2750' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='3650' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='3650' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='3650' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='3650' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='3650' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='3650' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='3650' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='3650' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='3650' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='3650' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='3650' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='3650' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='3650' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='3650' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='3650' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='3650' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='3650' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='3650' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='4550' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='4550' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='4550' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='4550' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='4550' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='4550' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='4550' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='4550' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='4550' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='4550' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='4550' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='4550' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='4550' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='4550' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='4550' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='4550' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='4550' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='4550' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='5450' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='5450' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='5450' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='5450' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='5450' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='5450' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='5450' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='5450' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='5450' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='5450' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='5450' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='5450' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='5450' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='5450' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='5450' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='5450' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='5450' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='5450' y='3625' font-size='50'  text-anchor='middle'>23</text> ###weekTitle###<g style='display:inline' clip-path='url(#weekmask)'>###weekNote###</g></svg>",
    "palSvg":"<svg width='90vw' height='52.9vw' viewBox='0 0 6300 3700' class='techo' xmlns='http://www.w3.org/2000/svg'><defs><clipPath id='weekmask'><rect x='0' y='100' width='6300' height='3600'/></clipPath></defs><text x='0' y='80'  text-anchor='star' class='weekTitle'>88</text> <text x='450' y='80'  text-anchor='middle' class='weekTitle'>阴云密布</text> <text x='900' y='80'  text-anchor='end' class='weekTitle'>日</text> <g style='display:inline' clip-path='url(#weekmask)'>###weekLine###</g><path d='M0 100 h6300' class='cls-2' /><path d='M0 300 h6300' class='cls-1' /><path d='M0 500 h6300' class='cls-1' /><path d='M0 700 h6300' class='cls-1' /><path d='M0 900 h6300' class='cls-1' /><path d='M0 1100 h6300' class='cls-1' /><path d='M0 1300 h6300' class='cls-1' /><path d='M0 1500 h6300' class='cls-1' /><path d='M0 1700 h6300' class='cls-1' /><path d='M0 1900 h6300' class='cls-1' /><path d='M0 2100 h6300' class='cls-1' /><path d='M0 2300 h6300' class='cls-1' /><path d='M0 2500 h6300' class='cls-1' /><path d='M0 2700 h6300' class='cls-1' /><path d='M0 2900 h6300' class='cls-1' /><path d='M0 3100 h6300' class='cls-1' /><path d='M0 3300 h6300' class='cls-1' /><path d='M0 3500 h6300' class='cls-1' /><path d='M0 3700 h6300' class='cls-2' /><path d='M100 200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 1800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 2800 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3000 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3200 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3400 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 3600 h6300' stroke-dasharray='800 100' class='cls-1' /><path d='M100 0 v3700' class='cls-1' /><path d='M200 0 v3700' class='cls-1' /><path d='M300 0 v3700' class='cls-1' /><path d='M400 0 v3700' class='cls-1' /><path d='M500 0 v3700' class='cls-1' /><path d='M600 0 v3700' class='cls-1' /><path d='M700 0 v3700' class='cls-1' /><path d='M800 0 v3700' class='cls-1' /><path d='M900 0 v3700' class='cls-2' /><path d='M1000 0 v3700' class='cls-1' /><path d='M1100 0 v3700' class='cls-1' /><path d='M1200 0 v3700' class='cls-1' /><path d='M1300 0 v3700' class='cls-1' /><path d='M1400 0 v3700' class='cls-1' /><path d='M1500 0 v3700' class='cls-1' /><path d='M1600 0 v3700' class='cls-1' /><path d='M1700 0 v3700' class='cls-1' /><path d='M1800 0 v3700' class='cls-2' /><path d='M1900 0 v3700' class='cls-1' /><path d='M2000 0 v3700' class='cls-1' /><path d='M2100 0 v3700' class='cls-1' /><path d='M2200 0 v3700' class='cls-1' /><path d='M2300 0 v3700' class='cls-1' /><path d='M2400 0 v3700' class='cls-1' /><path d='M2500 0 v3700' class='cls-1' /><path d='M2600 0 v3700' class='cls-1' /><path d='M2700 0 v3700' class='cls-2' /><path d='M2800 0 v3700' class='cls-1' /><path d='M2900 0 v3700' class='cls-1' /><path d='M3000 0 v3700' class='cls-1' /><path d='M3100 0 v3700' class='cls-1' /><path d='M3200 0 v3700' class='cls-1' /><path d='M3300 0 v3700' class='cls-1' /><path d='M3400 0 v3700' class='cls-1' /><path d='M3500 0 v3700' class='cls-1' /><path d='M3600 0 v3700' class='cls-2' /><path d='M3700 0 v3700' class='cls-1' /><path d='M3800 0 v3700' class='cls-1' /><path d='M3900 0 v3700' class='cls-1' /><path d='M4000 0 v3700' class='cls-1' /><path d='M4100 0 v3700' class='cls-1' /><path d='M4200 0 v3700' class='cls-1' /><path d='M4300 0 v3700' class='cls-1' /><path d='M4400 0 v3700' class='cls-1' /><path d='M4500 0 v3700' class='cls-2' /><path d='M4600 0 v3700' class='cls-1' /><path d='M4700 0 v3700' class='cls-1' /><path d='M4800 0 v3700' class='cls-1' /><path d='M4900 0 v3700' class='cls-1' /><path d='M5000 0 v3700' class='cls-1' /><path d='M5100 0 v3700' class='cls-1' /><path d='M5200 0 v3700' class='cls-1' /><path d='M5300 0 v3700' class='cls-1' /><path d='M5400 0 v3700' class='cls-2' /><path d='M5500 0 v3700' class='cls-1' /><path d='M5600 0 v3700' class='cls-1' /><path d='M5700 0 v3700' class='cls-1' /><path d='M5800 0 v3700' class='cls-1' /><path d='M5900 0 v3700' class='cls-1' /><path d='M6000 0 v3700' class='cls-1' /><path d='M6100 0 v3700' class='cls-1' /><path d='M6200 0 v3700' class='cls-1' /><path d='M6300 0 v3700' class='cls-2' /><path d='M100 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M1000 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M1900 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M2800 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M3700 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M4600 196 v3500' stroke-dasharray='8 192' class='cls-3'/><path d='M5500 196 v3500' stroke-dasharray='8 192' class='cls-3'/><text x='50' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='50' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='50' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='50' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='50' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='50' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='50' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='50' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='50' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='50' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='50' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='50' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='50' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='50' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='50' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='50' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='50' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='50' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='950' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='950' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='950' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='950' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='950' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='950' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='950' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='950' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='950' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='950' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='950' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='950' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='950' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='950' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='950' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='950' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='950' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='950' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='1850' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='1850' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='1850' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='1850' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='1850' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='1850' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='1850' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='1850' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='1850' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='1850' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='1850' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='1850' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='1850' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='1850' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='1850' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='1850' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='1850' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='1850' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='2750' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='2750' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='2750' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='2750' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='2750' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='2750' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='2750' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='2750' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='2750' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='2750' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='2750' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='2750' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='2750' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='2750' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='2750' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='2750' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='2750' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='2750' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='3650' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='3650' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='3650' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='3650' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='3650' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='3650' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='3650' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='3650' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='3650' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='3650' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='3650' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='3650' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='3650' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='3650' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='3650' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='3650' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='3650' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='3650' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='4550' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='4550' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='4550' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='4550' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='4550' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='4550' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='4550' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='4550' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='4550' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='4550' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='4550' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='4550' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='4550' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='4550' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='4550' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='4550' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='4550' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='4550' y='3625' font-size='50'  text-anchor='middle'>23</text> <text x='5450' y='225' font-size='50'  text-anchor='middle'>6</text> <text x='5450' y='425' font-size='50'  text-anchor='middle'>7</text> <text x='5450' y='625' font-size='50'  text-anchor='middle'>8</text> <text x='5450' y='825' font-size='50'  text-anchor='middle'>9</text> <text x='5450' y='1025' font-size='50'  text-anchor='middle'>10</text> <text x='5450' y='1225' font-size='50'  text-anchor='middle'>11</text> <text x='5450' y='1425' font-size='50'  text-anchor='middle'>12</text> <text x='5450' y='1625' font-size='50'  text-anchor='middle'>13</text> <text x='5450' y='1825' font-size='50'  text-anchor='middle'>14</text> <text x='5450' y='2025' font-size='50'  text-anchor='middle'>15</text> <text x='5450' y='2225' font-size='50'  text-anchor='middle'>16</text> <text x='5450' y='2425' font-size='50'  text-anchor='middle'>17</text> <text x='5450' y='2625' font-size='50'  text-anchor='middle'>18</text> <text x='5450' y='2825' font-size='50'  text-anchor='middle'>19</text> <text x='5450' y='3025' font-size='50'  text-anchor='middle'>20</text> <text x='5450' y='3225' font-size='50'  text-anchor='middle'>21</text> <text x='5450' y='3425' font-size='50'  text-anchor='middle'>22</text> <text x='5450' y='3625' font-size='50'  text-anchor='middle'>23</text> ###weekTitle###<g style='display:inline' clip-path='url(#weekmask)'>###weekNote###</g></svg>",
    "ringSvg":"<svg width='70vw'  height='40vw' viewBox='0 0 1400 800' class='techo' xmlns='http://www.w3.org/2000/svg'>###logRing###<circle cx='700' cy='400' r='240'  class='cls-techoNomalLine' /><circle cx='700' cy='400' r='200'  class='cls-techoNomalLine' /><circle cx='700' cy='400' r='190'  class='cls-techoNomalLine' /><text x='700' y='360' font-size='30' text-anchor='middle' font-family='Arial Black' >###weather###</text><text x='700' y='460' font-size='60' text-anchor='middle' font-family='Arial Black' >###day###</text><path  d='M00 	0  h1400 v800 h-1400 z'  class='cls-techoNomalLine'/><path  d='M648 	593  l-10	39'  class='cls-techoNomalLine'/><path  d='M600 	573  l-20	35'  class='cls-techoNomalLine'/><path  d='M559 	541  l-28	28'  class='cls-harderLine'/><path  d='M527 	500  l-35	20'  class='cls-techoNomalLine'/><path  d='M507 	452  l-39	10'  class='cls-techoNomalLine'/><path  d='M510 	400  l-50	0'  class='cls-techoPowerLine'/><path  d='M507 	348  l-39	-10'  class='cls-techoNomalLine'/><path  d='M527 	300  l-35	-20'  class='cls-techoNomalLine'/><path  d='M559 	259  l-28	-28'  class='cls-harderLine'/><path  d='M600 	227  l-20	-35'  class='cls-techoNomalLine'/><path  d='M648 	207  l-10	-39'  class='cls-techoNomalLine'/><path  d='M700 	210  l0	    -50'  class='cls-techoPowerLine'/><path  d='M752 	207  l10	-39'  class='cls-techoNomalLine'/><path  d='M800 	227  l20	-35'  class='cls-techoNomalLine'/><path  d='M841 	259  l28	-28'  class='cls-harderLine'/><path  d='M873 	300  l35	-20'  class='cls-techoNomalLine'/><path  d='M893 	348  l39	-10'  class='cls-techoNomalLine'/><path  d='M890 	400  l50	0'  class='cls-techoPowerLine'/><path  d='M893 	452  l39	10'  class='cls-techoNomalLine'/><path  d='M873 	500  l35	20'  class='cls-techoNomalLine'/><path  d='M841 	541  l28	28'  class='cls-harderLine'/><path  d='M800 	573  l20	35'  class='cls-techoNomalLine'/><path  d='M752 	593  l10	39'  class='cls-techoNomalLine'/><path  d='M700 	590  l0	    50'  class='cls-techoPowerLine'/>###logNote###</svg>",
    
    'gatherLogData':function(source,goalData){
        let logType=''
        let logs=[]
        let plans=[]
        let hideLogs=[]

        let starTime=''
        let endTime=''
        let timeType=''
        let timeNote=''
        let alias=''
        
        source=source.split(/\n/)
        source=source.filter(hasData)
        for(var j=0;j<source.length;j++){
            let thisLog=[]
            if(/^\s*\d{4}/.test(source[j])){
                logType='log'
            }else if(/^\s*\/\d{4}/.test(source[j])){
                logType='plan'
            }else if(/^\s*\*\d{4}/.test(source[j])){
                logType='hideLog'
            }else{
                logType=''
            }
            // 2. 捕获数据 填入变量alias\starTime\endTime\timeType\timeNote
            // 如果有别名 记入别名并删除source[j]中的别名数据
            if(/【\S+】$/.test(source[j])){
                alias=source[j].match(/【(\S+)】$/)[1]
                source[j]=source[j].replace(/【\S+】$/,'')
            }else{
                alias=''
            }
            source[j].replace(/(\d{4})-(\d{4})\x20+([a-z])\x20([^\n]*)/,pushData)//获得当日记录
            thisLog=[starTime,endTime,timeType,timeNote,alias]
            // 3. 根据记录类别分别push到相应的logs\plans\hideLogs
            switch(logType){
                case 'log':
                    logs.push(thisLog)
                    break;
                case 'plan':
                    plans.push(thisLog)
                    break;
                case 'hideLog':
                    hideLogs.push(thisLog)
                    break;
                default:
                    console.log('未匹配的记录类型：')
                    console.log(source)
            }
        }
        goalData.logs=logs
        goalData.plans=plans
        goalData.hideLogs=hideLogs
        
        return goalData
        function hasData(element){
            let reg=/[\t|\x20{4}][\/|\*]?\d{4}-\d{4}\x20+[a-z]\x20+[^\n]+/
            let hasdate=reg.test(element)?true:false
            return hasdate
        }
        function pushData(match,p1,p2,p3,p4,offset,string){
            starTime=p1
            endTime=p2
            timeType=p3
            timeNote=p4
        }
    
    },
    'getWeekData':function(text){
        let days=text.split(/\n(?=[\<|\>]?@\d{2})/)
        var sunday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"0",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var monday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"1",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var tuesday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"2",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var wednesday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"3",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var thursday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"4",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var firday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"5",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        var saturday={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"6",
            "logs":[],
            "hideLogs":[],
            "plans":[],
        };
        let resultArry=timeAccount.pluginSettings.starAtSunday?[sunday,monday,tuesday,wednesday,thursday,firday,saturday]:[monday,tuesday,wednesday,thursday,firday,saturday,sunday];
        resultArry.dayNumber=days.length>7?7:days.length
        resultArry.type='weekLog'

        for(var i=0;i<resultArry.dayNumber;i++){
            days[i].replace(/^[\<|\>]?@(\d{2})\x20*(\S+)?/,function(match,p1,p2,offset,string){
                resultArry[i].day=p1
                resultArry[i].weather=p2?p2:""
                if(/^\<@\d{2}/.test(days[i])){
                    resultArry[i].month=-1
                }else if(/^\>@\d{2}/.test(days[i])){
                    resultArry[i].month=1
                }else{
                    resultArry[i].month=0
                }
                days[i]=days[i].replace(/^[\<|\>]?@(\d{2})[^\n]*\n/,'')
                timeAccount.gatherLogData(days[i],resultArry[i])
            })
        }
        return resultArry
    },
    'getdayData':function(text){
        var today={
            "day":"",
            "month":0,
            "weather":"",
            "weekday":"0",
            "logs":[],
            "hideLogs":[],
            "plans":[],
            "type":"dayLog"
        };
        text.replace(/^[\<|\>]?@(\d{2})\x20*(\S+)?/,function(match,p1,p2,offset,string){
            today.day=p1
            today.weather=p2?p2:""
            if(/^\<@\d{2}/.test(text)){
                today.month=-1
            }else if(/^\>@\d{2}/.test(text)){
                today.month=1
            }else{
                today.month=0
            }
            text=text.replace(/^[\<|\>]?@(\d{2})[^\n]*\n/,'')
            timeAccount.gatherLogData(text,today)
        })
        return today

    },
    "drwLog":function(dataObj){
        let settings=timeAccount.pluginSettings
        let SVG=null
        switch (dataObj.type){
            case "mPlan":

            break
            case "weekLog":
                switch(settings.wlog){
                    case "手账时间轴":
                        SVG=timeAccount.weekLog
                        let weekdayName=settings.starAtSunday?["日","一","二","三","四","五","六"]:["一","二","三","四","五","六","日"]
                        let BG1="<path d='M0 50 h900' class='weekdayTitleBG'/><path d='M900 50 h4500' class='workdayTitleBG'/><path d='M5400 50 h900' class='weekdayTitleBG'/>"
                        let BG2="<path d='M0 50 h4500' class='workdayTitleBG'/><path d='M4500 50 h1800' class='weekdayTitleBG'/>"

                        let weektitle=settings.starAtSunday?BG1:BG2
                        let weekTimeLine=""
                        let weekTimeNote=""
                        for(var weekdayNumber=0;weekdayNumber<dataObj.dayNumber;weekdayNumber++){
                            let lineX=900*weekdayNumber
                            let textStarX=900*weekdayNumber+200
                            let textEndX=900*(weekdayNumber+1)

                            weektitle+="<text x='"+lineX+"' y='80'  text-anchor='star' class='weekTitle'>"+dataObj[weekdayNumber].day+"</text> "
                            weektitle+="<text x='"+(lineX+450)+"' y='80'  text-anchor='middle' class='weekTitle'>"+dataObj[weekdayNumber].weather+"</text>"
                            weektitle+="<text x='"+(lineX+900)+"' y='80'  text-anchor='end' class='weekTitle'>"+weekdayName[weekdayNumber]+"</text> "
                            for(var logNumber=0;logNumber<dataObj[weekdayNumber].logs.length;logNumber++){
                                let starTimeStr=dataObj[weekdayNumber].logs[logNumber][0]
                                let endTimeStr=dataObj[weekdayNumber].logs[logNumber][1]
                                let Timeclass=dataObj[weekdayNumber].logs[logNumber][2]
                                let timeNote=dataObj[weekdayNumber].logs[logNumber][4]?dataObj[weekdayNumber].logs[logNumber][4]:dataObj[weekdayNumber].logs[logNumber][3]
                                
                                let starY=Math.round((timeStrToHours(starTimeStr)-5.5)*200)
                                let endY=Math.round((timeStrToHours(endTimeStr)-5.5)*200)
                                // console.log(starTimeStr+"->"+timeStrToHours(starTimeStr)+"->"+starY)
                                // console.log(endTimeStr+"->"+timeStrToHours(endTimeStr)+"->"+endY)
                                // console.log("===========")
                                let timeNoteHtml=timeNoteToHtml(timeNote)
                                if(endY>100){
                                    starY=starY<100?100:starY
                                    weekTimeLine+="<line x1='"+(lineX+125)+"' y1='"+starY+"' x2='"+(lineX+125)+"' y2='"+endY+"' stroke='var(--"+Timeclass+"time)' stroke-width='50' />"
                                    weekTimeNote+="<foreignObject x='"+textStarX+"' y='"+starY+"' width='"+"700"+"' height='"+Math.abs(endY-starY)+"'><div xmlns='http://www.w3.org/1999/xhtml'>"+timeNoteHtml+"</div></foreignObject>"
                                }
                            }

                            // ###weekTitle###   ###weekLine###   ###weekNote###
                            
                            // <text x='0' y='80'  text-anchor='star' class='weekTitle'>88</text> 
                            // <text x='450' y='80'  text-anchor='middle' class='weekTitle'>阴云密布</text> 
                            // <text x='900' y='80'  text-anchor='end' class='weekTitle'>日</text> 


                        }
                        SVG=SVG.replace("###weekTitle###",weektitle)
                        SVG=SVG.replace("###weekLine###",weekTimeLine)
                        SVG=SVG.replace("###weekNote###",weekTimeNote)
                        
                    break
                    case "极简横向时间轴":

                    break
                    case "极简累计时间轴":

                    break
                    default:

                }
            break
            case "dayLog":
                switch(settings.dlog){
                    case "pal手账":
                        // ###plan###    ###log###  ###title###
                    break
                    case "时间环":
                        SVG=timeAccount.ringSvg
                        let timeSort=0
                        let ringSvg=""
                        let noteSvg=""

                        for(var i=0;i<dataObj.logs.length;i++){
                            let starHour=timeStrToHours(dataObj.logs[i][0])
                            let endHour=timeStrToHours(dataObj.logs[i][1])
                            let note=dataObj.logs[i][4]?dataObj.logs[i][4]:dataObj.logs[i][3]
                            let timeLast=Math.abs(endHour-starHour)
                            let midTime=0.5*(starHour+endHour)

                            
                            if(dataObj.logs[i][2]==settings.timeSortClass){
                                timeSort+=timeLast
                            }
                            ringSvg+="<circle  cx='700' cy='400' r='220' stroke='var(--"+dataObj.logs[i][2]+"time)' stroke-dasharray='"+((55*3.14159*timeLast)/3).toFixed(0)+",4000' stroke-dashoffset='-"+((55*3.14159*(starHour))/3).toFixed(2)+"' class='logRing'/>"

                            let moveArry=timeToSlogonLoca(midTime)
                            let X1=(700+moveArry[0]).toFixed(0)
                            let Y1=(400+moveArry[1]).toFixed(0)
                            let X2=(700+moveArry[2]).toFixed(0)
                            let Y2=(400+moveArry[3]).toFixed(0)
                            let dh=(moveArry[4]*100)
                            let X3=Number(X2)+dh
                            let endOrStar=moveArry[4]>0?"star":"end"
                            
                            noteSvg+="<path  d='M"+X1+" "+Y1+"	L"+X2+" "+Y2+" h"+dh+"'  class='redLine'/><text x='"+X3+"' y='"+Y2+"' font-size='30' text-anchor='"+endOrStar+"'  >"+note+"</text>"
                            
                            
                            // timeclass\star\end\timenote
                            // stroke\dash\offset
                        }
                        if(timeSort){
                            ringSvg+="<circle  cx='700' cy='400' r='195' stroke='var(--"+settings.timeSortClass+"time)' stroke-dasharray='"+((65*3.14159*timeSort)/4).toFixed(0)+",4000'  class='countTime'/> "
                        }
                        
                        ringSvg="<g transform='rotate(90,700,400)'>"+ringSvg+"</g>"
                        SVG=SVG.replace("###logRing###",ringSvg)
                        SVG=SVG.replace("###weather###",dataObj.weather)
                        SVG=SVG.replace("###day###",dataObj.day)
                        SVG=SVG.replace("###logNote###",noteSvg)
                        
                        function timeToSlogonLoca(hours){
                            let r=240
                            let alpha=hours*Math.PI/12
                            let xPOrN=hours%24>=12?1:-1
                            let yPOrN=hours%24<18&&hours%24>6?-1:1
                            let thata=Math.abs(alpha%Math.PI-0.5*Math.PI)
                            let r_thata=thata==0?r:r*thata/Math.sin(thata)
                            let dx1=xPOrN*r*Math.cos(thata)
                            let dy1=yPOrN*r*Math.sin(thata)
                            let dx2=xPOrN*r_thata*Math.cos(thata)
                            let dy2=yPOrN*r_thata*Math.sin(thata)
                            let dx_dys=[dx1,dy1,dx2,dy2,xPOrN]
                            return dx_dys
                        }
// 时间环形
// <circle  cx='700' cy='400' r='220' stroke='var(--atime)' stroke-dasharray='490,4000' stroke-dashoffset='-306.3' class='logRing'/>
// <circle  cx='700' cy='400' r='195' stroke='red' stroke-dasharray='490,4000' stroke-dashoffset='-306.3' class='countTime'/> 
// 指示线与note
// <path  d='M638	632  L616	714 h-100'  class='redLine'/>

                        
                    break
                    case "极简横向时间轴":

                    break
                    case "极简累计时间轴":

                    break
                    default:

                }

            break
            case "weekBill":
            break
            case "dayBill":
            break
            
            
        }
        // console.log(timeStrToHours("0600"))
        return SVG
        
        function timeStrToHours(timeStr){
            let hours=Number(timeStr.slice(0,2))
            let minuts=Number(timeStr.slice(2,4))
            let allTimeHours=hours+Number((minuts/60).toFixed(2))
            // console.log(allTimeHours)
            return allTimeHours
        }

        function timeNoteToHtml(timeNote){
            // 将可能存在的内部、外部连接、斜体、加粗、高亮转化
            return timeNote
        }
    },
    'weekLog2csv':function(){},
    'dayLog2csv':function(){},
}
function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}
class MyPlugin extends obsidian.Plugin {
    async onload() {
        await this.loadSettings();
        
        this.registerMarkdownPostProcessor(function(el,ctx){
            let code=el.querySelector('code')
            if(code){
                let text=el.innerText
                text=text.replace(/\n复制$/,'')
                switch(code.classList[0]){
                    case "language-mplan":
                        break;
                    case "language-wlog":
                        // console.log(timeAccount.getWeekData(text))
                        let weekData=timeAccount.getWeekData(text)
                        let weekSvg=timeAccount.drwLog(weekData)
                        // console.log(weekSvg)
                        el.innerHTML=weekSvg
                        break;
                    case "language-dlog":
                        let dayData=timeAccount.getdayData(text)
                        let todaySvg=timeAccount.drwLog(dayData)
                        el.innerHTML=todaySvg
                        break;
                    case "language-bill":
                        break;
                    default:
                }
            }
        })
        this.addSettingTab(new MypluginSettingTab(this.app, this));
        
    }
    async writeOptions(changeOpts) {
        this.settings.update((old) => (Object.assign(Object.assign({}, old), changeOpts(old))));
        await this.saveData(this.options);
    }
    loadSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            this.settings = Object.assign({}, DEFAULT_SETTINGS, yield this.loadData());
            timeAccount.pluginSettings=this.settings
        });
        
    }
    saveSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.saveData(this.settings);
        });
    }
}
class MypluginSettingTab extends obsidian.PluginSettingTab {
    constructor(app, plugin) {
        super(app, plugin);
        this.plugin = plugin;
    }
    saveSettings({ rerenderSettings = false, refreshViews = false } = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.plugin.saveData(this.plugin.settings);
           
            if (rerenderSettings) {
                this.display();
            }
        });
    }
    display() {
        let { containerEl } = this;
        containerEl.empty();
        new obsidian.Setting(containerEl)
        .setName('是否以周日作为一周起始')
        .setDesc('')
        .addToggle(toggle => toggle
            .setValue(this.plugin.settings.starAtSunday)
            .onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.starAtSunday = val;
                yield this.saveSettings();
            }
        )));
        new obsidian.Setting(containerEl)
            .setName('统计时间类名')
            .setDesc('当前值:'+this.plugin.settings.timeSortClass)
            .addText(text => {
            // text.inputEl.type = 'string';
            text.setValue(String(this.plugin.settings.timeSortClass));
            text.setPlaceholder(`输入一个小写英文字母`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeSortClass = val;
                yield this.saveSettings();
            }));
        });
    }
}
module.exports = MyPlugin;