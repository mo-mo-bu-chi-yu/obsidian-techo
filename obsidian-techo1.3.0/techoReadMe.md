# 需要的文件
- techo文件夹（测试用）
    - 202206.md
    - 202207.md
    - 202208.md
    - 202209.md
    - 202210.md
- techoPageSetting.json（用于记录techoShow页面的状态）
- obsidian-techo文件夹（插件文件）
    - main.js（插件）
    - data.json（存储插件设置）
    - styles.css（插件的外观样式）
    - manifest.json（插件的id信息）

# 用法
1. 将techoPageSetting.json文件放在.obsidian文件夹下
2. 将obsidian-techo文件夹放在.obsidian/plugins文件夹下
3. 将techo文件夹放入自己的obsidian库的根目录中
4. 开启techo插件
5. 在ob中新建一个md文件，文件名为TechoShow，在文件中随便输入一行字符，切换到预览模式
![[Pasted image 20221018192340.png]]
7. 将TechoShow.md文件固定到侧边栏常驻，右键锁定
![[Pasted image 20221018192225.png]]

techo文件夹下的文件是每月的日志文件，例如`techo/202210.md`是2022年10月的日志文件。
日志文件中可以写月/周/日待办、记录每日时间、账本。
TechoShow页面分为上方的小月历和下方的展示窗口，点击小月历相应的日、周、月、今天、前/后一月、前/后一年可以跳转到相应的时间。
![[Pasted image 20221018193209.png]]

# 设置
## 时间统计
- 统计类名：输入一个英文小写字母或`all`，当时间统计模式为折线（line）、柱状图(bar)时，将统计相应的时间类别。若为all则统计所有记录时间
- 月统计方式：pie、bar、line分别对应饼状图、柱状图、折线图
- 周统计方式：pie、bar、line、timeLines分别对应饼状图、柱状图、折线图、一周时间线
- 日统计方式：timeLines、timeRing分别表示日时间轴、时间环

## 账单统计
- 统计类名：同上，也是一个英文小写字母或`all`
- 预计每日收/支最大数值：填写一个数字，这个数字将是账本折线图、柱状图单日能显示的最大数字
- 单位收支：填写一个数字，这个数字将作为刻度值
![[Pasted image 20221018195940.png]]

# 进阶自定义
日志文件中每一笔时间、账单记录都要指定时间、账单的类型
```txt
1800-1900 y y类时间的备注
1000-1200 a1 a类时间的备注

-10 f f类账单备注
20 c1 c类账单备注
```
每一种时间、账单类型的颜色都可自定义。
![[Pasted image 20221018200652.png]]
方法如下：
打开.obsidian/plugins/obsidian-techo/styless.css（建议用Vscode打开方便取色，没有也可记事本打开。实际上该样式文件只定义了我常用的几种颜色，并未覆盖英文26个字母）
```css
--acolor:#a5c830;

--Acolor:#631515aa;
```
`--acolor`指类型为a的时间记录
`--Acolor`指类型为a的账单记录
后面的`a5c830`为16进制的颜色编码，`a5`、`c8`、`30`分别指红绿蓝三原色的亮度值，最小为`00`，最大为`ff`。
其后可再跟两位十六进制数值表示透明度，`00`表示完全透明，`ff`表示完全不透明

[在线颜色选择器 | RGB颜色查询对照表 (jb51.net)](http://tools.jb51.net/static/colorpicker/)

