const obsidian = require('obsidian');
const fs=require('fs');
// const { match } = require('assert');
// const { Console } = require('console');

//必要的预先定义的值
const DEFAULT_SETTINGS = {
    "starAtSunday":true,
    "timeSortClass":'a',
    "MtimeSort": 'bar',
    "WtimeSort": 'bar',
    "DtimeSort": 'timeRing',

    "billSortClass":'a',
    "billDayNumber":100,
    "billDayPace":20,
    "MbillSort": 'bar',
    "WbillSort": 'bar',
    "DbillSort": 'bar',
};
const DEFAULT_Page_SETTINGS={
    "name":"///",
    "year":function(){
        let today=new Date()
        return today.getFullYear()
    }(),
    "month":function(){
        let today=new Date()
        return today.getMonth()+1
    }(),
    "day":function(){
        let today=new Date()
        return today.getDate()
    }(),
    "weekNumber":20,

    "timeMode":"W",
    "selectedTagId":"timeTag"
}

var dataAnalyse={
    //获取可能存在的yaml
    
    'regDiaryYAML':/^(---\n)(\S+:\x20[^\n]+\n)+(---)/,
    //去除yaml后获取月区域内容
    //从开头到# 结束
    //匹配  以任意非换行符+换行符开始 其后跟着多个# +单个空格
    'regMonth':/^([^#+].*\n+)+(?!\s*#+\x20).*/,
    // 'regMonth':/^([^#+].*\n+)+(?!#+\s).*/,
    'regWeek':/#\s\d{1,2}[W|周]\n[^#]+/,
    'regDay':/##\s\d{1,2}[D|日]\n[^#]+/,
            //   /-\x20+\[[\x20x]\]\x20+[^\n]+/
    'regTodo':/-\x20+\[[\x20x]\]\x20+[^\n]+/g,
    //-  至少一个空格  [一个空格] 至少一个空格
    'regUndo':/^-\x20+\[\x20\]\x20+/,
    // 
    'regDone':/^-\x20+\[x\]\x20+/,
    'regTimeLog':/\d{4}-\d{4}\x20[a-z]\d?\x20[^\n]+/,
    // 'regAllTimeLog':/(?<=##\x20(\d{1,2})[D|日][^#]*)(\d{4}-\d{4}\x20[a-z]\d?\x20[^\n]+)/g,
    'regAllTimeLog':/(?<=##\x20(\d{1,2})[D|日][^#]*)(\d{4})-(\d{4})\x20([a-z]\d?)\x20([^\n]+)/g,
    'regTimeLogReplace':/\d{4}-\d{4}\x20[a-z]\d?\x20[^\n]+/,
    //为避免与时间日志混淆多匹配了前面的换行
    'regAllBillLog':/(?<=##\x20(\d{1,2})[D|日][^#]*)\n(-?)(\d+\.?\d?)\x20([a-z]\d?)\x20([^\n]+)/g,
    'regBill':/\n-?\d+\x20[a-z]\d?\x20[^\n]+/,
    //简单的网页连接
    'regMinURL':new RegExp('[a-zA-z]+://[^\\s\\(\\)]*'),
    'regMinURLReplace':new RegExp('([a-zA-z]+://[^\\s\\(\\)]*)'),
    //完整的网页链接 []()
    'regURL':/\[[^\[\]]+\]\([a-zA-z]+:\/\/[^\s()]*\)/,
    // 替换完整连接
    'regURLReplace':/\[([^\[\]]+)\]\(([a-zA-z]+:\/\/[^\s\(\)]*)\)/,
    // 'regURLReplace':new RegExp('[([^\[\]]+)\]\(([a-zA-z]+://[^\s\(\)]*)\)'),
    //邮箱连接
    'regEmal':/[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/,
    //本地文件连接
    'regEmal':/([\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?)/,
    
    'regLocalFile':/\[\[[^\[\]]+\]\]/,
    'regLocalFileReplace':/\[\[([^\[\]]+)\]\]/,
    // URL解析[链接名](链接地址)
    //\[([^\[\]]+)\]\(([a-zA-z]+://[^\s\(\)]*)\)   ->    $1  $2
    //
    
    'basePath':app.vault.adapter.basePath,
    'pageSettingPath':'\\.obsidian\\techoPageSetting.json',
    'diarryFilesPath':'\\techo',
    //
    'getFileTxtPromise':function(fileNameStr){
        let path=this.diarryFilesPath+ '\\\\'+fileNameStr+'.md'
        let realPath=(this.basePath+path).replace(/\\/g,'\\\\')
        let p=new Promise((resolve,reject)=>{
            fs.readFile(realPath,'utf-8',function(err,dataStr){
                if(err){
                    reject(err)
                }else{
                    resolve(dataStr)
                }
            })
            
        })
        return p
    },
    //将日记原文字符串存入 以备进一步分析 无则为空字符串
    // 三篇日记全部读取完成后启动 calculateData  
    // calculateData之后刷新页面
    'refreshDataAndView':function(){
        let year=techoPage.pageSettings.year
        let month=techoPage.pageSettings.month
        let Fday=new Date(year,Number(month)-2,5,8,0,0)
        let Tday=new Date(year,Number(month)-1,5,8,0,0)
        let Bday=new Date(year,month,5,8,0,0)
        let Str1=Fday.toISOString().slice(0,7).replace('-','')
        let Str2=Tday.toISOString().slice(0,7).replace('-','')
        let Str3=Bday.toISOString().slice(0,7).replace('-','')
        let p1=this.getFileTxtPromise(Str1)
        let p2=this.getFileTxtPromise(Str2)
        let p3=this.getFileTxtPromise(Str3)
        
        p1.then((data)=>{
            techoPage.FDataStr=data
        }).catch(()=>{
            techoPage.FDataStr=''
        })
        p2.then((data)=>{
            techoPage.TDataStr=data
        }).catch(()=>{
            techoPage.TDataStr=''
        })
        p3.then((data)=>{
            techoPage.BDataStr=data
        }).catch(()=>{
            techoPage.BDataStr=''
        })

        let p=[p1,p2,p3]
        Promise.allSettled(p).then(()=>{
            this.calculateData()
            // setTimeout(techoPage.refreshView, 3000 )
            techoPage.refreshView()
        })
        
        //计算并存入techoPage中，之后除非更换月份，否则不会再次读取
    },
    // 根据techoPage 中的设置 FDataStr TDataStr BDataStr 计算相应的数据并存入
    'calculateData':function(changeToMode){
        // 'calenderArr':[],

        // 'MtodoStrArr':[],
        // 'MtimeArr':[],
        // 'MbillArr':[],
        // 'MmainArr':[],
        
        // 'WtodoStrArr':[],
        // 'WtimeArr':[],
        // 'WbillArr':[],
        // 'WmainArr':[],
        
        // 'DtodoStrArr':[],
        // 'DtimeArr':[],
        // 'DbillArr':[],
        // 'DmainArr':[],
        if(changeToMode){
            switch(changeToMode){
                
                case 'W':
                    techoPage.WtodoStrArr=dataAnalyse.getWtodoStrArr()
                    techoPage.WtimeArr=dataAnalyse.getWtimeArr()
                    techoPage.WbillArr=dataAnalyse.getWbillArr()
                    // techoPage.WmainArr=dataAnalyse.getWmainArr()
                break
                case 'D':
                    techoPage.DtodoStrArr=dataAnalyse.getDtodoSWtrArr()
                    techoPage.DtimeArr=dataAnalyse.getDtimeArr()
                    techoPage.DbillArr=dataAnalyse.getDbillArr()
                    // techoPage.DmainArr=dataAnalyse.getDmainArr()
                break
            }

        }else{
            techoPage.calenderArr=calender.getMonthArr()

            techoPage.MtodoStrArr=dataAnalyse.getMtodoStrArr()
            techoPage.MtimeArr=dataAnalyse.getMtimeArr()
            techoPage.MbillArr=dataAnalyse.getMbillArr()
            // techoPage.MmainArr=dataAnalyse.getMmainArr()

            techoPage.WtodoStrArr=dataAnalyse.getWtodoStrArr()
            techoPage.WtimeArr=dataAnalyse.getWtimeArr()
            techoPage.WbillArr=dataAnalyse.getWbillArr()
            // techoPage.WmainArr=dataAnalyse.getWmainArr()

            techoPage.DtodoStrArr=dataAnalyse.getDtodoSWtrArr()
            techoPage.DtimeArr=dataAnalyse.getDtimeArr()
            techoPage.DbillArr=dataAnalyse.getDbillArr()
            // techoPage.DmainArr=dataAnalyse.getDmainArr()
        }
    },
    // 要读取的文件路径 '\\techo\\202205.md' 指库文件夹下./techo/202205.md的文件
    //callback参数（function），指明拿到数据字符串后要进行的操作
    'getFileTxt':function(filePath,callback){
        let realPath=(this.basePath+filePath).replace(/\\/g,'\\\\')
        // let a=function(realPath,)
        fs.readFile(realPath,'utf-8',function(err,dataStr){
            if(err){
                console.log('读取失败\n'+err)
                return ''
            }
            callback(dataStr)
            
        })
    },
    //指定文件路径、要写入的字符串，没有就新建文件
    'writeFileTxt':function(filePath,txtStr){
        let realPath=this.basePath+filePath.replace('/','\\')
        fs.writeFile(realPath,txtStr,function(err){
            if(err){
                console.log('写入失败\n'+err)
                return 
            }
        })
    },
    //根据指定的id元素 、数据字符串, 返回id元素需要的数据
    
    //number(Number):要规整的数字   n(Number):要规整为几位数字   str(String):用来填充的字符，默认为'0'
    'padleft':function (number, n, str){
        return Array(n-String(number).length+1).join(str||'0')+number;
    },
    'getMtodoStrArr':function(){
        let monthStr=''
        // 删除开头可能存在的yaml
        monthStr=techoPage.TDataStr.replace(this.regDiaryYAML,'')
        // 将多行回车变成一行回车
        monthStr=monthStr.replace(/\n{2,}/g,'\n')
        // 取出本月记录内容
        monthStr=monthStr.match(this.regMonth)?monthStr.match(this.regMonth)[0]:''
        // 取出本月待办
        monthStr=monthStr.match(this.regTodo)
        if(!monthStr){
            monthStr=false
        }
        return monthStr
        // techoPage.MtodoStrArr=monthStr
        
    },
    'getMtimeArr':function(){
        let monthStr=''
        let allTimeArr=[]
        let result=[]
        // 删除开头可能存在的yaml
        monthStr=techoPage.TDataStr.replace(this.regDiaryYAML,'')
        // 将多行回车变成一行回车
        monthStr=monthStr.replace(/\n{2,}/g,'\n')
        monthStr.replace(this.regAllTimeLog,gatherArr)
        // console.log(allTimeArr)
        // 0: (6) ['1', '0000', '0100', '0100', 'a1', '1D测试']
        // 1: (6) ['1', '0600', '0300', '0900', 'a', 'bkjghgjhgj']
        // 2: (6) ['2', '0000', '0200', '0200', 'a1', '2测试']
        // 3: (6) ['3', '0000', '0300', '0300', 'a1', '3测试']
        // 4: (6) ['5', '0000', '0500', '0500', 'a1', '5测试']
        // 5: (6) ['6', '0000', '0600', '0600', 'a1', '6测试']
        // 6: (6) ['7', '0000', '0700', '0700', 'a1', '7测试']
        // 7: (6) ['8', '0000', '0800', '0800', 'c1', '8测试']
        // 8: (6) ['9', '0000', '0900', '0900', 'a1', '9D测试']
        // 9: (6) ['10', '0000', '1000', '1000', 'v1', '10D测试']
        // 10: (6) ['11', '0000', '1100', '1100', 'a1', '11D测试']
        // 11: (6) ['12', '0000', '1200', '1200', 'b1', '12D测试']
        // 12: (6) ['13', '0000', '1300', '1300', 'a1', '13D测试']
        // 13: (6) ['13', '1400', '0100', '1500', 'a2', '13D测试2']
        // 14: (6) ['14', '0000', '1400', '1400', 'a1', '14D测试']
        // 15: (6) ['15', '0000', '1500', '1500', 'a1', '15D测试']
        // 16: (6) ['16', '0000', '1600', '1600', 'a1', '16D测试']
        // 17: (6) ['17', '0000', '1700', '1700', 'a1', '17D测试']
        // 18: (6) ['18', '0000', '1800', '1800', 'k1', '18D测试dscd']
        // 19: (6) ['19', '0000', '1900', '1900', 'a1', '19D测试19']
        // 20: (6) ['20', '0000', '2000', '2000', 'k1', '20D测试']
        // 21: (6) ['21', '0000', '2100', '2100', 'a1', '21D测试']
        // 22: (6) ['22', '0000', '2200', '2200', 'd1', '22D测试']
        // 23: (6) ['23', '0000', '2300', '2300', 'e1', '23D测试']
        // 24: (6) ['24', '0000', '2300', '2300', 'a1', '24D测试']
        // 25: (6) ['25', '0000', '0000', '0000', 'a1', '25D测试']
        // 26: (6) ['26', '0000', '0100', '0100', 'a1', '26D测试']
        // 27: (6) ['27', '0000', '0200', '0200', 'a1', '27D测试']
        // 28: (6) ['28', '0000', '0300', '0300', 'a1', '28D测试']
        // 29: (6) ['29', '0000', '0400', '0400', 'a1', '29D测试']
        // 30: (6) ['30', '0000', '0500', '0500', 'a1', '30D测试']

        let sortClass=techoPage.pluginSettings.timeSortClass
        let thisMonthDayNumber=calender.getMonthDaysNumber(techoPage.pageSettings.year,techoPage.pageSettings.month)
                
        switch(techoPage.pluginSettings.MtimeSort){
            
            //首先过滤数组，只留下统计类别的记录
            //创建一个满月天数的数组，
            //对于每一个留下的数组，根据其天数加到满月数组上
            // (31)  bar line
            // 0: (3) ['0400', 240, '4.0']
            // 1: (3) ['0200', 120, '2.0']
            // 2: (3) ['0300', 180, '3.0']
            // 3: (3) ['0000', 0, '0.0']
            // 4: (3) ['0500', 300, '5.0']
            // 5: (3) ['0600', 360, '6.0']
            // 6: (3) ['0700', 420, '7.0']
            // 7: (3) ['0000', 0, '0.0']
            // 8: (3) ['0900', 540, '9.0']
            // 9: (3) ['0000', 0, '0.0']
            // 10: (3) ['1100', 660, '11.0']
            // 11: (3) ['0000', 0, '0.0']
            // 12: (3) ['1400', 840, '14.0']
            // 13: (3) ['1400', 840, '14.0']
            // 14: (3) ['1500', 900, '15.0']
            // 15: (3) ['1600', 960, '16.0']
            // 16: (3) ['1700', 1020, '17.0']
            // 17: (3) ['0000', 0, '0.0']
            // 18: (3) ['1900', 1140, '19.0']
            // 19: (3) ['0000', 0, '0.0']
            // 20: (3) ['2100', 1260, '21.0']
            // 21: (3) ['0000', 0, '0.0']
            // 22: (3) ['0000', 0, '0.0']
            // 23: (3) ['2300', 1380, '23.0']
            // 24: (3) ['0000', 0, '0.0']
            // 25: (3) ['0100', 60, '1.0']
            // 26: (3) ['0200', 120, '2.0']
            // 27: (3) ['0300', 180, '3.0']
            // 28: (3) ['0400', 240, '4.0']
            // 29: (3) ['0500', 300, '5.0']
            // 30: (3) ['1500', 900, '15.0']
            case 'line':
                let filterRegLine
                let filtedArrLine
                if(sortClass=='all'){
                    filtedArrLine=allTimeArr
                }else{
                    filterRegLine=new RegExp(sortClass+'\\d\?')
                    filtedArrLine  = allTimeArr.filter(arr => arr[4].match(filterRegLine ));
                }
                
                let fullMonthArrLine=new Array(thisMonthDayNumber).fill('0000');
                
                for(let i=0;i<filtedArrLine.length;i++){
                    fullMonthArrLine[Number(filtedArrLine[i][0])-1]=addOrCut(fullMonthArrLine[Number(filtedArrLine[i][0])-1]+'+'+filtedArrLine[i][2])
                }
                //对满月数组 '0530'-->5.5
                for(let j=0;j<thisMonthDayNumber;j++){
                    //添加实际分钟数(数字)  实际小时(小数)
                    fullMonthArrLine[j]=([fullMonthArrLine[j],timeStrtoMNumber(fullMonthArrLine[j]),timeStrtoHNumber(fullMonthArrLine[j])])
                }
                result=fullMonthArrLine
            break
            case 'bar':
                let filterRegBar
                let filtedArrBar 
                if(sortClass=='all'){
                    filtedArrBar =allTimeArr
                }else{
                    filterRegBar=new RegExp(sortClass+'\\d\?')
                    filtedArrBar   = allTimeArr.filter(arr => arr[4].match(filterRegBar));
                }

                let fullMonthArrBar=new Array(thisMonthDayNumber).fill('0000');
                
                for(let i=0;i<filtedArrBar.length;i++){
                    fullMonthArrBar[Number(filtedArrBar[i][0])-1]=addOrCut(fullMonthArrBar[Number(filtedArrBar[i][0])-1]+'+'+filtedArrBar[i][2])
                }
                //对满月数组 '0530'-->5.5
                for(let j=0;j<thisMonthDayNumber;j++){
                    //添加实际分钟数(数字)  实际小时(小数)
                    fullMonthArrBar[j]=([fullMonthArrBar[j],timeStrtoMNumber(fullMonthArrBar[j]),timeStrtoHNumber(fullMonthArrBar[j])])
                }
                result=fullMonthArrBar
            break
            case 'pie':
                let timeClassHaveMeet=[]
                let resultArr=[]
                let thisIndex
                // 遍历每个时间记录
                // 如果时间种类已经出现 将这个记录附加到对应的元素数组
                // 否则 在timeClassHaveMeet 中记录下这个数组，将相关记录直接添加到记录数组中
                for(let i=0;i<allTimeArr.length;i++){
                    //如果已经遇到过该类
                    let thisClass=allTimeArr[i][4][0]
                    if(timeClassHaveMeet.includes(thisClass)){
                        thisIndex=resultArr.findIndex(isThisClass)
                        resultArr[thisIndex]=[resultArr[thisIndex][0],timeStrtoMNumber(allTimeArr[i][2],resultArr[thisIndex][1])]
                    }else{
                        timeClassHaveMeet.push(thisClass)
                        resultArr.push([allTimeArr[i][4][0],timeStrtoMNumber(allTimeArr[i][2])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArr.sort(function (a, b) {return b[1]-a[1]})
                result=resultArr
            break
        }
        return result

        function gatherArr(match, p1, p2,p3,p4,p5,offset, string){
            allTimeArr.push([p1,p2,addOrCut(p3+'-'+p2),p3,p4,p5])
            return ''
        }
        function addOrCut(strExpress){
            let Hours1
            let Hours2
            let minute1
            let minute2
            let aOc    //  '+'  '-'相加还是相减
            strExpress.replace(/(\d{2})(\d{2})([+|-])(\d{2})(\d{2})/,function(match, p1, p2,p3,p4,p5,offset, string){
                Hours1=Number(p1)
                minute1=Number(p2)
                aOc=p3
                Hours2=Number(p4)
                minute2=Number(p5)
            })
            if(aOc=='+'){
                let T1=new Date()
                T1.setHours(Hours1)
                T1.setMinutes(minute1)
                T1.setTime(T1.getTime()+((Hours2*60)+minute2)*60*1000)
                let resultH=padleft(T1.getHours(),2)  
                let resultM=padleft(T1.getMinutes(),2) 
                return resultH+resultM
            }else{
                let T1=new Date()
                T1.setHours(Hours1)
                T1.setMinutes(minute1)
                let T2=new Date()
                T2.setHours(Hours2)
                T2.setMinutes(minute2)
                let T3=new Date("1/01/2000 00:0:00")
                T3.setTime(T3.getTime()+Math.abs(T1.getTime()-T2.getTime()))
                let resultH=padleft(T3.getHours(),2)  
                let resultM=padleft(T3.getMinutes(),2) 
                return resultH+resultM
            }
            function padleft(number, n, str){
                return Array(n-String(number).length+1).join(str||'0')+number;
            }

        }
        //将 '0320'  -> 200  时间字符串转分钟 数字 
        function timeStrtoMNumber(timeStr,minute){
            let H=parseInt(timeStr.slice(0,2))
            let M=parseInt(timeStr.slice(2,4))
            return minute?60*H+M+minute:60*H+M
        }
        //将 '0320'  -> 200  时间字符串转小时 数字 
        function timeStrtoHNumber(timeStr){
            let H=parseInt(timeStr.slice(0,2))
            let M=parseInt(timeStr.slice(2,4))
            return (H+M/60).toFixed(1)
        }
    },
    'getMbillArr':function(){
        let monthStr=''
        let allBillArr=[]
        let result=[]
        // 删除开头可能存在的yaml
        monthStr=techoPage.TDataStr.replace(this.regDiaryYAML,'')
        
        // 将多行回车变成一行回车
        monthStr=monthStr.replace(/\n{2,}/g,'\n')
        monthStr.replace(this.regAllBillLog,gatherArr)
        // allBillArr
        // 0: (5) ['1', '-', '15', 'a', '1日a类记账']
        // 1: (5) ['1', '+', '20', 'b', '1日b类记账']
        // 2: (5) ['2', '-', '15', 'a', '2日a类记账']
        // 3: (5) ['2', '+', '20', 'b', '2日b类记账']
        // 4: (5) ['3', '-', '15', 'a', '3日a类记账']
        // 5: (5) ['3', '+', '20', 'b', '3日b类记账']
        // 6: (5) ['13', '-', '15', 'a', '13日a类记账']
        // 7: (5) ['13', '+', '20', 'b', '13日b类记账']
        let billSortClass=techoPage.pluginSettings.billSortClass
        let thisMonthDayNumber=calender.getMonthDaysNumber(techoPage.pageSettings.year,techoPage.pageSettings.month)

        switch(techoPage.pluginSettings.MbillSort){
            //首先过滤数组，只留下统计类别的记录
            //创建一个满月天数的数组，
            //对于每一个留下的数组，根据其天数加到满月数组上
            case 'line':
                let filterRegLine
                let filtedArrLine
                if(billSortClass=='all'){
                    filtedArrLine  = allBillArr
                }else{
                    filterRegLine=new RegExp(billSortClass+'\\d\?')
                    filtedArrLine  = allBillArr.filter(arr => arr[4].match(filterRegLine ));
                }
                // [收入],[支出]
                // 每一日 [20,15,32,....,20],[15,20,32,02,.....,30]
                let fullMonthArrLinein=new Array(thisMonthDayNumber).fill(0);
                let fullMonthArrLineout=new Array(thisMonthDayNumber).fill(0);
                for(let i=0;i<filtedArrLine.length;i++){
                    if(filtedArrLine[i][1]=='+'){
                        fullMonthArrLinein[Number(filtedArrLine[i][0])-1]+=Number(filtedArrLine[i][2])
                    }else{
                        fullMonthArrLineout[Number(filtedArrLine[i][0])-1]+=Number(filtedArrLine[i][2])
                    }
                }
                result=[fullMonthArrLinein,fullMonthArrLineout]
                
            break
            // [
            //    ['a', 140],
            //    ['b', 40]
            // ]
            case 'inPie':
                // 筛选出收入
                let filtedArrInPie=allBillArr.filter(arr => arr[1].match(/\+/))
                
                let billClassHaveMeetInPie=[]
                let thisIndexInPie
                let resultArrInPie=[]
                for(let i=0;i<filtedArrInPie.length;i++){
                    //如果已经遇到过该类
                    // ['a',20]
                    let thisClass=filtedArrInPie[i][3][0]
                    if(billClassHaveMeetInPie.includes(thisClass)){
                        thisIndexInPie=resultArrInPie.findIndex(isThisClass)
                        resultArrInPie[thisIndexInPie]=[filtedArrInPie[i][3][0],resultArrInPie[thisIndexInPie][1]+Number(filtedArrInPie[i][2])]
                    }else{
                        billClassHaveMeetInPie.push(thisClass)
                        resultArrInPie.push([filtedArrInPie[i][3][0],Number(filtedArrInPie[i][2])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArrInPie.sort(function (a, b) {return b[1]-a[1]})
                result=resultArrInPie
            break
            case 'outPie':
                let filtedArrOutPie=allBillArr.filter(arr => arr[1].match(/\-/))
                
                let billClassHaveMeetOutPie=[]
                let thisIndexOutPie
                let resultArrOutPie=[]
                for(let i=0;i<filtedArrOutPie.length;i++){
                    //如果已经遇到过该类
                    // ['a',20]
                    let thisClass=filtedArrOutPie[i][3][0]
                    if(billClassHaveMeetOutPie.includes(thisClass)){
                        thisIndexOutPie=resultArrOutPie.findIndex(isThisClass)
                        resultArrOutPie[thisIndexOutPie]=[filtedArrOutPie[i][3][0],resultArrOutPie[thisIndexOutPie][1]+Number(filtedArrOutPie[i][2])]
                    }else{
                        billClassHaveMeetOutPie.push(thisClass)
                        resultArrOutPie.push([filtedArrOutPie[i][3][0],Number(filtedArrOutPie[i][2])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArrOutPie.sort(function (a, b) {return b[1]-a[1]})
                result=resultArrOutPie

            break
            case 'table':
                result=allBillArr
            break

        }
        function gatherArr(match, day, PositiveOrnegative,number,classStr,billNote,offset, string){
            allBillArr.push([day,PositiveOrnegative?'-':'+',number,classStr,billNote])
        }
        return result
    },
    /**
     * timeMode='M'
     * [ 
     *   [周数]
     *   [日期,是否本月,[todolists],[scheduleLists]],    
     *   [日期,是否本月,[todolists],[scheduleLists]],
     *   [日期,是否本月,[todolists],[scheduleLists]],    
     *   [日期,是否本月,[todolists],[scheduleLists]],
     *   [日期,是否本月,[todolists],[scheduleLists]],    
     *   [日期,是否本月,[todolists],[scheduleLists]],
     *   [日期,是否本月,[todolists],[scheduleLists]]
     * ]
     */
    /***
     * timeMode='W'
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     * [日期,周几,[todolists],[timelogs]]
     */
    /**
     * timeMode='D'
     * string
     */
    
    /**
     * 返回周待办字符串
     * @returns 
        0: "- [x] 202207  31周计划1"
        1: "- [x] 202207  31周计划2"
        2: "- [ ] 202207  31周计划3"
        3: "- [ ] 202207  31周计划4"
        4: "- [ ] 202207  31周计划5"
        5: "- [x] 202207  31周计划6"
        6: "- [ ] 202207  31周计划7"
        7: "- [x] 202208  31周计划1 续上="
        8: "- [x] 202208  31周计划2续上="
        9: "- [ ] 202208  31周计划3续上="
        10: "- [ ] 202208  31周计划4续上="
        11: "- [ ] 202208  31周计划5续上="
        12: "- [x] 202208  31周计划6续上="
        13: "- [ ] 202208  31周计划7续上="
     */
    'getWtodoStrArr':function(){
        // 确定读取那一周数据
        let weekNumberStr=dataAnalyse.padleft(techoPage.pageSettings.weekNumber,2)
        let realReg=new RegExp('#\\s'+weekNumberStr+'[W|周]\\n[^#]+','g')

        // 删除开头可能存在的yaml
        // 三篇日记去除yaml后拼字符串
        let result=techoPage.FDataStr.replace(this.regDiaryYAML,'')+techoPage.TDataStr.replace(this.regDiaryYAML,'')+techoPage.BDataStr.replace(this.regDiaryYAML,'')
        // 将多行回车变成一行回车
        result=result.replace(/\n{2,}/g,'\n')
        // 取出本周记录内容
        result=result?result.match(realReg):''
        if(result){
            result=result.join('')
        }else{
            result=false
        }
        result=result?result.match(this.regTodo):''
        if(!result){
            result=false
        }
        return result
    },
    'getWtimeArr':function(){
        let year=techoPage.pageSettings.year
        let week=techoPage.pageSettings.weekNumber
        let month=techoPage.pageSettings.month
        // let day=techoPage.pageSettings.day
        let starAtSunday=techoPage.pluginSettings.starAtSunday
        let weekArr=calender.getWeekArr(year,week,starAtSunday)
        // weekArr
        // 0: (2) ['20220731', 0]
        // 1: (2) ['20220801', 1]
        // 2: (2) ['20220802', 2]
        // 3: (2) ['20220803', 3]
        // 4: (2) ['20220804', 4]
        // 5: (2) ['20220805', 5]
        // 6: (2) ['20220806', 6]
        // 初步处理后的数据
        let weekTimeArr=[]
        // 最终输出结果
        let result

        let thisMonthStr=String(year)+dataAnalyse.padleft(month,2)
        let day
        let dayReg
        let monthStr
        let todayTimeArr=[]
        for(let i=0;i<weekArr.length;i++){
            day=String(Number(weekArr[i][0].slice(6,8)))
            dayReg=new RegExp('##\\s'+day+'[D|日]\\n[^#]+','g')
            
            let thisDayStr=weekArr[i][0]
            let thisDayday=weekArr[i][1]
            // 如果是本笔记就读取本笔记
            if(weekArr[i][0].slice(0,6)==thisMonthStr){
                monthStr=techoPage.TDataStr
            // 迟于当前月
            }else if(Number(weekArr[i][0].slice(0,6))>Number(thisMonthStr)){
                monthStr=techoPage.BDataStr
            //早于当前月
            }else{
                monthStr=techoPage.FDataStr
            }
            // 删除开头可能存在的yaml
            monthStr=monthStr.replace(this.regDiaryYAML,'')
            // 将多行回车变成一行回车
            monthStr=monthStr.replace(/\n{2,}/g,'\n')
            // 取出当日记录
            monthStr=monthStr.match(dayReg)
            // 合并为一条字符串
            if(monthStr){
                monthStr=monthStr.join('')
            }else{
                monthStr=''
            }
            monthStr.replace(/(\d{4})-(\d{4})\x20([a-z]\d?)\x20([^\n]+)/g,gatherArr)
            for(let j=0;j<todayTimeArr.length;j++){
                weekTimeArr.push([thisDayStr,thisDayday,todayTimeArr[j][0],todayTimeArr[j][1],todayTimeArr[j][2],todayTimeArr[j][3],todayTimeArr[j][4]])
            }
            todayTimeArr=[]
            // weekTimeArr.push(monthStr)
            // monthStr.match()
            
        }
        // weekTimeArr
        /**
         *        [日期字串，周几, 起始时间, 持续时长, 终止时间, 时间类别, 备注]
         *   0: (7) ['20220731', 0, '0000', '0500', '0500', 'a1', '31D测试']
         *   1: (7) ['20220801', 1, '0000', '0100', '0100', 'a1', '1D测试']
         *   2: (7) ['20220801', 1, '0600', '0300', '0900', 'a', 'bkjghgjhgj']
         *   3: (7) ['20220802', 2, '0000', '0200', '0200', 'a1', '2测试']
         *   4: (7) ['20220803', 3, '0000', '0300', '0300', 'a1', '3测试']
         *   5: (7) ['20220804', 4, '0000', '0300', '0300', 'a1', '4测试']
         *   6: (7) ['20220805', 5, '0000', '0500', '0500', 'a1', '5测试']
         *   7: (7) ['20220806', 6, '0000', '0600', '0600', 'a1', '6测试']
         */
        let sortClass=techoPage.pluginSettings.timeSortClass
        switch(techoPage.pluginSettings.WtimeSort){
            case 'line':
                // 过滤统计类别
                /**
                 * 对应类别的时间、分钟数、小时数
                 * 0: (3) ['0500', 300, '5.0']
                 * 1: (3) ['0400', 240, '4.0']
                 * 2: (3) ['0200', 120, '2.0']
                 * 3: (3) ['0300', 180, '3.0']
                 * 4: (3) ['0300', 180, '3.0']
                 * 5: (3) ['0500', 300, '5.0']
                 * 6: (3) ['0600', 360, '6.0']
                 */

                let filterRegLine
                let filtedArrLine
                if(sortClass=='all'){
                    filtedArrLine=weekTimeArr
                }else{
                    filterRegLine=new RegExp(sortClass+'\\d\?')
                    filtedArrLine  = weekTimeArr.filter(arr => arr[5].match(filterRegLine ));
                }

                let fullWeekArrLine=new Array(7).fill('0000');
                for(let j=0;j<filtedArrLine.length;j++){
                    fullWeekArrLine[Number(filtedArrLine[j][1])]=addOrCut(fullWeekArrLine[Number(filtedArrLine[j][1])]+'+'+filtedArrLine[j][3])
                }
                //对满月数组 '0530'-->5.5
                for(let k=0;k<7;k++){
                    //添加实际分钟数(数字)  实际小时(小数)
                    fullWeekArrLine[k]=([fullWeekArrLine[k],timeStrtoMNumber(fullWeekArrLine[k]),timeStrtoHNumber(fullWeekArrLine[k])])
                }
                result=fullWeekArrLine

            break
            case 'bar':
                let filterRegBar
                let filtedArrBar
                if(sortClass=='all'){
                    filtedArrBar=weekTimeArr
                }else{
                    filterRegBar=new RegExp(sortClass+'\\d\?')
                    filtedArrBar  = weekTimeArr.filter(arr => arr[5].match(filterRegBar ));
                }

                let fullWeekArrBar=new Array(7).fill('0000');
                for(let j=0;j<filtedArrBar.length;j++){
                    fullWeekArrBar[Number(filtedArrBar[j][1])]=addOrCut(fullWeekArrBar[Number(filtedArrBar[j][1])]+'+'+filtedArrBar[j][3])
                }
                //对满月数组 '0530'-->5.5
                for(let k=0;k<7;k++){
                    //添加实际分钟数(数字)  实际小时(小数)
                    fullWeekArrBar[k]=([fullWeekArrBar[k],timeStrtoMNumber(fullWeekArrBar[k]),timeStrtoHNumber(fullWeekArrBar[k])])
                }
                result=fullWeekArrBar
            break
            case 'pie':
                let timeClassHaveMeet=[]
                let resultArr=[]
                let thisIndex
                // 遍历每个时间记录
                // 如果时间种类已经出现 将这个记录附加到对应的元素数组
                // 否则 在timeClassHaveMeet 中记录下这个数组，将相关记录直接添加到记录数组中
                for(let i=0;i<weekTimeArr.length;i++){
                    //如果已经遇到过该类
                    let thisClass=weekTimeArr[i][5][0]
                    if(timeClassHaveMeet.includes(thisClass)){
                        thisIndex=resultArr.findIndex(isThisClass)
                        resultArr[thisIndex]=[weekTimeArr[i][5][0],timeStrtoMNumber(weekTimeArr[i][3],resultArr[thisIndex][1])]
                    }else{
                        timeClassHaveMeet.push(thisClass)
                        resultArr.push([weekTimeArr[i][5][0],timeStrtoMNumber(weekTimeArr[i][3])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArr.sort(function (a, b) {return b[1]-a[1]})
                
                result=resultArr
            break
            
            case 'timeLines':
                // 某一天
                // [起始分钟数，持续分钟数，分类名]
                // [起始分钟数，持续分钟数，分类名]
                // [起始分钟数，持续分钟数，分类名]
                // 共七天
                let fullWeekArrTimeLine=[[],[],[],[],[],[],[]]
                for(let j=0;j<weekTimeArr.length;j++){
                    fullWeekArrTimeLine[weekTimeArr[j][1]].push([timeStrtoMNumber(weekTimeArr[j][2]),timeStrtoMNumber(weekTimeArr[j][3]),weekTimeArr[j][5][0],weekTimeArr[j][6]])
                }
                result=fullWeekArrTimeLine

            break
        }
        return result
        // allTimeArr
        // [
        //     [日期字串，周几, 起始时间, 持续时长, 终止时间, 时间类别, 备注]
        //     ["20220731",'1', '0000', '0100', '0100', 'a1', '1D测试']
        // ]
        function gatherArr(match, star,end,className,note,offset, string){
            todayTimeArr.push([star,addOrCut(end+'-'+star),end,className,note])
        }
        function addOrCut(strExpress){
            let Hours1
            let Hours2
            let minute1
            let minute2
            let aOc    //  '+'  '-'相加还是相减
            strExpress.replace(/(\d{2})(\d{2})([+|-])(\d{2})(\d{2})/,function(match, p1, p2,p3,p4,p5,offset, string){
                Hours1=Number(p1)
                minute1=Number(p2)
                aOc=p3
                Hours2=Number(p4)
                minute2=Number(p5)
            })
            if(aOc=='+'){
                let T1=new Date()
                T1.setHours(Hours1)
                T1.setMinutes(minute1)
                T1.setTime(T1.getTime()+((Hours2*60)+minute2)*60*1000)
                let resultH=padleft(T1.getHours(),2)  
                let resultM=padleft(T1.getMinutes(),2) 
                return resultH+resultM
            }else{
                let T1=new Date()
                T1.setHours(Hours1)
                T1.setMinutes(minute1)
                let T2=new Date()
                T2.setHours(Hours2)
                T2.setMinutes(minute2)
                let T3=new Date("1/01/2000 00:0:00")
                T3.setTime(T3.getTime()+Math.abs(T1.getTime()-T2.getTime()))
                let resultH=padleft(T3.getHours(),2)  
                let resultM=padleft(T3.getMinutes(),2) 
                return resultH+resultM
            }
            function padleft(number, n, str){
                return Array(n-String(number).length+1).join(str||'0')+number;
            }

        }
        //将 '0320'  -> 200  时间字符串转分钟 数字 
        function timeStrtoMNumber(timeStr,minute){
            let H=parseInt(timeStr.slice(0,2))
            let M=parseInt(timeStr.slice(2,4))
            return minute?60*H+M+minute:60*H+M
        }
        //将 '0320'  -> 200  时间字符串转小时 数字 
        function timeStrtoHNumber(timeStr){
            let H=parseInt(timeStr.slice(0,2))
            let M=parseInt(timeStr.slice(2,4))
            return (H+M/60).toFixed(1)
        }
    },
    'getWbillArr':function(){
        let year=techoPage.pageSettings.year
        let week=techoPage.pageSettings.weekNumber
        let month=techoPage.pageSettings.month
        // let day=techoPage.pageSettings.day
        let starAtSunday=techoPage.pluginSettings.starAtSunday
        let weekArr=calender.getWeekArr(year,week,starAtSunday)
        // weekArr
        // 0: (2) ['20220731', 0]
        // 1: (2) ['20220801', 1]
        // 2: (2) ['20220802', 2]
        // 3: (2) ['20220803', 3]
        // 4: (2) ['20220804', 4]
        // 5: (2) ['20220805', 5]
        // 6: (2) ['20220806', 6]
        // 初步处理后的数据
        let weekBillArr=[]
        // 最终输出结果
        let result=[]

        let thisMonthStr=String(year)+dataAnalyse.padleft(month,2)
        let day
        let dayReg
        let monthStr
        let todayTimeArr=[]
        for(let i=0;i<weekArr.length;i++){
            day=String(Number(weekArr[i][0].slice(6,8)))
            dayReg=new RegExp('##\\s'+day+'[D|日]\\n[^#]+','g')
            
            let thisDayStr=weekArr[i][0]
            let thisDayday=weekArr[i][1]
            // 如果是本笔记就读取本笔记
            if(weekArr[i][0].slice(0,6)==thisMonthStr){
                monthStr=techoPage.TDataStr
            // 迟于当前月
            }else if(Number(weekArr[i][0].slice(0,6))>Number(thisMonthStr)){
                monthStr=techoPage.BDataStr
            //早于当前月
            }else{
                monthStr=techoPage.FDataStr
            }
            // 删除开头可能存在的yaml
            monthStr=monthStr.replace(this.regDiaryYAML,'')
            // 将多行回车变成一行回车
            monthStr=monthStr.replace(/\n{2,}/g,'\n')
            // 取出当日记录
            monthStr=monthStr.match(dayReg)
            // 合并为一条字符串
            if(monthStr){
                monthStr=monthStr.join('')
            }else{
                monthStr=''
            }
            monthStr.replace(this.regAllBillLog,gatherArr)
            for(let j=0;j<todayTimeArr.length;j++){
                weekBillArr.push([thisDayStr,thisDayday,todayTimeArr[j][0],todayTimeArr[j][1],todayTimeArr[j][2],todayTimeArr[j][3],todayTimeArr[j][4]])
            }
            todayTimeArr=[]
        }
        // weekBillArr
        /**weekBillArr
         * 0: (7) ['20220731', 0, '31', '-', '50', 'a', '31日开销']
         * 1: (7) ['20220731', 0, '31', '-', '20.5', 'b', '31日开销']
         * 2: (7) ['20220801', 1, '1', '-', '15', 'a', '1日a类记账']
         * 3: (7) ['20220801', 1, '1', '+', '20', 'b', '1日b类记账']
         * 4: (7) ['20220801', 1, '1', '+', '100', 'v1', '收入v']
         * 5: (7) ['20220801', 1, '1', '-', '100', 'v2', '支出v']
         * 6: (7) ['20220802', 2, '2', '-', '15', 'a', '2日a类记账']
         * 7: (7) ['20220802', 2, '2', '+', '20', 'a', '2日b类记账']
         * 8: (7) ['20220803', 3, '3', '-', '15', 'a', '3日a类记账']
         * 9: (7) ['20220803', 3, '3', '+', '20', 'a', '3日b类记账']
         * 10: (7) ['20220804', 4, '4', '+', '20.5', 'a', '4日b类记账']
         * 11: (7) ['20220805', 5, '5', '+', '20', 'a', '5日b类记账']
         * 12: (7) ['20220806', 6, '6', '+', '20', 'c', '3日b类记账']
         */
        let billSortClass=techoPage.pluginSettings.billSortClass
        switch(techoPage.pluginSettings.WbillSort){
            case 'line':
                let filterRegLine
                let filtedArrLine
                if(billSortClass=='all'){
                    filtedArrLine =weekBillArr
                }else{
                    filterRegLine=new RegExp(billSortClass+'\\d\?')
                    filtedArrLine=weekBillArr.filter(arr=>{arr[5].match(filterRegLine)})
                }
                // [7天收入数组],[7天支出数组]
                let weekInBilArr=new Array(7).fill(0)
                let weekOutBilArr=new Array(7).fill(0)
                for(let i=0;i<filtedArrLine.length;i++){
                    if(filtedArrLine[i][3]=='+'){
                        weekInBilArr[Number(filtedArrLine[i][1])]+=Number(filtedArrLine[i][4])
                    }else{
                        weekOutBilArr[Number(filtedArrLine[i][1])]+=Number(filtedArrLine[i][4])
                    }
                }
                result=[weekInBilArr,weekOutBilArr]

            break
            case 'inPie':
                // 0: (2) ['v', 100]
                // 1: (2) ['a', 80.5]
                // 2: (2) ['b', 20]
                // 3: (2) ['c', 20]
                let filtedInPie=weekBillArr.filter(arr => arr[3].match(/\+/))
                let billClassHaveMeetInPie=[]
                let thisIndexInPie
                let resultArrInPie=[]
                for(let i=0;i<filtedInPie.length;i++){
                    //如果已经遇到过该类
                    // ['a',20]
                    let thisClass=filtedInPie[i][5][0]
                    if(billClassHaveMeetInPie.includes(thisClass)){
                        thisIndexInPie=resultArrInPie.findIndex(isThisClass)
                        resultArrInPie[thisIndexInPie]=[filtedInPie[i][5][0],resultArrInPie[thisIndexInPie][1]+Number(filtedInPie[i][4])]
                    }else{
                        billClassHaveMeetInPie.push(thisClass)
                        resultArrInPie.push([filtedInPie[i][5][0],Number(filtedInPie[i][4])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArrInPie.sort(function (a, b) {return b[1]-a[1]})
                result=resultArrInPie

            break
            case 'outPie':
                
                let filtedOutPie=weekBillArr.filter(arr => arr[3].match(/\-/))
                let billClassHaveMeetOutPie=[]
                let thisIndexOutPie
                let resultArrOutPie=[]
                for(let i=0;i<filtedOutPie.length;i++){
                    //如果已经遇到过该类
                    // ['a',20]
                    let thisClass=filtedOutPie[i][5][0]
                    if(billClassHaveMeetOutPie.includes(thisClass)){
                        thisIndexOutPie=resultArrOutPie.findIndex(isThisClass)
                        resultArrOutPie[thisIndexOutPie]=[filtedOutPie[i][5][0],resultArrOutPie[thisIndexOutPie][1]+Number(filtedOutPie[i][4])]
                    }else{
                        billClassHaveMeetOutPie.push(thisClass)
                        resultArrOutPie.push([filtedOutPie[i][5][0],Number(filtedOutPie[i][4])])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArrOutPie.sort(function (a, b) {return b[1]-a[1]})
                result=resultArrOutPie

            break
            case 'table':
                result=weekBillArr
            break
        }
        return result
        function gatherArr(match, day, PositiveOrnegative,number,classStr,billNote,offset, string){
            todayTimeArr.push([day,PositiveOrnegative?'-':'+',number,classStr,billNote])
        }
    },
    'getDtodoSWtrArr':function(){
        let dayStr=String(Number(techoPage.pageSettings.day))
        
        let realReg=new RegExp('##\\s'+dayStr+'[D|日]\\n[^#]+','g')
        let result=techoPage.TDataStr.match(realReg)
        if(result){
            result=result.join('')
            result=result.match(this.regTodo)
        }
        
        if(!result){
            result=false
        }
        // return result
        return result
    },
    'getDtimeArr':function(){
        let monthStr=''
        let allTimeArr=[]
        let dayStr=String(Number(techoPage.pageSettings.day))
    
        let todayReg=new RegExp('(?<=##\\x20'+dayStr+'[D|日][^#]*)(\\d{4})-(\\d{4})\\x20([a-z]\\d?)\\x20([^\\n]+)','g')
        monthStr=techoPage.TDataStr.replace(this.regDiaryYAML,'')
        monthStr=monthStr.replace(/\n{2,}/g,'\n')
        monthStr.replace(todayReg,gatherArr)
        return allTimeArr
        function gatherArr(match, star, end,classStr,note,offset, string){
            allTimeArr.push([timeStrtoMNumber(star),timeStrtoMNumber(end)-timeStrtoMNumber(star),timeStrtoMNumber(end),classStr[0],note])    
        }
        
        //将 '0320'  -> 200  时间字符串转分钟 数字 
        function timeStrtoMNumber(timeStr,minute){
            let H=parseInt(timeStr.slice(0,2))
            let M=parseInt(timeStr.slice(2,4))
            return minute?60*H+M+minute:60*H+M
        }
    },
    'getDbillArr':function(){
        
        let monthStr=''
        let allBillArr=[]
        let dayStr=String(Number(techoPage.pageSettings.day))
        let todayReg=new RegExp('(?<=##\\x20'+dayStr+'[D|日][^#]*)\n(-?)(\\d+\\.?\\d?)\x20([a-z]\\d?)\\x20([^\n]+)','g')
        monthStr=techoPage.TDataStr.replace(this.regDiaryYAML,'')
        monthStr=monthStr.replace(/\n{2,}/g,'\n')
        monthStr.replace(todayReg,gatherArr)
        function gatherArr(match, PositiveOrnegative,number,classStr,billNote,offset, string){
            allBillArr.push([PositiveOrnegative?'-':'+',number,classStr,billNote])
        }
        return allBillArr
    }
    
    

    //输入字符串 返回匹配到的本月记录相关内容字符串
    
}

let calender={
    //这个属性放到page中去
    'moveToThisWeekFirstDay':function(date,starAtSunday){
        //true
        //0 1 2 3 4 5 6
        //0 -1 -2 -3 -4 -5 -6

        //false
        //0 1 2 3 4 5 6
        //-6 0 -1 -2 -3 -4 -5
        let weekday=date.getDay()
        let move=starAtSunday?[0,-1,-2,-3,-4,-5,-6]:[-6,0,-1,-2,-3,-4,-5]
        let movePace=move[weekday]
        let day=new Date()
        day.setTime(date.getTime()+movePace*24*60*60*1000)
        return day
    },
    'moveToThisWeekLastDay':function(date,starAtSunday){
        let weekday=date.getDay()
        let move=starAtSunday?[6,5,4,3,2,1,0]:[0,6,5,4,3,2,1]
        let movePace=move[weekday]
        let day=new Date()
        day.setTime(date.getTime()+movePace*24*60*60*1000)
        return day
    },
    'YMDarrToTime':function(arr){
        let str=arr[1]+'/'+arr[2]+'/'+arr[0]
        let date=new Date(str)
        return date
    },
    'YWWarrtoTime':function(arr,starAtSunday){
        let firstDay=new Date('01/01/'+arr[0])
        let firstWeekDay=new Date('01/01/'+arr[0])
        let moveFront
        let move
        if(starAtSunday){
            moveFront=firstDay.getDay()==0?0:7-firstDay.getDay()
            firstWeekDay.setTime(firstDay.getTime()+moveFront*24*60*60*1000)
            firstWeekDay.setHours(0,23,0)
            move=Number(arr[1]-1)*7+Number(arr[2])
        }else{
            moveFront=(firstDay.getDay()==0||firstDay.getDay()==1)?1-firstDay.getDay():8-firstDay.getDay()
            firstWeekDay.setTime(firstDay.getTime()+moveFront*24*60*60*1000)
            firstWeekDay.setHours(0,23,0)
            move=Number(arr[1]-1)*7+Number(arr[2]-1)
        }
        let today=new Date()
        today.setTime(firstWeekDay.getTime()+move*24*60*60*1000)
        return today
    },
    'timeToYMDarr':function(Data){
        let yearStr=String(Data.getFullYear())
        let monthStr=String(dataAnalyse.padleft((Data.getMonth()+1),2))
        let dayStr=String(dataAnalyse.padleft(Data.getDate(),2))
        let arr=[]
        arr.push(yearStr)
        arr.push(monthStr)
        arr.push(dayStr)
        return arr
    },
    'timeToYWWarr':function(Data,starAtSunday){
        let yearStr=String(Data.getFullYear())
        let weekDayStr=String(dataAnalyse.padleft(Data.getDay(),2))

        let firstDay=new Date('01/01/'+yearStr)
        let firstWeekDay=new Date('01/01/'+yearStr)
        if(starAtSunday){
            let moveFront=firstDay.getDay()==0?0:7-firstDay.getDay()
            firstWeekDay.setTime(firstDay.getTime()+moveFront*24*60*60*1000)
            firstWeekDay.setHours(0,23,0)
        }else{
            let moveFront=(firstDay.getDay()==0||firstDay.getDay()==1)?1-firstDay.getDay():8-firstDay.getDay()
            firstWeekDay.setTime(firstDay.getTime()+moveFront*24*60*60*1000)
            firstWeekDay.setHours(0,23,0)
        }
        let duration=Data.getTime()-firstWeekDay.getTime()
        let weekStr=String(dataAnalyse.padleft(Math.trunc(duration/(7*24*60*60*1000)+1),2))

        let arr=[]
        arr.push(yearStr)
        arr.push(weekStr)
        arr.push(weekDayStr)
        return arr
    },
    //输入年份，月份返回一个月历数组
    // [
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //    周,[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月],[日期,是否本月]
    //]
    'getMonthArr':function(){
        let year=Number(techoPage.pageSettings.year)
        let month=Number(techoPage.pageSettings.month)
        let starAtSunday=techoPage.pluginSettings.starAtSunday
        
        let result=[]
        let monthFirstDay=new Date(year,month-1,1,8,0,0)
        let monthLastDay=new Date(year,month,1,8,0,0)
        
        monthLastDay.setDate(monthLastDay.getDate()-1)
        
        let firstDay=this.moveToThisWeekFirstDay(monthFirstDay,starAtSunday)
        let lastDay=this.moveToThisWeekLastDay(monthLastDay,starAtSunday)
        

        let thisWeek=[]
        let today=new Date()
        for(let i=firstDay.getTime();i<=lastDay.getTime();i+=(24*60*60*1000)){
            today.setTime(i)
            //如果是一周起始
            if((today.getDay()+Number(starAtSunday))==1){
                thisWeek=[]
                let todayWeekNumber=this.timeToYWWarr(today,starAtSunday)[1]
                thisWeek.push(todayWeekNumber)
            }
            // let dayStr=''+(today.getMonth()+1)+'/'+today.getDate()
            let dayStr=''+today.getDate()
            //判断当前日是否为本月、是否为本日
            let todayState
            if((today.getMonth()+1)==month){
                todayState='thismonth'
                // if(today.getDate()==Number(techoPage.pageSettings.day)){
                //     todayState='thisday'
                // }
                
            }else {
                todayState='notthismonth'
            }
            
            thisWeek.push([dayStr,todayState])
            let isWeekday=starAtSunday?today.getDay()==6:today.getDay()==0
            if(isWeekday){
                result.push(thisWeek)
            }
        }
        return result
            
    },
    'getMonthDaysNumber':function(year,month){
        //月份 0-11 表示1-12月 超出部分自动顺延
        let lastday=new Date(year,month,1)
        lastday.setTime(lastday.getTime()-24*60*60*1000)
        return lastday.getDate()
    },

    'getWeekArr':function(year,weekNumber,starAtSunday){
        let weekArr=[]
        let yearStr=String(year)
        let weekNumberStr=dataAnalyse.padleft(weekNumber,2)
        let firstDayWeekday=dataAnalyse.padleft(starAtSunday?0:1,2)
        let firstWeekDay=this.YWWarrtoTime([yearStr,weekNumberStr,firstDayWeekday],starAtSunday)
        let todayStr
        for(let i=0;i<7;i++){
            // todayStr=''+firstWeekDay.getMonth()+'/'+firstWeekDay.getDate()
            todayStr=''+String(firstWeekDay.getFullYear())+dataAnalyse.padleft(firstWeekDay.getMonth()+1,2)+dataAnalyse.padleft(firstWeekDay.getDate(),2)
            todayDay=firstWeekDay.getDay()
            weekArr.push([todayStr,todayDay])
            firstWeekDay.setTime(firstWeekDay.getTime()+24*60*60*1000)
        }
        return weekArr
    },
    'minuteToDHM':function(minutes){
        let Day=Math.trunc(minutes/(24*60))
        let minutesLast=minutes-(24*60*Day)
        let hour=Math.trunc(minutesLast/60)
        minutesLast=minutesLast-60*hour
        let minute=Math.trunc(minutesLast)
    
        let result=[]
        result.push(Day)
        result.push(hour)
        result.push(minute)
        return result
    }
    
}
var techoPage={
    'pluginSettings':{},
    'pageSettings':{},
    'FDataStr':'',
    'TDataStr':'',
    'BDataStr':'',
    'calenderArr':[],

    'MtodoStrArr':[],
    'MtimeArr':[],
    'MbillArr':[],
    'MmainArr':[],
    
    'WtodoStrArr':[],
    'WtimeArr':[],
    'WbillArr':[],
    'WmainArr':[],
    
    'DtodoStrArr':[],
    'DtimeArr':[],
    'DbillArr':[],
    'DmainArr':[],
    'saveSetting':function(){
        dataAnalyse.writeFileTxt(dataAnalyse.pageSettingPath,  JSON.stringify(this.pageSettings))
    },
    // 读取 techoPageSetting.json 没有就写入并读取
    'refreSetting':function(){
        dataAnalyse.getFileTxt(dataAnalyse.pageSettingPath,  function(dataStr){
            if(dataStr){techoPage.pageSettings= JSON.parse(dataStr)}
            else{techoPage.pageSettings=DEFAULT_Page_SETTINGS
                dataAnalyse.writeFileTxt(dataAnalyse.pageSettingPath,  JSON.stringify(DEFAULT_Page_SETTINGS))
            }
        })
    },
    'creatEl':function(elTag,tagClass,id,inTxt){
        let el=document.createElement(elTag)
        if(!tagClass==''){el.setAttribute('class',tagClass)}
        if(!id==''){el.setAttribute('id',id)}
        if(!inTxt==''){el.append(document.createTextNode(inTxt))}
        return el
    },
    'addChildren':function  (fatherEl,childrenArr){
        for(var i=0;i<childrenArr.length;i++){
            fatherEl.appendChild(childrenArr[i])
        }
        return fatherEl
    },
    // 更新全部视图 或更新部分视图
    // IdArrMaybe: ['calender','littlewindow','main']
    'refreshView':function(IdArrMaybe){
        let oldNode
        let newNode
        let fatherNode
        idsArr=IdArrMaybe?IdArrMaybe:['techoCalender','littleWindow']
       
        for(let i=0;i<idsArr.length;i++){
            
            switch(idsArr[i]){
                case 'techoCalender':
                    oldNode=document.getElementById('techoCalender')
                    fatherNode=document.getElementById('techoLeftLeaf')
                    newNode=this.creatTechoCalender()
                break
                case 'littleWindow':
                    oldNode=document.getElementById('littleWindow')
                    fatherNode=document.getElementById('techoTag')
                    newNode=this.creatLittleWindow()
                break
                default:
                    oldNode=''
                    fatherNode=''
                    newNode=''
            }
            fatherNode.replaceChild(newNode,oldNode)
        }
    },
    'creatTechoPage':function(){
        let techoPage=this.creatEl('div','','techoPage','')
        let leftLeaf=this.creatEl('div','','techoLeftLeaf','')
        // let mainLeaf=this.creatEl('div','','techoMain','这是主界面')

        let calender=this.creatEl('div','','techoCalender','这里是小月历')

        let Tag=this.creatEl('div','selectBox','techoTag','')
        let selectTags=this.creatEl('div','selectTags','selectTags','')
        let todoTag=this.creatEl('btn','selectTag','todoTag','todo')
        let timeTag=this.creatEl('btn','selectTag','timeTag','时间')
        let billTag=this.creatEl('btn','selectTag','billTag','账单')
        let littleWindow=this.creatEl('div','littleWindow','littleWindow','这里是显示窗口')

        eval(this.pageSettings.selectedTagId).setAttribute('class','selectTag tagDisplay')

        // let littleWindow=this.creatElById(this.settings.selectedTagId)

        selectTags=this.addChildren(selectTags,[todoTag,timeTag,billTag])
        Tag=this.addChildren(Tag,[selectTags,littleWindow])
        leftLeaf=this.addChildren(leftLeaf,[calender,Tag])
        techoPage=this.addChildren(techoPage,[leftLeaf])
        
        return techoPage
    },
    'creatTechoCalender':function(){
        // let year=this.settings.year
        // let month=this.settings.month
        // let starAtSunday=this.settings.starAtSunday
        // sortedDate=this.techoCalenderMaker.getMonthArr(year,month,starAtSunday)
        // 根据日历数组生成日历table元素节点
        let techoCalender=this.creatEl('div','','techoCalender','')
        let calenderTable=this.creatEl('table','','calenderTable','')

        //生成表头
        let thead=this.creatEl('thead','','','')
        let tr1=this.creatEl('tr','','','')
        let theadTitle=this.pluginSettings.starAtSunday?['W','日','一','二','三','四','五','六']:['W','一','二','三','四','五','六','日']
        let th
        let tbody=this.creatEl('tbody','','','')
        let trForBody
        let weekNumberClass=''
        let dayclass=''
        for(var i=0;i<8;i++){
            th=this.creatEl('th','','',theadTitle[i])
            tr1.appendChild(th)
        }
        thead.appendChild(tr1)
        //生成表体
        
        for(let i=0;i<this.calenderArr.length;i++){
            trForBody=document.createElement('tr')
            let td
            for(let j=0;j<this.calenderArr[i].length;j++){
                if(j==0){
                    weekNumberClass=(techoPage.pageSettings.timeMode=='W'&&Number(this.calenderArr[i][0])==Number(techoPage.pageSettings.weekNumber))?'weekNumber calenderDisplay':'weekNumber'
                    td=this.creatEl('td',weekNumberClass,'',this.calenderArr[i][0])
                }else{
                    dayclass=(this.calenderArr[i][j][1]=='thismonth'&&techoPage.pageSettings.timeMode=='D'&&Number(this.calenderArr[i][j][0])==Number(techoPage.pageSettings.day))?this.calenderArr[i][j][1]+' calenderDisplay':this.calenderArr[i][j][1]
                    
                    td=this.creatEl('td',dayclass,'',this.calenderArr[i][j][0])
                }
                trForBody.appendChild(td) 
            }
            tbody.append(trForBody)
        }
        calenderTable.appendChild(thead)
        calenderTable.appendChild(tbody)
        let todayStr=this.pageSettings.year+'/'+this.pageSettings.month+'/'+this.pageSettings.day
        let monthClass=(techoPage.pageSettings.timeMode=='M')?'calenderMessageL calenderDisplay':'calenderMessageL'
                    
        let calenderMessage1=this.creatEl('span',monthClass,'message1',todayStr)
        
        let linkSpan=this.creatEl('span','calenderMessageL','linkSpan','')
        let Str=String(this.pageSettings.year)+String(this.pageSettings.month)
        let link=this.creatEl('a','internal-link','link',Str)
        link.setAttribute('data-href','techo/'+Str)
        link.setAttribute('href','techo/'+Str)
        link.setAttribute('target','_blank')
        link.setAttribute('rel','noopener')
        linkSpan.appendChild(link)

        let calenderMessage2=this.creatEl('span','calenderMessageR','message2','《')
        let calenderMessage3=this.creatEl('span','calenderMessageR','message3','<')
        let calenderMessage4=this.creatEl('span','calenderMessageR','message4','今')
        let calenderMessage5=this.creatEl('span','calenderMessageR','message5','>')
        let calenderMessage6=this.creatEl('span','calenderMessageR','message6','》')
        techoCalender.appendChild(calenderMessage1)
        techoCalender.appendChild(linkSpan)
        techoCalender.appendChild(calenderMessage6)
        techoCalender.appendChild(calenderMessage5)
        techoCalender.appendChild(calenderMessage4)
        techoCalender.appendChild(calenderMessage3)
        techoCalender.appendChild(calenderMessage2)
        techoCalender.appendChild(calenderTable)
        return techoCalender
    },
    'creatLittleWindow':function(){
        let resultEl=this.creatEl('div','','littleWindow','')
        let sortedDate
        let littleWindow=document.getElementById('littleWindow')
        let W=parseInt(window.getComputedStyle(littleWindow).width)
        let H=parseInt(window.getComputedStyle(littleWindow).height)
        let togetherNumber=0
        switch(this.pageSettings.selectedTagId){
            case 'todoTag':
                // let result=_this.creatEl('div','','littleWindow','')
                // 对与每一条任务  
                //直接将原笔记内容替换为li中的innerHTML
                
                switch(this.pageSettings.timeMode){
                    case 'M':
                        sortedDate=this.MtodoStrArr
                    break
                    case 'W':
                        sortedDate=this.WtodoStrArr
                    break
                    case 'D':
                        sortedDate=this.DtodoStrArr
                    break
                }
                let daskUl='<ul class="contains-task-list">'
                for(let i=0;i<sortedDate.length;i++){
                    //[连接名]()  -> a标签
                    
                    let undoBefor='<li data-line="0" data-task="" class="task-list-item"><input data-line="0" type="checkbox" class="task-list-item-checkbox">'
                    let doneBefor='<li data-line="0" data-task="x" class="task-list-item is-checked"><input data-line="0" checked="" type="checkbox" class="task-list-item-checkbox">'
                    //是否完成
                    if(sortedDate[i].match(dataAnalyse.regUndo)){
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regUndo,undoBefor)
                        sortedDate[i]+='</li>'
                    }else{
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regDone,doneBefor)
                        sortedDate[i]+='</li>'
                    }
                    //是否有完整链接
                    
                    if(sortedDate[i].match(dataAnalyse.regURL)){
                        
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regURLReplace,MdToLink)
                        //是否有简单的连接
                    }else if(sortedDate[i].match(dataAnalyse.regMinURL)){
                        
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regMinURLReplace,minMdToLink)
                    }else{
                    }
                    
                    //是否有本地连接--------
                    // <a data-href="插件笔记" href="插件笔记" class="internal-link" target="_blank" rel="noopener">插件笔记</a>
                    // <a data-href="工程笔记/待办" href="工程笔记/待办" class="internal-link" target="_blank" rel="noopener">工程笔记/待办</a>
                    if(sortedDate[i].match(dataAnalyse.regLocalFile)){
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regLocalFileReplace,mdFileLink)
                    }

                    //是否有邮箱
                    if(sortedDate[i].match(dataAnalyse.regEmal)){
                        sortedDate[i]=sortedDate[i].replace(dataAnalyse.regEmal,MdToEmal)
                    }
                    
                    
                    daskUl+=sortedDate[i]
                    function MdToLink(match, title, src, offset, string){
                        let a1='<a aria-label-position="top" aria-label="'
                        let a2='" rel="noopener" class="external-link" href="'
                        let a3='" target="_blank">'
                        let a4='</a>'
                        return a1+src+a2+src+a3+title+a4

                    }
                    //网页连接 ->a标签
                    function minMdToLink(match,  src, offset, string){
                        let a1='<a aria-label-position="top" aria-label="'
                        let a2='" rel="noopener" class="external-link" href="'
                        let a3='" target="_blank">'
                        let a4='</a>'
                        return a1+src+a2+src+a3+src+a4

                    }
                    //[[md文件]] -->a标签
                    // <a data-href="工程笔记/待办" href="工程笔记/待办" class="internal-link" target="_blank" rel="noopener">工程笔记/待办</a>
                    function mdFileLink(match, title, offset, string){
                        let a1='<a data-href="'
                        // 工程笔记/待办
                        let a2='" href="'
                        // 工程笔记/待办
                        let a3='" class="internal-link" target="_blank" rel="noopener">'
                        // 工程笔记/待办
                        let a4='</a>'
                        return a1+title+a2+title+a3+title+a4
                    }

                    function MdToEmal(match, emal, offset, string){
                        let a1='<a aria-label-position="top" aria-label="mailto:'
                        // 2030950597@qq.com
                        let a2='" rel="noopener" class="external-link" href="mailto:'
                        // 2030950597@qq.com
                        let a3='" target="_blank">'
                        // 2030950597@qq.com
                        let a4='</a>'
                        return a1+emal+a2+emal+a3+emal+a4
                    }
                }
                
                daskUl+='</ul>'
                resultEl.innerHTML=daskUl
            break
            case 'timeTag':
                // 根据 统计模式 时间模式确定
                // 直接根据统计模式确定
                // 根据时间模式确定数据来源
                // 根据统计方式展示绘图
                let timeSort=''
                // this.pageSettings.timeMode='D'
                // this.pluginSettings.WtimeSort='pie'
                switch(this.pageSettings.timeMode){
                    case 'M':
                        sortedDate=this.MtimeArr
                        timeSort=this.pluginSettings.MtimeSort
                        togetherNumber=calender.getMonthDaysNumber(techoPage.pageSettings.year,techoPage.pageSettings.month)
                        *24*60
                    break
                    case 'W':
                        sortedDate=this.WtimeArr
                        timeSort=this.pluginSettings.WtimeSort
                        togetherNumber=7*27*60
                    break
                    case 'D':
                        sortedDate=this.DtimeArr
                        timeSort=this.pluginSettings.DtimeSort
                        togetherNumber=24*60
                    break
                }
                
                let resultTxt
                switch(timeSort){
                    case 'pie':
                        resultTxt=creatPie(sortedDate,W,H,togetherNumber)
                    break
                    case 'bar':
                        resultTxt=creatBar(sortedDate,W,H)
                    break
                    case 'line':
                        resultTxt=creatLine(sortedDate,W,H)
                    break
                    case 'timeLines':
                        resultTxt=creatTimeLines(sortedDate,W,H)
                    break
                    case 'timeRing':
                        resultTxt=creatTimeRing(sortedDate,W,H,togetherNumber)
                    break
                    case 'timeLine':
                        resultTxt=creatTimeLine(sortedDate,W,H)
                    break
                }
                resultEl.innerHTML=resultTxt
                
                
            break
            
            case 'billTag':
                // this.pageSettings.timeMode='M'
                // this.pluginSettings
                let billSort=''
                switch(this.pageSettings.timeMode){
                    case 'M':
                        sortedDate=this.MbillArr
                        billSort=this.pluginSettings.MbillSort
                        togetherNumber=50
                    break
                    case 'W':
                        sortedDate=this.WbillArr
                        billSort=this.pluginSettings.WbillSort
                        togetherNumber=50
                    break
                    case 'D':
                        sortedDate=this.DbillArr
                        billSort=this.pluginSettings.DbillSort
                        togetherNumber=50
                    break
                }
                let billResultTxt
                switch(billSort){
                    case "line":
                        billResultTxt=creatBillLine(sortedDate,W,H)
                    break
                    case "inPie":
                        billResultTxt=creatBillPie(sortedDate,W,H)
                    break
                    case "outPie":
                        billResultTxt=creatBillPie(sortedDate,W,H)
                    break
                    case "table":
                        billResultTxt=creatBillTable(sortedDate,W,H)
                    break

                }
                if(billSort=="table"){
                    resultEl.appendChild(billResultTxt)
                }else{
                    resultEl.innerHTML=billResultTxt
                }
                
                
                
            break
            
        }
        return resultEl
        function creatPie(sortedDate,W,H,togetherNumber){
            let SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #ffffff00;'>"
            SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate(90 0 0)'>"
            SVGTxt+=creatRingSVG(W,H,togetherNumber,sortedDate)
            SVGTxt+="</g>"
            SVGTxt+=creatIndex(W,H,togetherNumber,sortedDate)
            SVGTxt+="</svg>"
            return SVGTxt
            function creatRingSVG(W,H,togetherNumber,sortedDate){
                let r=0.35
                let circumference=3.1415926*r*W*2
                // 记录当前占比到了n%
                let locatedNow=0
                // 记录生成的字符串
                let longNow
                let result=''
                for(let i=0;i<sortedDate.length;i++){
                    longNow=sortedDate[i][1]/togetherNumber
                    locatedNow=sortedDate[i][2]?(sortedDate[i][2]/togetherNumber):locatedNow
                    result+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(r*W)+"' fill='#00000000' stroke-width='30' stroke-dashoffset='"+(-locatedNow*circumference)+"' stroke-dasharray='"+(longNow*circumference)+"  "+(circumference)+"' stroke='var(--"+sortedDate[i][0]+"color)'/>"
                    locatedNow+=longNow
                }
                return result
            }
            function creatIndex(W,H,togetherNumber,sortedDate){
                let resultTxt=''
                let txtY=~~(1*W)
                let txtX=~~(0.05*W)

                for(let j=0;j<sortedDate.length;j++){
                    resultTxt+="<rect width='10' height='10' x='"+txtX+"' y='"+txtY+"' fill='var(--"+sortedDate[j][0]+"color' />"
                    resultTxt+="<text fill=var(--txt) x='"+(txtX+15)+"' y='"+(txtY+8)+"' font-size='15' >"+sortedDate[j][0]+":</text>"
                    let DHMArr=calender.minuteToDHM (sortedDate[j][1])
                    if(DHMArr[0]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+50)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[0]+"D</text>"
                    }
                    if(DHMArr[1]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+80)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[1]+"H</text>"
                    }
                    if(DHMArr[2]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+120)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[2]+"m</text>"
                    }
                    resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+170)+"' y='"+(txtY+8)+"' font-size='15' >"+(100*sortedDate[j][1]/togetherNumber).toFixed(1)+"%</text>"
                    txtY+=20
                }
                return resultTxt
            }

        }
        function creatBar(sortedDate,W,H){
            
            const padTop=5
            const padBottom=5
            const padLeft=18
            const padRight=7
            // 共计xx小时
            let nonthTimeSum=0
            let Havailable=H-padTop-padBottom
            let Wavailable=W-padLeft-padRight


            let B_itemH=sortedDate.length>27?~~(Havailable/32):~~(Havailable/9)
            // (Havailable/(sortedDate.length+1))

            let B_itemLocalY
            let B_itemLocalW
            let color=techoPage.pluginSettings.timeSortClass=='all'?'var(--allItemTime)':'var(--'+techoPage.pluginSettings.timeSortClass+'color)'
            let SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #ffffff00;'>"
            SVGTxt+="<g transform='translate(0,"+~~(0.5*B_itemH)+")'>"
            for(let i=0;i<sortedDate.length;i++){
                nonthTimeSum+=Number(sortedDate[i][2])
                B_itemLocalY=B_itemH*i+padTop
                B_itemLocalW=~~(sortedDate[i][1]*Wavailable/1440)
                SVGTxt+="<text fill=var(--txt) x='"+(padLeft-5)+"' y='"+~~(B_itemLocalY+0.4*B_itemH)+"' text-anchor='end' font-size='10' >"+(i+1)+"  </text>"
                SVGTxt+="<line x1='"+padLeft+"' y1='"+~~B_itemLocalY+"' x2='"+(padLeft+B_itemLocalW)+"' y2='"+~~B_itemLocalY+"'  stroke="+color+" stroke-width='"+~~(B_itemH*0.8)+"'></line>"
                // SVGTxt+="<line x1='"+padLeft+"' y1='"+~~B_itemLocalY+"' x2='"+(padLeft+B_itemLocalW)+"' y2='"+~~B_itemLocalY+"'  stroke='#f00' stroke-width='1'></line>"
                SVGTxt+="<text fill=var(--txt) x='"+(5+padLeft+B_itemLocalW)+"' y='"+~~(B_itemLocalY+0.4*B_itemH)+"' font-size='10' >"+sortedDate[i][2]+"H</text>"
            }
            SVGTxt+="<text fill=var(--txt) x='"+0.5*W+"' y='"+~~(B_itemLocalY+1.4*B_itemH)+"' text-anchor='middle' font-size='10' >"+techoPage.pluginSettings.timeSortClass+"：共计"+nonthTimeSum.toFixed(1)+"H  日均"+(nonthTimeSum/sortedDate.length).toFixed(1)+"H</text>"
            SVGTxt+="<line x1='"+padLeft+"' y1='"+(padTop-0.4*B_itemH)+"' x2='"+padLeft+"' y2='"+(B_itemLocalY+0.4*B_itemH)+"'  stroke=var(--vLine) stroke-width='1'></line>"
            
            
            
            
            SVGTxt+="</g>"
            SVGTxt+="</svg>"
            // ==========

            
            return SVGTxt
        }
        function creatLine(sortedDate,W,H){
            const padTop=5
            const padBottom=5
            const padLeft=18
            const padRight=7
            // 共计xx小时
            let nonthTimeSum=0
            let Havailable=H-padTop-padBottom
            let Wavailable=W-padLeft-padRight

            let L_itemH=sortedDate.length>27?~~(Havailable/32):~~(Havailable/9)
            let L_itemLocalY
            let L_itemLocalW
            let color=techoPage.pluginSettings.timeSortClass=='all'?'var(--allItemTime)':'var(--'+techoPage.pluginSettings.timeSortClass+'color)'

            SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #00000000;'>"
            SVGTxt+="<g transform='translate(0,"+~~(0.5*L_itemH)+")'>"

            let line="<polyline points='"
            // 10,10 100,20 80,85 28,55
            
            // []
            // 将统计数据转化成点坐标 装到一个数组
            // 根据坐标数组生成折线、点 、标注
            let points=''
            let thisPoint=[]
            for(let i=0;i<sortedDate.length;i++){
                nonthTimeSum+=Number(sortedDate[i][2])
                //该条记录的宽 高
                L_itemLocalY=L_itemH*i+padTop
                L_itemLocalW=~~(sortedDate[i][1]*Wavailable/1440)

                // 根据宽高 存入坐标点
                thisPoint=[(padLeft+L_itemLocalW),(~~L_itemLocalY)]
                points+='  '+(padLeft+L_itemLocalW)+','+(L_itemLocalY)+'   '
                //左侧的日期
                SVGTxt+="<text fill=var(--txt) x='"+(padLeft-5)+"' y='"+~~(L_itemLocalY+0.4*L_itemH)+"' text-anchor='end' font-size='10' >"+(i+1)+"  </text>"
                SVGTxt+="<line x1='"+padLeft+"' y1='"+~~L_itemLocalY+"' x2='"+(padLeft+Wavailable)+"' y2='"+~~L_itemLocalY+"'  stroke='"+(i%2==0?'var(--oddBar)':'var(--evelBar)')+"' stroke-width='"+~~L_itemH+"'></line>"
                // SVGTxt+="<line x1='"+padLeft+"' y1='"+~~L_itemLocalY+"' x2='"+(padLeft+L_itemLocalW)+"' y2='"+~~L_itemLocalY+"'  stroke='#f00' stroke-width='1'></line>"
                // 绘制点
                SVGTxt+="<circle cx='"+(padLeft+L_itemLocalW)+"' cy='"+~~L_itemLocalY+"' r='2'  fill=var(--point) />"
                
                // 绘制右侧标注
                SVGTxt+="<text fill=var(--txt) x='"+(5+padLeft+L_itemLocalW)+"' y='"+~~(L_itemLocalY+0.4*L_itemH)+"' font-size='10' >"+sortedDate[i][2]+"H</text>"
            }
            SVGTxt+="<text fill=var(--txt) x='"+0.5*W+"' y='"+~~(L_itemLocalY+L_itemH)+"' text-anchor='middle' font-size='10' >共计"+nonthTimeSum.toFixed(1)+"H  日均"+(nonthTimeSum/sortedDate.length).toFixed(1)+"H</text>"
            SVGTxt+="<line x1='"+padLeft+"' y1='"+(padTop-0.4*L_itemH)+"' x2='"+padLeft+"' y2='"+(L_itemLocalY+0.4*L_itemH)+"'  stroke='var(--vLine)' stroke-width='1'></line>"
            

            line+=points
            line+="' fill='#00000000' stroke="+color+" stroke-width='2'>"
            SVGTxt+=line
            SVGTxt+="</g>"
            SVGTxt+="</svg>"
            return SVGTxt
        }
        function creatTimeLines(sortedDate,W,H){
            const padTop=5
            const padBottom=5
            const padLeft=5
            const padRight=5
            let Havailable=H-padTop-padBottom
            let Wavailable=W-padLeft-padRight
            const itemWidth=~~(Wavailable/7)
            let xLocated=padLeft+itemWidth/2
            let yLocated=padTop
            let yEnded=0

            let verticalLine=''
            let horizontalLine=''
            let SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #00000000;'>"
            for(let i=0;i<sortedDate.length;i++){
                for(let j=0;j<sortedDate[i].length;j++){
                    yLocated=padTop+Havailable*(sortedDate[i][j][0]/1440)
                    yEnded=padTop+Havailable*((sortedDate[i][j][0]+sortedDate[i][j][1])/1440)
                    SVGTxt+="<line x1='"+xLocated+"' y1='"+yLocated+"' x2='"+xLocated+"' y2='"+yEnded+"'  stroke=var(--"+sortedDate[i][j][2][0]+"color) stroke-width='"+itemWidth+"'></line>"
                    // SVGTxt+="<line x1='"+xLocated+"' y1='"+yLocated+"' x2='"+xLocated+"' y2='"+yEnded+"'  stroke='#289' stroke-width='"+itemWidth+"'></line>"
                    // verticalLine+="<line x1='"+~~(xLocated+itemWidth/2)+"' y1='"+padTop+"' x2='"+~~(xLocated+itemWidth/2)+"' y2='"+(padTop+Havailable)+"'  stroke='#555' stroke-width='1'></line>"
            
                }
                xLocated+=itemWidth
            }
            let oneHoueHeigh=Havailable/24
            for(let k=0;k<8;k++){
                let x=padLeft+k*itemWidth
                verticalLine+="<line x1='"+x+"' y1='"+padTop+"' x2='"+x+"' y2='"+(padTop+Havailable)+"'  stroke='var(--vLine)' stroke-width='1'></line>"
            
            }
            for(let j=0;j<25;j++){
                let y=j*oneHoueHeigh+padTop
                if(j%6==0){
                    horizontalLine+="<line x1='"+padLeft+"' y1='"+y+"' x2='"+(padLeft+7*itemWidth)+"' y2='"+y+"'  stroke='var(--sixClock)' stroke-width='2'></line>"
            
                }else if(j%3==0){
                    horizontalLine+="<line x1='"+padLeft+"' y1='"+y+"' x2='"+(padLeft+7*itemWidth)+"' y2='"+y+"'  stroke='var(--threeClock)' stroke-width='2'></line>"
            
                }else{
                    horizontalLine+="<line x1='"+padLeft+"' y1='"+y+"' x2='"+(padLeft+7*itemWidth)+"' y2='"+y+"'  stroke='var(--hLine)' stroke-width='1'></line>"
            
                }
            }
            SVGTxt+=verticalLine
            SVGTxt+=horizontalLine
                    
            SVGTxt+='</svg>'
            
            return SVGTxt
        }
        function creatTimeLine(sortedDate,W,H){
            const padTop=5
            const padBottom=5
            const padLeft=5
            const padRight=5
            let Havailable=H-padTop-padBottom
            let Wavailable=W-padLeft-padRight
            const itemWidth=5
            let xLocated=5
            let yLocated=padTop
            let yEnded=0

            let SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #00000000;'>"
      

            for(let j=0;j<25;j++){
                if(j%6==0){
                    SVGTxt+="<line x1='"+(2*xLocated+itemWidth)+"' y1='"+(padTop+j*Havailable/24)+"' x2='"+(W-padRight)+"' y2='"+(padTop+j*Havailable/24)+"'  stroke='var(--sixClock)' stroke-width='2'></line>"
                }else if(j%3==0){
                    SVGTxt+="<line x1='"+(2*xLocated+itemWidth)+"' y1='"+(padTop+j*Havailable/24)+"' x2='"+(W-padRight)+"' y2='"+(padTop+j*Havailable/24)+"'  stroke='var(--threeClock)' stroke-width='2'></line>"
                }else{
                    SVGTxt+="<line x1='"+(2*xLocated+itemWidth)+"' y1='"+(padTop+j*Havailable/24)+"' x2='"+(W-padRight)+"' y2='"+(padTop+j*Havailable/24)+"'  stroke='var(--hLine)' stroke-width='1'></line>"
                }
            }

            for(let i=0;i<sortedDate.length;i++){
                yLocated=padTop+Havailable*(sortedDate[i][0]/1440)
                yEnded=padTop+Havailable*(sortedDate[i][2]/1440)
                SVGTxt+="<line x1='"+xLocated+"' y1='"+yLocated+"' x2='"+xLocated+"' y2='"+yEnded+"'  stroke=var(--"+sortedDate[i][3][0]+"color) stroke-width='"+itemWidth+"'></line>"
                SVGTxt+="<foreignObject width='"+(W-10)+"' height='"+(Havailable*(sortedDate[i][1]/1440))+"' x='15' y='"+yLocated+"'><div class='time-note'>"+(sortedDate[i][4])+"</div></foreignObject>"
                // SVGTxt+="<line x1='"+xLocated+"' y1='"+yLocated+"' x2='"+xLocated+"' y2='"+yEnded+"'  stroke='#289' stroke-width='"+itemWidth+"'></line>"
                // <foreignObject width='180' height='140' x='20' y='0'><div class='time-note'>睡觉</div></foreignObject>
            }
            
            
    
            SVGTxt+='</svg>'
            
            return SVGTxt
        }
        function creatTimeRing(sortedDate,W,H,togetherNumber){
            let ringWidth=12
            SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #ffffff00;'>"
            SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate(90 0 0)'>"
                   

            SVGTxt+=creatRingSVG(W,H,togetherNumber,sortedDate,ringWidth)
            SVGTxt+="</g>"
            let helfWidth=0.5*ringWidth
            let dayStr=techoPage.pageSettings.year+"/"+techoPage.pageSettings.month+"/"+techoPage.pageSettings.day

            SVGTxt+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(0.35*W-helfWidth)+"' fill='#00000000' stroke-width='1'  stroke=var(--circleLine) />"
            SVGTxt+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(0.35*W+helfWidth)+"' fill='#00000000' stroke-width='1'  stroke=var(--circleLine) />"
            SVGTxt+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(0.35*W+helfWidth)+"' fill='#00000000' stroke-width='1'  stroke=var(--circleLine) />"
            SVGTxt+="<line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"' stroke=var(--circleLine) stroke-width='2' />"
            SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate(15 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate(30 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(45 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(60 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(75 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(90 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(105 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(120 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(135 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(150 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(165 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
           
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(180 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(195 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(210 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(225 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(240 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(255 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(270 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(285 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(300 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(315 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.32*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='2' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(330 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            SVGTxt+="<g transform-origin='"+~~(0.5*W)+' '+~~(0.5*W)+"' transform='rotate(345 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine1) stroke-width='1' /></g>"
            // SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate( 0 0)'><line x1='"+~~(0.5*W)+"' y1='"+(~~(0.5*W)+~~(0.35*W-helfWidth))+"' x2='"+~~(0.5*W)+"' y2='"+(~~(0.5*W)+~~(0.35*W+helfWidth))+"'stroke=var(--circleLine) stroke-width='1' /></g>"
            SVGTxt+="<text fill=var(--txt) text-anchor='middle' x='"+~~(0.5*W)+"' y='"+~~(0.5*W+8)+"' font-size='15' >"+dayStr+"</text>"
            SVGTxt+=creatIndex(W,H,sortedDate)
            SVGTxt+="</svg>"
            return SVGTxt
            function creatRingSVG(W,H,togetherNumber,sortedDate,ringWidth){
                let ringRealWidth=ringWidth?ringWidth:30
                let r=0.35
                let circumference=3.1415926*r*W*2
                // 记录当前占比到了n%
                let locatedNow=0
                // 记录生成的字符串
                let longNow
                let result=''
                for(let i=0;i<sortedDate.length;i++){
                    longNow=sortedDate[i][1]/togetherNumber
                    locatedNow=sortedDate[i][0]/togetherNumber
                    result+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(r*W)+"' fill='#00000000' stroke-width='"+ringRealWidth+"' stroke-dashoffset='"+(-locatedNow*circumference)+"' stroke-dasharray='"+(longNow*circumference)+"  "+(circumference)+"' stroke='var(--"+sortedDate[i][3][0]+"color)'/>"
                    
                }
                return result
            }
            function creatIndex(W,H,sortedDate){
                let timeClassHaveMeet=[]
                let resultArr=[]
                let thisIndex
                // 遍历每个时间记录
                // 如果时间种类已经出现 将这个记录附加到对应的元素数组
                // 否则 在timeClassHaveMeet 中记录下这个数组，将相关记录直接添加到记录数组中
                for(let i=0;i<sortedDate.length;i++){
                    //如果已经遇到过该类
                    let thisClass=sortedDate[i][3][0]
                    if(timeClassHaveMeet.includes(thisClass)){
                        thisIndex=resultArr.findIndex(isThisClass)
                        resultArr[thisIndex]=[resultArr[thisIndex][0],sortedDate[i][1]+resultArr[thisIndex][1]]
                    }else{
                        timeClassHaveMeet.push(thisClass)
                        resultArr.push([sortedDate[i][3][0],sortedDate[i][1]])
                    }

                    function isThisClass(element){
                        let thisClassReg=new RegExp('^'+thisClass+'\\d?$')
                        return thisClassReg.test(element[0])
                    }
                    
                }
                resultArr.sort(function (a, b) {return b[1]-a[1]})


                let resultTxt=''
                let txtY=~~(1*W)
                let txtX=~~(0.2*W)

                for(let j=0;j<resultArr.length;j++){
                    resultTxt+="<rect width='10' height='10' x='"+txtX+"' y='"+txtY+"' fill='var(--"+resultArr[j][0]+"color' />"
                    resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+25)+"' y='"+(txtY+8)+"' font-size='15' >"+resultArr[j][0]+":</text>"
                    let DHMArr=calender.minuteToDHM (resultArr[j][1])
                    if(DHMArr[0]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+50)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[0]+"D</text>"
                    }
                    if(DHMArr[1]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+100)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[1]+"H</text>"
                    }
                    if(DHMArr[2]){
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+150)+"' y='"+(txtY+8)+"' font-size='15' >"+DHMArr[2]+"m</text>"
                    }
                    txtY+=20
                }
                return resultTxt
            }
        }
        function creatBillTable(sortedDate,W,H){
            let billTable=document.createElement('table')
            billTable.setAttribute('id','techoBillTable')
            let thead=document.createElement('thead')
            let tbody=document.createElement('tbody')
            let tr
            let td1
            let td2
            let td3
            thead.innerHTML="<tr><th>金额</th><th>类别</th><th>备注</th></tr>"
            billTable.appendChild(thead)

            for(let i=0;i<sortedDate.length;i++){
                tr=document.createElement('tr')
                td1=document.createElement('td')
                td2=document.createElement('td')
                td3=document.createElement('td')
                td1.append(document.createTextNode(sortedDate[i][0]+sortedDate[i][1]))
                td2.append(document.createTextNode(sortedDate[i][2]))
                td3.append(document.createTextNode(sortedDate[i][3]))

                tr.setAttribute('class',sortedDate[i][0]=='-'?'billOut':'billIn')

                tr.appendChild(td1)
                tr.appendChild(td2)
                tr.appendChild(td3)
                tbody.appendChild(tr)

            }
            billTable.appendChild(tbody)
            return billTable
        }
        function creatBillLine(sortedDate,W,H){
            const padTop=5
            const padBottom=5
            const padLeft=18
            const padRight=7
            const NumberMax=techoPage.pluginSettings.billDayNumber
            let Vpace=techoPage.pluginSettings.billDayPace
            // 共计xx小时
            let BillInSum=0
            let BillOutSum=0
            let Havailable=H-padTop-padBottom
            let Wavailable=W-padLeft-padRight

            let L_itemH=sortedDate[0].length>27?~~(Havailable/32):~~(Havailable/9)

            SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #00000000;'>"
            SVGTxt+="<g transform='translate(0,"+~~(-0.5*L_itemH)+")'>"
            let pointsIn=''
            let pointsOut=''
            let colorLine=''
            let indexLine=''
            let thisXIn
            let thisYIn
            let thisXOut
            let thisYOut
            let weekdayArr=techoPage.pluginSettings.starAtSunday?['日','一','二','三','四','五','六']:['一','二','三','四','五','六','日']
            // 10,10 100,20 80,85 28,55
            for(let i=0;i<sortedDate[0].length;i++){
                thisXIn=padLeft+Wavailable*(sortedDate[0][i]/NumberMax)
                thisYIn=padTop+(i+1)*L_itemH
                pointsIn+=' '+thisXIn+','+thisYIn+' '
                BillInSum+=sortedDate[0][i]

                thisXOut=padLeft+Wavailable*(sortedDate[1][i]/NumberMax)
                thisYOut=padTop+(i+1)*L_itemH
                pointsOut+=' '+thisXOut+','+thisYOut+' '
                BillOutSum+=sortedDate[1][i]

                let index=techoPage.pageSettings.timeMode=='W'?weekdayArr[i]:i+1
                SVGTxt+="<text fill=var(--txt) x='"+(padLeft-5)+"' y='"+(padTop+(i+1)*L_itemH+5)+"' text-anchor='end' font-size='10' >"+index+"</text>"
                colorLine+="<line x1='"+padLeft+"' y1='"+(padTop+(i+1)*L_itemH)+"' x2='"+W+"' y2='"+(padTop+(i+1)*L_itemH)+"'  stroke='"+(i%2==0?'var(--oddBar)':'var(--evelBar)')+"' stroke-width='"+~~L_itemH+"'></line>"
                // colorLine+="<line x1='"+padLeft+"' y1='"+(padTop+(i+1)*L_itemH)+"' x2='"+W+"' y2='"+(padTop+(i+1)*L_itemH)+"'  stroke='#000' stroke-width='1'></line>"

            }
            let VlineNumber=~~(NumberMax/Vpace)+1
            for(let j=0;j<VlineNumber;j++){
                indexLine+="<line x1='"+(padLeft+Wavailable*(Vpace*j/NumberMax))+"' y1='"+(padTop)+"' x2='"+(padLeft+Wavailable*(Vpace*j/NumberMax))+"' y2='"+(padTop+(sortedDate[0].length+0.5)*L_itemH)+"'  stroke='#777' stroke-width='1'></line>"
                
            }
            let lineIn="<polyline points='"
            lineIn+=pointsIn
            lineIn+="' fill='#00000000' stroke=var(--inBillLine) stroke-width='1'/>"
            
            let lineOut="<polyline points='"
            lineOut+=pointsOut
            lineOut+="' fill='#00000000' stroke=var(--outBillLine) stroke-width='1'/>"
            SVGTxt+="<text fill=var(--txt) x='"+0.5*W+"' y='"+((sortedDate[0].length+1.5)*L_itemH+padTop)+"' text-anchor='middle' font-size='10' >收："+BillInSum.toFixed(1)+"&nbsp;&nbsp;支"+BillOutSum.toFixed(1)+"&nbsp;&nbsp;合："+(BillInSum-BillOutSum).toFixed(1)+"</text>"
            SVGTxt+="<line x1='"+padLeft+"' y1='"+(padTop)+"' x2='"+padLeft+"' y2='"+((sortedDate[0].length+0.5)*L_itemH+padTop)+"'  stroke=var(--vLine) stroke-width='1'></line>"
            

            SVGTxt+=colorLine
            SVGTxt+=indexLine
            SVGTxt+=lineIn
            SVGTxt+=lineOut
            SVGTxt+="</g>"
            SVGTxt+="</svg>"
            return SVGTxt
        }
        function creatBillPie(sortedDate,W,H){
            let allMoney=0
            let SVGTxt='当前无账单'
            for(let i=0;i<sortedDate.length;i++){
                allMoney+=sortedDate[i][1]
            }
            if(!(allMoney==0)){
                SVGTxt="<svg   viewBox='0 0 "+W+"  "+H+"' style='background: #ffffff00;'>"
                SVGTxt+="<g transform-origin='"+0.5*W+' '+0.5*W+"' transform='rotate(90 0 0)'>"
                SVGTxt+=creatRingSVG(W,H,allMoney,sortedDate)
                SVGTxt+="</g>"
                SVGTxt+=creatIndex(W,H,allMoney,sortedDate)
                let billIcon=eval("techoPage.pluginSettings."+techoPage.pageSettings.timeMode+'billSort')=='inPie'?'收入':'支出'
                SVGTxt+="<text fill=var(--txt) x='"+~~(0.5*W)+"' y='"+(~~(0.5*W)+8)+"' font-size='15' text-anchor='middle' >"+billIcon+"</text>"
                SVGTxt+="</svg>"
                
                function creatRingSVG(W,H,togetherNumber,sortedDate){
                    let r=0.35
                    let circumference=Number((3.14*r*2*W).toFixed(2))
                    // 记录当前占比到了n%
                    let locatedNow=0
                    // 记录生成的字符串
                    let longNow
                    let result=''
                    for(let i=0;i<sortedDate.length;i++){
                        longNow=Number((sortedDate[i][1]/togetherNumber).toFixed(2))
                        // locatedNow=sortedDate[i][2]?(sortedDate[i][2]/togetherNumber):locatedNow
                        result+="<circle  cx='"+~~(0.5*W)+"' cy='"+~~(0.5*W)+"' r='"+~~(r*W)+"' fill='#00000000' stroke-width='30' stroke-dashoffset='"+(-locatedNow*circumference)+"' stroke-dasharray='"+(longNow*circumference)+"  "+(circumference)+"' stroke='var(--"+sortedDate[i][0].toUpperCase()+"color)'/>"
                        locatedNow+=longNow
                    }
                    return result
                }
                function creatIndex(W,H,togetherNumber,sortedDate){
                    let resultTxt=''
                    let txtY=~~(1*W)
                    let txtX=~~(0.05*W)

                    for(let j=0;j<sortedDate.length;j++){
                        resultTxt+="<rect width='10' height='10' x='"+txtX+"' y='"+txtY+"' fill='var(--"+sortedDate[j][0].toUpperCase()+"color' />"
                        resultTxt+="<text fill=var(--txt) x='"+(txtX+15)+"' y='"+(txtY+8)+"' font-size='15' >"+sortedDate[j][0].toUpperCase()+":</text>"
                        
                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+80)+"' y='"+(txtY+8)+"' font-size='15' >"+sortedDate[j][1]+"</text>"

                        resultTxt+="<text fill=var(--txt) text-anchor='end' x='"+(txtX+170)+"' y='"+(txtY+8)+"' font-size='15' >"+(100*sortedDate[j][1]/togetherNumber).toFixed(1)+"%</text>"
                        txtY+=20
                    }
                    return resultTxt
                }




            }
            return  SVGTxt
        }
    }
    //更新数据
    // 计算数据
    // 生成新页面
    // 替换旧页面
    
}






function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

class MyPlugin extends obsidian.Plugin {
    async onload() {
        await this.loadSettings();
        this.registerMarkdownPostProcessor(function(el,ctx){
            //进入新页面之前先保存设置
            if(!techoPage.pageSettings.name.match(/TechoShow.md$/)){
                techoPage.saveSetting()
            }
            // 如果是规定的标题
            if(ctx.sourcePath.match(/TechoShow.md$/)){
                techoPage.refreSetting()
                if(techoPage.pageSettings.name!==ctx.sourcePath){
                    // 设置骨架
                    el.innerHTML=techoPage.creatTechoPage().innerHTML

                    el.setAttribute('id','techoPage')
                    dataAnalyse.refreshDataAndView()
                    
                //    techoPage.refreshIds(['techoCalender',techoPage.pageSettings.selectedTagId,'techoMain'])
                }else{
                    el=null
                }
            }
            techoPage.pageSettings.name=ctx.sourcePath
        })
        this.registerDomEvent(document, 'click', (evt) => {
            //根据类名批量绑定事件 代理事件
            switch(evt.target.className){
                case 'selectTag':
                    let oldTag=document.getElementById(techoPage.pageSettings.selectedTagId)
                    oldTag.setAttribute('class','selectTag')
                    evt.target.setAttribute('class','selectTag tagDisplay')
                    techoPage.pageSettings.selectedTagId=evt.target.id
                    techoPage.refreshView(['littleWindow'])
                    
                
                break
                //点击左上 2022/05/21 跳转到月识图
                
                case 'calenderMessageL':
                        techoPage.pageSettings.timeMode='M'
                        dataAnalyse.refreshDataAndView()
                break
                case 'calenderMessageR':
                    switch(evt.target.id){
                        // 上一年
                        case 'message2':
                            techoPage.pageSettings.timeMode='M'
                            techoPage.pageSettings.year=String(Number(techoPage.pageSettings.year-1))
                            techoPage.pageSettings.day='01'
                            dataAnalyse.refreshDataAndView()
                        break
                        // 上一月
                        case 'message3':
                            techoPage.pageSettings.timeMode='M'
                            if(techoPage.pageSettings.month=='01'){
                                techoPage.pageSettings.year=String(Number(techoPage.pageSettings.year-1))
                                techoPage.pageSettings.month='12'
                            }else{
                                techoPage.pageSettings.month=dataAnalyse.padleft(Number(techoPage.pageSettings.month)-1,2)
                            }
                            techoPage.pageSettings.day='01'
                            dataAnalyse.refreshDataAndView()
                        break
                        // 当下
                        case 'message4':
                            
                            techoPage.pageSettings.year
                            techoPage.pageSettings.timeMode='D'
                            let today=new Date()

                            techoPage.pageSettings.year=String(today.getFullYear())
                            techoPage.pageSettings.month=dataAnalyse.padleft(today.getMonth()+1,2)
                            techoPage.pageSettings.day=dataAnalyse.padleft(today.getDate(),2)
                            dataAnalyse.refreshDataAndView()
                        break
                        // 下一月
                        case 'message5':
                            techoPage.pageSettings.timeMode='M'
                            if(techoPage.pageSettings.month=='12'){
                                techoPage.pageSettings.year=String(Number(techoPage.pageSettings.year+1))
                                techoPage.pageSettings.month='01'
                            }else{
                                techoPage.pageSettings.month=dataAnalyse.padleft(Number(techoPage.pageSettings.month)+1,2)
                            }
                            techoPage.pageSettings.day='01'
                            dataAnalyse.refreshDataAndView()

                        break
                        // 下一年
                        case 'message6':
                            techoPage.pageSettings.timeMode='M'
                            techoPage.pageSettings.year=String(Number(techoPage.pageSettings.year)+1)
                            techoPage.pageSettings.day='01'
                            dataAnalyse.refreshDataAndView()
                        break
                    }
                break
                // 点击周数
                
                case 'weekNumber':
                    
                    let targetWeekNumber=evt.target.innerHTML
                    if(!(Number(targetWeekNumber)==Number(techoPage.pageSettings.weekNumber))){
                        techoPage.pageSettings.weekNumber=targetWeekNumber
                        techoPage.pageSettings.timeMode='W'
                        dataAnalyse.calculateData()
                        // setTimeout(techoPage.refreshView, 3000 )
                        techoPage.refreshView()

                    }else{
                        techoPage.pageSettings.timeMode='W'
                        techoPage.refreshView()
                    }
                break
                // 点击日期
                case 'thismonth':
                    // let oldDayNodes=document.getElementsByClassName('calenderDisplay')
                    // if(oldDayNodes[0]){
                    //     oldDayNodes[0].setAttribute('class','thismonth')
                    // }
                    // evt.target.setAttribute('class','thismonth calenderDisplay')
                    let targetDayNumber=evt.target.innerHTML
                    if(!(Number(targetDayNumber)==Number(techoPage.pageSettings.day))){
                        techoPage.pageSettings.day=targetDayNumber
                        techoPage.pageSettings.timeMode='D'
                        dataAnalyse.calculateData('D')
                        techoPage.refreshView()
                    }else{
                        techoPage.pageSettings.timeMode='D'

                        techoPage.refreshView()
                    }
                break
                default:
                
                

            }
            switch(evt.target.id){
                
                
            }
            // if(evt.target.nodeName.toLowerCase()=='btn'){
                
            //     //替换这里
            //     let oldDisplay=document.querySelectorAll('btn.techoDisplay')[0]
            //     oldDisplay.setAttribute('class',techoPage.pageSettings.showPageId)
                
            //     dataAnalyse.refreshDateToEl('202205',evt.target.id)
            //     evt.target.setAttribute('class',evt.target.id+'   techoDisplay')

            // }
        });
        this.registerInterval(window.setInterval(function(){
            if(document.getElementById('techoPage')){
                dataAnalyse.refreshDataAndView()
            }
        }, 60000));
        this.addSettingTab(new SampleSettingTab(this.app, this));
    }
    // abbindTechoEvent=function(){
    //     document.querySelectorAll('.techoIndexs').forEach(function (indexButton) {
            
    //         indexButton.addEventListener('click', function () {
    //         });
    //     });

    // }
    async writeOptions(changeOpts) {
        this.settings.update((old) => (Object.assign(Object.assign({}, old), changeOpts(old))));
        await this.saveData(this.options);
    }
    // 关闭插件前写入设置
    onunload() {
        techoPage.saveSetting()
    }
    loadProcessor() {
        this.registerMarkdownPostProcessor((el, ctx) => __awaiter(this, void 0, void 0, function* () {

        }));
    }
    loadSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            this.settings = Object.assign({}, DEFAULT_SETTINGS, yield this.loadData());
            techoPage.pluginSettings=this.settings
            techoPage.refreSetting()
        });
        
    }
    saveSettings() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.saveData(this.settings);
        });
    }
    
}
class SampleSettingTab extends obsidian.PluginSettingTab {
    constructor(app, plugin) {
        super(app, plugin);
        this.plugin = plugin;
    }
    saveSettings({ rerenderSettings = false, refreshViews = false } = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.plugin.saveData(this.plugin.settings);
           
            if (rerenderSettings) {
                this.display();
            }
        });
    }
    display() {
        const MtimeSort=['bar','line','pie']
        const WtimeSort=['bar','line','pie','timeLines']
        const DtimeSort=['timeRing','timeLine']
        const MbillSort=['line','inPie','outPie']
        const WbillSort=['line','inPie','outPie']
        const DbillSort=['table']

        let { containerEl } = this;
        containerEl.empty();

        new obsidian.Setting(containerEl)
        .setName('是否以周日作为一周起始')
        .setDesc('')
        .addToggle(toggle => toggle
            .setValue(this.plugin.settings.starAtSunday)
            .onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.starAtSunday = val;
                yield this.saveSettings({ rerenderSettings: true, refreshViews: true });
            }
        )));

        containerEl.createEl('h2', { text: '时间统计' });
        new obsidian.Setting(containerEl)
            .setName('统计时间类名')
            .setDesc('当前值:'+this.plugin.settings.timeSortClass)
            .addText(text => {
            // text.inputEl.type = 'string';
            text.setValue(String(this.plugin.settings.timeSortClass));
            text.setPlaceholder(`输入一个小写英文字母`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.timeSortClass = val;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
        .setName('月时间统计方式')
        .setDesc("当前值： "+this.plugin.settings.MtimeSort)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.MtimeSort= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption(this.plugin.settings.MtimeSort,this.plugin.settings.MtimeSort)
            for(let i=0;i<MtimeSort.length;i++){
                if(!(MtimeSort[i]==this.plugin.settings.MtimeSort)){
                    dropdown.addOption(MtimeSort[i],MtimeSort[i])
                }
            }
        });
        new obsidian.Setting(containerEl)
        .setName('周时间统计方式')
        .setDesc("当前值： "+this.plugin.settings.WtimeSort)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.WtimeSort= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption(this.plugin.settings.WtimeSort,this.plugin.settings.WtimeSort)
            for(let i=0;i<WtimeSort.length;i++){
                if(!(WtimeSort[i]==this.plugin.settings.WtimeSort)){
                    dropdown.addOption(WtimeSort[i],WtimeSort[i])
                }
            }
        });
        new obsidian.Setting(containerEl)
        .setName('日时间统计方式')
        .setDesc("当前值： "+this.plugin.settings.DtimeSort)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.DtimeSort= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption(this.plugin.settings.DtimeSort,this.plugin.settings.DtimeSort)
            for(let i=0;i<DtimeSort.length;i++){
                if(!(DtimeSort[i]==this.plugin.settings.DtimeSort)){
                    dropdown.addOption(DtimeSort[i],DtimeSort[i])
                }
            }
        });

        
        containerEl.createEl('h2', { text: '账单统计' });
        new obsidian.Setting(containerEl)
            .setName('统计账单类名')
            .setDesc('当前值：'+this.plugin.settings.billSortClass)
            .addText(text => {
            // text.inputEl.type = 'string';
            text.setValue(String(this.plugin.settings.billSortClass));
            text.setPlaceholder(`输入一个小写英文字母`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.billSortClass = val;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('预计每日收/支最大数')
            .setDesc('当前值：'+this.plugin.settings.billDayNumber)
            .addText(text => {
            // text.inputEl.type = 'string';
            text.setValue(Number(this.plugin.settings.billDayNumber));
            text.setPlaceholder(`输入数字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.billDayNumber = val;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
            .setName('单位收支')
            .setDesc('当前值：'+this.plugin.settings.billDayPace)
            .addText(text => {
            // text.inputEl.type = 'string';
            text.setValue(Number(this.plugin.settings.billDayPace));
            text.setPlaceholder(`输入数字`);
            text.onChange((val) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.billDayPace = val;
                yield this.saveSettings();
            }));
        });
        new obsidian.Setting(containerEl)
        .setName('月账单统计方式')
        .setDesc("当前值： "+this.plugin.settings.MbillSort)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.MbillSort= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption(this.plugin.settings.MbillSort,this.plugin.settings.MbillSort)
            for(let i=0;i<MbillSort.length;i++){
                if(!(MbillSort[i]==this.plugin.settings.MbillSort)){
                    dropdown.addOption(MbillSort[i],MbillSort[i])
                }
            }
        });
        new obsidian.Setting(containerEl)
        .setName('周账单统计方式')
        .setDesc("当前值： "+this.plugin.settings.WbillSort)
        .addDropdown(dropdown => {
            dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
                this.plugin.settings.WbillSort= value;
                yield this.plugin.saveSettings();
            }))
            
            dropdown.addOption(this.plugin.settings.WbillSort,this.plugin.settings.WbillSort)
            for(let i=0;i<WbillSort.length;i++){
                if(!(WbillSort[i]==this.plugin.settings.WbillSort)){
                    dropdown.addOption(WbillSort[i],WbillSort[i])
                }
            }
        });
        new obsidian.Setting(containerEl)
        .setName('日账单统计方式')
        .setDesc("当前值： "+this.plugin.settings.DbillSort)
        // .addDropdown(dropdown => {
        //     dropdown.onChange((value) => __awaiter(this, void 0, void 0, function* () {
        //         this.plugin.settings.DbillSort= value;
        //         yield this.plugin.saveSettings();
        //     }))
            
        //     // dropdown.addOption(this.plugin.settings.DbillSort,this.plugin.settings.DbillSort)
        //     // for(let i=0;i<DbillSort.length;i++){
        //     //     if(!(DbillSort[i]==this.plugin.settings.DbillSort)){
        //     //         dropdown.addOption(DbillSort[i],DbillSort[i])
        //     //     }
        //     // }
        // });




    }
}


module.exports = MyPlugin;